#include <vector>
#include <QtTest>
#include "SmartPtr.hpp"
#include "bytes/MarksMap.hpp"
#include "bytes/Mark.hpp"

#define A(_offset, _size) \
	map.add(make_shared<Mark>(), _offset, _size);

#define C(_offset, _size) \
	QCOMPARE(result[_index]->offset(), size_t(_offset)); \
	QCOMPARE(result[_index]->size(), size_t(_size)); \
	_index++;


class MarksMapTests : public QObject
{
	Q_OBJECT
	
private slots:
	void add();
	void addMore();
	void remove();
	void range();
	//void insertBytesBeginning();
};


void MarksMapTests::add()
{
	MarksMap map;
	A(5, 2);
	A(1, 20);

	std::vector<MarkPtr> result = map.all();

	QCOMPARE(result.size(), size_t(2));
	unsigned int _index = 0;
	C(1, 20);
	C(5, 2);
}

void MarksMapTests::addMore()
{
	MarksMap map;
	A(3, 1);
	A(1, 5);
	A(2, 3);
	A(5, 3);
	A(20, 2);
	A(21, 5);
	A(10, 5);
	A(14, 2);
	A(30, 1);
	A(31, 1);
	A(32, 1);
	A(33, 1);
	A(31, 2);
	A(33, 4);
	A(37, 1);
	A(40, 4);
	A(40, 4);
	A(1, 100);
	A(50, 4);
	A(50, 5);

	std::vector<MarkPtr> result = map.all();

	QCOMPARE(result.size(), size_t(20));
	unsigned int _index = 0;
	C(1, 100);
	C(1, 5);
	C(2, 3);
	C(3, 1);
	C(5, 3);
	C(10, 5);
	C(14, 2);
	C(20, 2);
	C(21, 5);
	C(30, 1);
	C(31, 2);
	C(31, 1);
	C(32, 1);
	C(33, 4);
	C(33, 1);
	C(37, 1);
	C(40, 4);
	C(40, 4);
	C(50, 5);
	C(50, 4);
}

void MarksMapTests::remove()
{
	MarksMap map;
	A(3, 1);
	MarkPtr test1 = make_shared<Mark>();
	map.add(test1, 1, 5);
	A(2, 3);
	A(5, 3);
	A(20, 2);
	A(21, 5);
	A(10, 5);
	A(14, 2);
	MarkPtr test3 = make_shared<Mark>();
	map.add(test3, 30, 1);
	A(31, 1);
	A(32, 1);
	A(33, 1);
	A(31, 2);
	A(33, 4);
	A(37, 1);
	A(40, 4);
	A(40, 4);
	MarkPtr test2 = make_shared<Mark>();
	map.add(test2, 1, 100);
	A(50, 4);
	A(50, 5);

	map.remove(test1);
	map.remove(test2);
	map.remove(test3);

	A(3, 20);

	std::vector<MarkPtr> result = map.all();

	QCOMPARE(result.size(), size_t(18));
	unsigned int _index = 0;
	C(2, 3);
	C(3, 20);
	C(3, 1);
	C(5, 3);
	C(10, 5);
	C(14, 2);
	C(20, 2);
	C(21, 5);
	C(31, 2);
	C(31, 1);
	C(32, 1);
	C(33, 4);
	C(33, 1);
	C(37, 1);
	C(40, 4);
	C(40, 4);
	C(50, 5);
	C(50, 4);
}

void MarksMapTests::range()
{
	MarksMap map;
	A(3, 1);
	A(1, 5);
	A(2, 3);
	A(5, 3);
	A(20, 2);
	A(21, 5);
	A(10, 5);
	A(14, 2);
	A(30, 1);
	A(31, 1);
	A(32, 1);
	A(33, 1);
	A(31, 2);
	A(33, 4);
	A(37, 1);
	A(40, 4);
	A(40, 4);
	A(1, 100);
	A(50, 4);
	A(50, 5);

	std::vector<MarkPtr> result = map.range(4, 20);

	QCOMPARE(result.size(), size_t(7));
	unsigned int _index = 0;
	C(1, 100);
	C(1, 5);
	C(2, 3);
	C(5, 3);
	C(10, 5);
	C(14, 2);
	C(20, 2);
}

/*void MarksMapTests::insertBytesBeginning()
{
	MarksMap map;
	A(5, 2);
	A(1, 20);

	map.insertBytes(0, 10);

	std::vector<MarkPtr> result = map.all();

	QCOMPARE(result.size(), size_t(2));
	unsigned int _index = 0;
	C(11, 20);
	C(15, 2);
}*/


QTEST_MAIN(MarksMapTests)
#include "MarksMapTests.moc"
