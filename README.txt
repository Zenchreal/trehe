The Reverse Engineering Hex Editor

Trehe is a side project I've been working on for the past couple of years. It's
still in a pre-alpha stage as many features are not implemented or not working
correctly. The name is currently a working title and will likely be changed on
official release.

The "killer feature" of Trehe is the ability to build up structures by marking
a range bytes with a name and type. This can be used to aid in the process of
reverse engineering file formats. These marks will be stored on disk alongside
the file they are associated with.

This project is under the BSD license. See LICENSE.txt
