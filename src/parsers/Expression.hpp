#pragma once
#include <string>

bool parse_size_t(const std::string& input, size_t& result);
