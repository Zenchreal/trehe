#include "Expression.hpp"

#include <iostream>
#include <string>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>

namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;

using qi::_1;
using qi::_val;
using ascii::space;
using qi::uint_parser;
using qi::lit;
using qi::char_;
using qi::lexeme;
using qi::no_case;
using qi::eps;


template<typename Iterator, typename T>
struct IntExpressionParser : qi::grammar<Iterator, T(), ascii::space_type>
{
	IntExpressionParser() : IntExpressionParser::base_type(start)
    {
		decInt %= uint_parser<T, 10>();

		hexInt %= uint_parser<T, 16>();

		octInt %= uint_parser<T, 8>();

		binInt %= uint_parser<T, 2>();

		num %= no_case[
			   lexeme["0d" >> decInt]
			|  lexeme["0x" >> hexInt]
			|  lexeme["0o" >> octInt]
			|  lexeme["0b" >> binInt]
			|  decInt
			];

		bitorExp =
			bitxorExp[_val = _1]
			>> *(  (char_('|') >> bitxorExp[_val |= _1])
			    )
			;

		bitxorExp =
			bitandExp[_val = _1]
			>> *(  (char_('^') >> bitandExp[_val ^= _1])
			    )
			;

		bitandExp =
			bitshiftExp[_val = _1]
			>> *(  (char_('&') >> bitshiftExp[_val &= _1])
			    )
			;

		bitshiftExp =
			expression[_val = _1]
			>> *(  (lit(">>") >> expression[_val >>= _1])
				|  (lit("<<") >> expression[_val <<= _1])
			    )
			;

		expression =
			term[_val = _1]
			>> *(  (char_('+') >> term[_val += _1])
			    |  (char_('-') >> term[_val -= _1])
			    )
			;

		term =
			factor[_val = _1]
			>> *(  (char_('*') >> factor[_val *= _1])
			    |  (char_('/') >> factor[_val /= _1])
				|  (char_('%') >> factor[_val %= _1])
			    )
			;

		factor =
			num[_val = _1]
			|  '(' >> expression[_val = _1] >> ')'
			|  (char_('-') >> factor[_val = -_1])
			|  (char_('+') >> factor[_val = _1])
			|  (char_('~') >> factor[_val = ~_1])
			;

		start %= bitorExp;
	}

	qi::rule<Iterator, T()> decInt;
	qi::rule<Iterator, T()> hexInt;
	qi::rule<Iterator, T()> octInt;
	qi::rule<Iterator, T()> binInt;
	qi::rule<Iterator, T(), ascii::space_type> num;
	qi::rule<Iterator, T(), ascii::space_type> bitorExp;
	qi::rule<Iterator, T(), ascii::space_type> bitandExp;
	qi::rule<Iterator, T(), ascii::space_type> bitxorExp;
	qi::rule<Iterator, T(), ascii::space_type> bitshiftExp;
	qi::rule<Iterator, T(), ascii::space_type> expression;
	qi::rule<Iterator, T(), ascii::space_type> term;
	qi::rule<Iterator, T(), ascii::space_type> factor;
    qi::rule<Iterator, T(), ascii::space_type> start;
};


/**
 * Template that allows us to handle parsing different types of values easily.
 * Parse only integer types.
 */
template<typename T>
bool parse(const std::string& input, T& result)
{
	std::string::const_iterator iter = input.begin();
	std::string::const_iterator enditer = input.end();
	IntExpressionParser<std::string::const_iterator, T> grammar;

	if (qi::phrase_parse(iter, enditer, grammar, ascii::space, result) &&
		iter == enditer)
	{
		// Everything parsed correctly
		return true;
	}
	else
	{
		// Parsing failed
		result = 0;
		return false;
	}
}

/**
 * We specialize all types of parsers we'll need here so we don't have to drag
 * Boost Spirit around everywhere.
 */

bool parse_size_t(const std::string& input, size_t& result)
{
	return parse<size_t>(input, result);
}
