/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Main application file
 */
#include <iostream>
#include <fstream>
#include <QSessionManager>
#include <QDesktopServices>
#include <QDir>
#include <QTime>
#include <json/json.h>

#if QT_VERSION >= 0x050000
#include <QStandardPaths>
#endif

#include "Application.hpp"
#include "SmartPtr.hpp"
#include "bytes/MemoryBytes.hpp"
#include "bytes/BytesMap.hpp"
#include "bytes/MMappedFileWrapper.hpp"
#include "bytes/MMappedBytes.hpp"
#include "ui/MainWindow.hpp"
#include "bytes/Interpretation.hpp"
#include "bytes/BasicInterp.hpp"
#include "bytes/MarksMap.hpp"
#include "bytes/Mark.hpp"

#include "parsers/Expression.hpp"

//using namespace std;
using std::cout;
using std::endl;
using std::hex;
using std::dec;


void testBytesMap()
{
	BytesMap bytesMap;
	cout << "Application! " << endl;
	bytesMap.insert(0, make_shared<MemoryBytes>(50));
	/*bytesMap.insert(50, make_shared<MemoryBytes>(12));
	bytesMap.insert(50 + 12, make_shared<MemoryBytes>(12));*/
	if (bytesMap.insert(0, make_shared<MemoryBytes>((const uint8_t*) "ABCDE", 5)))
		cout << "yes" << endl;
	else
		cout << "no" << endl;

	if (bytesMap.insert(0, make_shared<MemoryBytes>(89)))
		cout << "yes" << endl;
	else
		cout << "no" << endl;

	if (bytesMap.insert(0, make_shared<MemoryBytes>(29)))
		cout << "yes" << endl;
	else
		cout << "no" << endl;

	if (bytesMap.insert(120, make_shared<MemoryBytes>(54)))
		cout << "yes" << endl;
	else
		cout << "no" << endl;

	if (bytesMap.remove(0, 3))
		cout << "yes" << endl;
	else
		cout << "no" << endl;

	if (bytesMap.overwrite(171, make_shared<MemoryBytes>((const uint8_t*) "YXGGGGGGYXGGGGGG", 16 + 37)))
		cout << "yes" << endl;
	else
		cout << "no" << endl;

	if (bytesMap.insert(224, make_shared<MemoryBytes>(66)))
		cout << "yes" << endl;
	else
		cout << "no" << endl;

	/*if (bytesMap.remove(2, 1000))
		cout << "yes" << endl;
	else
		cout << "no" << endl;*/

	BytesMap::Reader reader(bytesMap);
	reader.seek(0);
	reader.seek(26);

	uint8_t huge[1000];
	reader.seek(0);
	cout << "read " << reader.read(huge, 1000) << endl;
	
	reader.seek(225);
	reader.seek(0);

	while (!reader.end())
	{
		uint8_t byte[1];
		cout << dec << reader.tell() << ": ";
		reader.read(byte, 1);
		cout << hex << (unsigned int) byte[0] << " " << endl;
	}
}

void testOtherStuff()
{
	/*UInt32Interp interp;
	std::string output;

	uint8_t bytes[] = {0x12, 0x34, 0x56, 0xA8};
	interp.bytesToStringBounded(bytes, 4, output, 100);

	cout << "'" << output << "'" << endl;

	size_t length = 4;
	interp.stringToBytes("4510", bytes, length);
	cout << hex << (int) bytes[0] << endl;*/

	/*MarksMap map;
	map.add(make_shared<Mark>(), 4, 4);
	map.add(make_shared<Mark>(), 0, 1);
	map.add(make_shared<Mark>(), 0, 100);
	map.add(make_shared<Mark>(), 4, 6);

	cout << "+++++" << endl;
	for (MarkPtr mark : map.all())
	{
		cout << "mark: " << mark->offset() << " to " << mark->endingOffset() << endl;
	}

	cout << "=====" << endl;
	for (MarkPtr mark : map.range(1, 4))
	{
		cout << "mark: " << mark->offset() << " to " << mark->endingOffset() << endl;
	}

	cout << "?????" << endl;
	for (MarkPtr mark : map.contained(4, 800))
	{
		cout << "mark: " << mark->offset() << " to " << mark->endingOffset() << endl;
	}

	Json::Value root;
	map.serialize(root["marks"]);*/

	//cout << Json::FastWriter().write(root) << endl;
}

int main(int argc, char* argv[])
{
	Application app(argc, argv);
	MainWindow mainWin;
	mainWin.show();

	if (sizeof(size_t) > sizeof(ptrdiff_t))
	{
		cout << "==================================================" << endl;
		cout << "sizeof(size_t) = " << sizeof(size_t) << endl;
		cout << "sizeof(ptrdiff_t) = " << sizeof(ptrdiff_t) << endl;
		cout << "You might encounter problems opening larger files!" << endl;
		cout << "==================================================" << endl;
	}

	testOtherStuff();

	// Load the first command line argument as a file
	QStringList args = app.arguments();
	if (args.size() >= 2)
	{
		mainWin.openFile(args[1]);
	}
	
	return app.exec();
}

Application::Application(int& argc, char** argv):
	QApplication(argc, argv)
{
	setOrganizationName("Trehe");
	setOrganizationDomain("trehe.com");
	setApplicationName("Trehe");

	srand(QTime::currentTime().msec());
	
	Interpretation::initializeList();

	loadConfig();
	loadLast();

	connect(this, SIGNAL(aboutToQuit()), this, SLOT(beforeQuit()));
}

void Application::commitData(QSessionManager& manager)
{
	cout << "commitData()" << endl;
	if (manager.allowsInteraction())
	{
		// TODO Write this code
	}
	else
	{
		// TODO Write this code
	}
}

static QString dataFile(const QString& fileName)
{
	// This has changed in Qt5 so we have to ifdef around it
#if QT_VERSION >= 0x050000
	QDir dir(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation));
	std::cout << dir.absolutePath().toUtf8().constData() << std::endl;
#else
	QDir dir(QDesktopServices::storageLocation(QDesktopServices::DataLocation));
#endif

	QDir().mkpath(dir.absolutePath()); // Hacka wacka loo
	return dir.absoluteFilePath(fileName);
}

void Application::loadConfig()
{
	std::ifstream in;
	in.open(dataFile("config.json").toUtf8().constData(), std::ios_base::in);
	if (!in.is_open())
	{
		return;
	}

	Json::Reader reader;
	if (!reader.parse(in, configRoot))
	{
		cout << "Failed to parse config.json" << endl;
		return;
	}
	
	if (!configRoot["theme"].asString().empty())
	{
		loadTheme(configRoot["theme"].asString().c_str());
	}
}

void Application::saveConfig() const
{
	std::ofstream out;
	out.open(dataFile("config.json").toUtf8().constData(), std::ios_base::out);
	if (!out.is_open())
		return;

	out << configRoot;

	out.close();
}

void Application::loadLast()
{
	std::ifstream in;
	in.open(dataFile("last.json").toUtf8().constData(), std::ios_base::in);
	if (!in.is_open())
	{
		return;
	}

	Json::Reader reader;
	if (!reader.parse(in, lastRoot))
	{
		cout << "Failed to parse last.json" << endl;
		return;
	}
}

void Application::saveLast() const
{
	std::ofstream out;
	out.open(dataFile("last.json").toUtf8().constData(), std::ios_base::out);
	if (!out.is_open())
		return;

	out << lastRoot;

	out.close();
}

void Application::loadTheme(const QString& name)
{
	std::ifstream in;
	in.open(dataFile(name).toUtf8().constData(), std::ios_base::in);
	if (!in.is_open())
	{
		return;
	}

	Json::Reader reader;
	if (!reader.parse(in, themeRoot))
	{
		cout << "Failed to parse " << name.toUtf8().constData() << endl;
		return;
	}
}

void Application::beforeQuit()
{
	saveLast();
}
