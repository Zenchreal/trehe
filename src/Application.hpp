/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Main application file
 */
#pragma once
#include <QApplication>
#include <json/json.h>

class QSessionManager;


class Application : public QApplication
{
	Q_OBJECT

public:
	Application(int& argc, char** argv);
	virtual void commitData(QSessionManager& manager);

	Json::Value& config()
	{
		return configRoot;
	}

	const Json::Value& config() const
	{
		return configRoot;
	}

	void loadConfig();
	void saveConfig() const;

	Json::Value& last()
	{
		return lastRoot;
	}

	const Json::Value& last() const
	{
		return lastRoot;
	}

	void loadLast();
	void saveLast() const;
	
	Json::Value& theme()
	{
		return themeRoot;
	}

	const Json::Value& theme() const
	{
		return themeRoot;
	}

	void loadTheme(const QString& name);

protected slots:
	void beforeQuit();

private:
	Json::Value configRoot;
	Json::Value lastRoot;
	Json::Value themeRoot;
};
