/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Quick benchmarking tool
 */
#pragma once
//#include <sys/time.h>
#include <string>
#include <iostream>


class QuickBenchmark
{
public:
	inline QuickBenchmark(const std::string& name):
		name(name)
	{
		//gettimeofday(&timeStart, nullptr);
	}

	inline void stop()
	{
		/*gettimeofday(&timeEnd, nullptr);

		const long long micros = ((timeEnd.tv_sec * 1000000 + timeEnd.tv_usec) -
			(timeStart.tv_sec * 1000000 + timeStart.tv_usec));

		std::cout << name << ": " << micros << std::endl;*/
	}

private:
	//struct timeval timeStart, timeEnd;
	std::string name;
};
