/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#pragma once
#include <cstddef>
#include <QDialog>
#include <QByteArray>

namespace Ui
{
	class FindDialog;
}


class FindDialog : public QDialog
{
	Q_OBJECT
    
public:
	explicit FindDialog(QWidget* parent = nullptr);
	virtual ~FindDialog();
	void setBackwards(bool flag);
	void setCaseInsensitive(bool flag);
	void setBytes(const QByteArray& data);
	bool backwards() const;
	bool caseInsensitive() const;
	QByteArray bytes() const;
    
protected slots:
	void saveResult();

private:
	Ui::FindDialog* ui;
	bool backwardsResult;
	bool caseInsensitiveResult;
	QByteArray resultBytes;
};
