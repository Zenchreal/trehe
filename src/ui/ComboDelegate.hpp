/*
 * Put license here.
 *
 * Copyright (C) 2013 Ben Moench.
 */
#pragma once
#include <QItemDelegate>

class ComboDelegate : public QItemDelegate
{
	Q_OBJECT
	
public:
	ComboDelegate(QObject* parent = 0);
	virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
	virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
	virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
	virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem& option, const QModelIndex& index) const;
};
