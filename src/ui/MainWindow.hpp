/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Main hex editor window
 */
#pragma once
#include <memory>
#include "SmartPtr.hpp"
#include <QMainWindow>
#include "bytes/Mark.hpp"

class QAction;
class QMenu;
class QLabel;
class QMimeData;
class HexEditorWidget;
class Document;
class InspectorWindow;
class MarksWindow;
class Application;


class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow();
	virtual ~MainWindow();
	
	/**
	 * Load a file with the given filename. If the current document is unsaved,
	 * prompt the user to save it.
	 */
	void openFile(const QString& filename);
	
	/**
	 * Returns false if the user cancels.
	 */
	bool saveFile();

protected:
	virtual void resizeEvent(QResizeEvent * event);
	virtual void closeEvent(QCloseEvent* event);
	virtual void contextMenuEvent(QContextMenuEvent* event);
	virtual void dragEnterEvent(QDragEnterEvent* event);
	virtual void dragMoveEvent(QDragMoveEvent* event);
	virtual void dragLeaveEvent(QDragLeaveEvent* event);
	virtual void dropEvent(QDropEvent* event);

private slots:
	void new_();
	void open();
	void openFormat();
	void save();
	void saveAs();
	void reload();
	void readOnly();
	void quit();
	
	void undo();
	void redo();
	void cut();
	void copy();
	void paste();
	void delete_();
	void selectAll();
	void saveToFile();
	void insertMode();

	void copyAsValue();
	void copyAsHexString();
	void copyAsEscString();
	void copyAsBase64String();
	void pasteAsValue();
	void pasteAsHexString();
	void pasteAsBase64String();
	
	void find();
	void findNext();
	void findPrev();
	void goto_();
	void selectBlock();

	void quickMark();
	void mark();
	void removeMarks();
	
	void viewInspector();
	void viewMarks();
	void littleEndian();
	void bigEndian();
	void increaseLineLength();
	void decreaseLineLength();
	void resetLineLength();

	void about();

	void notImplemented();
	
	void editorStateChanged();
	void statusMessage(const QString& msg);

	void overwriteBytes(QByteArray bytes);

	void selectMark(MarkPtr mark);

private:
	void createEditor();
	void createInspector();
	void createMarksWindow();
	void createActions();
	void createMenus();
	void createStatusBar();
	void updateDocument();
	void updatePosAndSel();
	void updateInspector();
	void updateMarks();
	void updateEndianCheckmark();
	bool checkModified();
	void restoreLast();

	void showOpenFileError(const QString& filename);
	void showFileTooLargeError(const QString& filename);

	void todo(const QString& what);
	void printMimeTypes(const QMimeData* mimeData) const;

	// Just keep a copy of this around so we can access the configuration
	Application* app;

	// The main hex editor widget and document
	HexEditorWidget* editor;
	shared_ptr<Document> doc;

	// Current find options (for find next/previous);
	QByteArray findPattern;
	bool findBackwards;
	bool findCaseInsensitive;
	
	// Other windows
	InspectorWindow* inspector;
	MarksWindow* marksWindow;

	// Actions
	QAction* newAct;
	QAction* openAct;
	QAction* openFormatAct;
	QAction* saveAct;
	QAction* saveAsAct;
	QAction* reloadAct;
	QAction* readOnlyAct;
	QAction* quitAct;
	
	QAction* undoAct;
	QAction* redoAct;
	QAction* cutAct;
	QAction* copyAct;
	QMenu* copyAsMenu;
	QAction* pasteAct;
	QMenu* pasteAsMenu;
	QAction* deleteAct;
	QAction* selectAllAct;
	QAction* insertFileAct;
	QAction* saveToFileAct;
	QAction* insertModeAct;
	QAction* preferencesAct;

	QAction* copyAsValueAct;
	QAction* copyAsHexStringAct;
	QAction* copyAsBase64Act;
	QAction* copyAsEscStringAct;
	QAction* pasteAsValueAct;
	QAction* pasteAsHexStringAct;
	QAction* pasteAsBase64Act;

	QAction* findAct;
	QAction* findNextAct;
	QAction* findPrevAct;
	QAction* replaceAct;
	QAction* gotoAct;
	QAction* selectBlockAct;

	QAction* quickMarkAct;
	QAction* markAct;
	QAction* removeMarksAct;

	QAction* inspectorAct;
	QAction* marksAct;
	QAction* littleEndianAct;
	QAction* bigEndianAct;
	QAction* increaseLineLengthAct;
	QAction* decreaseLineLengthAct;
	QAction* resetLineLengthAct;

	QAction* aboutAct;

	// Menus
	QMenu* fileMenu;
	QMenu* editMenu;
	QMenu* searchMenu;
	QMenu* marksMenu;
	QMenu* viewMenu;
	QMenu* helpMenu;

	// Status bar
	QLabel* mainStatus;
	QLabel* overwriteStatus;
	QLabel* fileSizeStatus;
};

