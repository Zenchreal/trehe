/*
 * Put license here.
 *
 * Copyright (C) 2013 Ben Moench.
 */
#include "MarksWindow.hpp"
#include <string>
#include <iostream>
#include <set>
#include <QTreeView>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QContextMenuEvent>
#include <QMessageBox>
#include <QMimeData>
#include <QClipboard>
#include <QApplication>
#include <QTextStream>
#include <QDebug>
#include "ComboDelegate.hpp"
#include "ColumnsDialog.hpp"
#include "bytes/Mark.hpp"
#include "bytes/MarksHierarchy.hpp"
#include "bytes/Interpretation.hpp"

using std::cout;
using std::endl;


/**
 * Storage for per-QStandardItem metadata (i.e. keep track of what
 * MarkPtr an item is using).
 */
struct MarkUserData
{
	enum Column
	{
		NAME = 0,
		TYPE = 1,
		VALUE = 2
	};
	
	MarkUserData():
		column(NAME)
	{
	}
	
	MarkUserData(Column column, MarkPtr mark):
		column(column),
		mark(mark)
	{
	}
	
	Column column;
	MarkPtr mark;
};

Q_DECLARE_METATYPE(MarkUserData);

MarksWindow::MarksWindow(QWidget* parent):
	QDockWidget(parent, Qt::Tool | Qt::CustomizeWindowHint |
		Qt::WindowTitleHint | Qt::WindowCloseButtonHint),
	tree(nullptr),
	model(nullptr),
	blockItemChanged(false),
	contextMenu(this)
{
	tree = new QTreeView();
	model = new QStandardItemModel();
	model->setHorizontalHeaderLabels(QStringList()
		<< tr("Name")
		<< tr("Type")
		<< tr("Value")
		<< tr("Offset")
		<< tr("Size"));
	
	tree->setModel(model);
	tree->setFocusPolicy(Qt::ClickFocus);
	tree->setSelectionMode(QTreeView::ExtendedSelection);

	tree->hideColumn(3);
	tree->hideColumn(4);
	
	setWidget(tree);
	setWindowTitle(tr("Marks"));
	createContextMenu();
	
	// This will disable the appropriate context menu items
	selectionChanged(QItemSelection(), QItemSelection());

	connect(model, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(itemChanged(QStandardItem*)));
	connect(tree, SIGNAL(clicked(const QModelIndex&)), this, SLOT(itemClicked(const QModelIndex&)));
	connect(tree, SIGNAL(expanded(const QModelIndex&)), this, SLOT(itemExpanded(const QModelIndex&)));
	connect(tree, SIGNAL(collapsed(const QModelIndex&)), this, SLOT(itemCollapsed(const QModelIndex&)));
	connect(tree->selectionModel(), SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
		this, SLOT(selectionChanged(const QItemSelection&, const QItemSelection&)));
}

void MarksWindow::updateAll(const std::vector<MarkPtr>& marks, const Document& doc)
{
	blockItemChanged = true;
	
	// We can't use clear because that would wipe out the header labels
	model->removeRows(0, model->rowCount());

	// Not sure if this should be here
	MarksHierarchy n;
	n.build(marks);
	updateHierarchy(n);
	
	//updateValues(doc);

	blockItemChanged = false;
}

void MarksWindow::updateHierarchy(const MarksHierarchy& marksHier)
{
	blockItemChanged = true;
	model->removeRows(0, model->rowCount());
	doUpdateHierarchy(marksHier.root(), model->invisibleRootItem());
	blockItemChanged = false;
}

void MarksWindow::doUpdateHierarchy(const MarksHierarchy::Node& node,
		QStandardItem* parent)
{
	// Get the parent mark
	MarkPtr parentMark = node.ptr();

	for (const MarksHierarchy::Node& child : node)
	{
		MarkPtr mark = child.ptr();
		
		// Create the name, type, value
		QStandardItem* name = new QStandardItem(QString(tr("[%0]")).arg(mark->offset()));
		QStandardItem* type = new QStandardItem("t");
		QStandardItem* value = new QStandardItem("v");
		
		name->setData(QVariant::fromValue(MarkUserData(MarkUserData::NAME, mark)));
		type->setData(QVariant::fromValue(MarkUserData(MarkUserData::TYPE, mark)));
		value->setData(QVariant::fromValue(MarkUserData(MarkUserData::VALUE, mark)));

		type->setFlags(type->flags() & ~Qt::ItemIsEditable);
		
		// Append the row
		QList<QStandardItem*> row;
		row << name << type << value;
		parent->appendRow(row);

		// Collapse/expand
		tree->setExpanded(name->index(), !mark->isCollapsed());
		
		// Recursively add the children of this node
		doUpdateHierarchy(child, name);
	}
}

void MarksWindow::updateValues(const Document& doc)
{
	blockItemChanged = true;

	for (int i = 0; i < model->rowCount(); i++)
	{
		doUpdateValues(doc, model->item(i, 0), model->item(i, 1), model->item(i, 2));
	}

	blockItemChanged = false;
}

void MarksWindow::doUpdateValues(const Document& doc,
	QStandardItem* name,
	QStandardItem* type,
	QStandardItem* value)
{
	// Get the current mark
	MarkPtr mark = name->data().value<MarkUserData>().mark;

	shared_ptr<Interpretation> interp = mark->defaultInterpretation();

	// Update the current item
	QPixmap pix(10, 10);
	pix.fill(QColor(mark->color()));
	name->setIcon(QIcon(pix));

	if (mark->hasName())
	{
		name->setText(mark->name().c_str()); // TODO Unicode!!
	}
	else
	{
		name->setText(QString(tr("[%0]")).arg(mark->offset()));

		// Offset should be italic
		//QFont font1 = name->font();
		//font1.setItalic(true);
		//name->setFont(font1);
	}

	// Set the type name
	bool structType = name->rowCount() > 0;
	if (structType)
	{
		type->setText("struct");
	}
	else if (interp)
	{
		type->setText(interp->name().c_str());
	}
	else
	{
		type->setText("data");
	}
		
	// Type name should be bold
	QFont font = type->font();
	font.setBold(true);
	type->setFont(font);

	// Set value
	if (structType)
	{
		value->setText("");
	}
	else if (interp)
	{
		size_t length = std::min(mark->size(), size_t(64));
		
		QByteArray bytes(length, '\0');
		BytesMap::Reader reader = doc.reader();
		reader.seek(mark->offset());
		reader.read(bytes.data(), length);
	
		std::string valueString;
		Interpretation::Result result;
		
		result = interp->bytesToStringBounded(
			(const uint8_t*) bytes.data(),
			length,
			valueString,
			64
		);
		
		if (result == Interpretation::SUCCESS)
		{
			value->setText(valueString.c_str());
		}
		else
		{
			value->setText("oops");
		}
	}
	else
	{
		value->setText("0");
	}

	// Update all children
	for (int i = 0; i < name->rowCount(); i++)
	{
		doUpdateValues(doc, name->child(i, 0), name->child(i, 1), name->child(i, 2));
	}
}

void MarksWindow::loadState(const Json::Value& in)
{
	QByteArray state(in.get("columnHeaderState", "").asCString());
	tree->header()->restoreState(QByteArray::fromBase64(state));
}

void MarksWindow::saveState(Json::Value& out) const
{
	out["columnHeaderState"] = tree->header()->saveState().toBase64().constData();
}

void MarksWindow::contextMenuEvent(QContextMenuEvent* event)
{
	assert(event);
	event->accept();
	contextMenu.exec(event->globalPos());
}

void MarksWindow::createContextMenu()
{
	editAct = new QAction(tr("&Edit..."), this);
	connect(editAct, SIGNAL(triggered()), this, SLOT(editClicked()));

	resizeAct = new QAction(tr("&Resize"), this);

	removeAct = new QAction(tr("&Delete"), this);
	//removeAct->setShortcuts(QKeySequence::Delete);
	connect(removeAct, SIGNAL(triggered()), this, SLOT(removeClicked()));

	cutTreeAct = new QAction(tr("Cu&t"), this);

	copyTreeAct = new QAction(tr("&Copy"), this);
	copyTreeAct->setShortcuts(QKeySequence::Copy);
	connect(copyTreeAct, SIGNAL(triggered()), this, SLOT(copyClicked()));

	pasteTreeAct = new QAction(tr("&Paste at Cursor"), this);

	// C-like Struct

	columnsAct = new QAction(tr("C&olumns..."), this);
	connect(columnsAct, SIGNAL(triggered()), this, SLOT(columnsClicked()));

	contextMenu.addAction(editAct);
	contextMenu.addAction(removeAct);
	contextMenu.addSeparator();
	contextMenu.addAction(cutTreeAct);
	contextMenu.addAction(copyTreeAct);
	contextMenu.addAction(pasteTreeAct);
	contextMenu.addSeparator();
	contextMenu.addAction(columnsAct);

	addAction(copyTreeAct);
}

std::set<MarkPtr> MarksWindow::selectedMarks() const
{
	std::set<MarkPtr> result;
	for (QModelIndex index : tree->selectionModel()->selectedIndexes())
	{
		result.insert(getMark(index));
	}
	return result;
}

MarkPtr MarksWindow::getMark(const QModelIndex& index) const
{
	return getMark(model->itemFromIndex(index));
}

MarkPtr MarksWindow::getMark(const QStandardItem* item) const
{
	if (!item)
		return MarkPtr();

	return item->data().value<MarkUserData>().mark;
}

void MarksWindow::itemChanged(QStandardItem* item)
{
	assert(item);
	
	// Assumption: this only works if signals are emitted in the same
	// thread as everything else.
	if (blockItemChanged)
		return;

	MarkUserData data = item->data().value<MarkUserData>();

	// Make sure there's a mark associated with this item
	if (!data.mark)
		return;

	if (data.column == MarkUserData::NAME)
	{
		data.mark->setName(item->text().toUtf8().constData()); // TODO Unicode
	}
}

void MarksWindow::itemClicked(const QModelIndex& index)
{
	MarkPtr mark = getMark(index);
	
	if (mark)
		markClicked(mark);
}

void MarksWindow::itemExpanded(const QModelIndex& index)
{
	MarkPtr mark = getMark(index);

	if (mark)
		mark->setCollapsed(false);
}

void MarksWindow::itemCollapsed(const QModelIndex& index)
{
	MarkPtr mark = getMark(index);

	if (mark)
		mark->setCollapsed(true);
}

void MarksWindow::selectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
	int count = tree->selectionModel()->selectedRows().count();

	editAct->setEnabled(count > 0);
	resizeAct->setEnabled(count == 1);
	removeAct->setEnabled(count > 0);
	cutTreeAct->setEnabled(count > 0);
	copyTreeAct->setEnabled(count > 0);
}

void MarksWindow::editClicked()
{
}

void MarksWindow::removeClicked()
{
	std::set<MarkPtr> marks = selectedMarks();
	
	if (marks.size() > 1)
	{
		// Confirm the user wants to delete this many marks
		QMessageBox::StandardButton reply;
		reply = QMessageBox::question(this, tr("Delete marks"), tr(
			"Are you sure you want to permanently delete %1 marks?").arg(marks.size()),
			QMessageBox::Yes | QMessageBox::No);

		if (reply != QMessageBox::Yes)
			return;
	}

	for (const MarkPtr& mark : marks)
	{
		
	}
}

static void collectMarks(const QStandardItem* item, std::set<MarkPtr>& marks)
{
	marks.insert(item->data().value<MarkUserData>().mark);
	for (int i = 0; i < item->rowCount(); i++)
	{
		collectMarks(item->child(i, 0), marks);
	}
}

void MarksWindow::copyClicked()
{/*
	// Collect all selected marks and children in a set to avoid dupes
	std::set<MarkPtr> marks;
	for (QModelIndex index : tree->selectionModel()->selectedIndexes())
	{
		collectMarks(model->itemFromIndex(index), marks);
	}

	// Convert to vector for MarksHierarchy
	std::vector<MarkPtr> marksVector;
	for (const MarkPtr& mark : marks)
	{
		marksVector.push_back(mark);
	}

	std::vector<MarkPtr>::iterator iter;

	std::stable_sort(marksVector.begin(), marksVector.end(), [](const MarkPtr& a, const MarkPtr& b)
	{
		return bool(a->offset() < b->offset());
	});

	QString outstr;
	QTextStream out(&outstr);

	for (const MarkPtr& mark : marksVector)
	{
		out << mark->offset() << " " << mark->name().c_str() << endl;
	}
	
	// Create mime data
	QMimeData* mime = new QMimeData();
	mime->setText(outstr);

	QApplication::clipboard()->setMimeData(mime);*/
}

void MarksWindow::columnsClicked()
{
	ColumnsDialog dlg(this);
	for (int i = 0; i < tree->header()->count(); i++)
	{
		dlg.addColumn(model->horizontalHeaderItem(i)->text(), !tree->isColumnHidden(i));
	}

	if (dlg.exec() == QDialog::Accepted)
	{
		for (int i = 0; i < tree->header()->count(); i++)
		{
			tree->setColumnHidden(i, !dlg.columnChecked(model->horizontalHeaderItem(i)->text()));
		}
	}
}
