/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * The hex editor widget
 */
#include "HexEditorWidget.hpp"
#include <iostream>
#include <algorithm>
#include <QApplication>
#include <QString>
#include <QtGui>
#include <QPainter>
#include <QTextStream>
#include <QClipboard>
#include <QScrollBar>
#include <QMenu>
#include <QToolTip>
#include <QTextCodec>
#include "RangePathGen.hpp"
#include "bytes/Document.hpp"
#include "QuickBenchmark.hpp"
#include "CopyPasteHandler.hpp"


HexEditorWidget::HexEditorWidget(QWidget* parent, bool custContextMenu, const Json::Value& theme):
	QAbstractScrollArea(parent),
	theme(theme),
	bytesPerLine(16),
	totalBytes(0),
	drawTextStatics(true),
	caretTimer(0),
	customContextMenuFlag(custContextMenu),
	copyPasteHandler(this),
	addressDigitCount(8)
{
	// Default to a blank document
	doc = make_shared<Document>("", true);
	
	setAutoFillBackground(false);
	clearEditorState();
	createFonts();
	createBrushes();
	createPens();
	createContextMenu();
	
	// TODO This is only for testing
	//std::cout << this->theme << std::endl;

	updateFileSize();
	updateLayout();

	viewport()->setCursor(Qt::IBeamCursor);
	restartTimer();
}

void HexEditorWidget::reloadTheme(const Json::Value& theme)
{
	this->theme = theme;
	
	createFonts();
	createBrushes();
	createPens();
	createContextMenu();

	updateFileSize();
	updateLayout();
}

void HexEditorWidget::setDocument(shared_ptr<Document> doc)
{
	// In case of a null document, just put something empty here. If
	// doc is null we will crash.
	if (!doc)
		doc = make_shared<Document>("", true);
	
	this->doc = doc;
	
	clearEditorState();
	updateFileSize();
	verticalScrollBar()->setValue(0);
}

shared_ptr<Document> HexEditorWidget::document() const
{
	return doc;
}

void HexEditorWidget::setLineLength(unsigned int size)
{
	if (size > 0 && size <= 1024)
	{
		bytesPerLine = size;
	}
	updateFileSize();
	updateLayout();
}

unsigned int HexEditorWidget::lineLength() const
{
	return bytesPerLine;
}

void HexEditorWidget::setAddressLength(unsigned int count)
{
	if (count > 0 && count <= 1024)
	{
		addressDigitCount = count;
	}
	updateFileSize();
	updateLayout();
}

unsigned int HexEditorWidget::addressLength()
{
	return addressDigitCount;
}

QSize HexEditorWidget::sizeHint() const
{
	return QSize(750, 500);
}

void HexEditorWidget::setSelection(Selection sel)
{
	using std::min;
	using std::max;

	if (sel.first == size_t(-1) || sel.second == size_t(-1))
		return;

	if (totalBytes < 1)
	{
		clearSelection();
		return;
	}

	selCaretPos = CaretPosition(min<size_t>(max<size_t>(sel.first, 0), totalBytes - 1), LD);
	caretPos = CaretPosition(min<size_t>(max<size_t>(sel.second, 0), totalBytes - 1), RD);
}

void HexEditorWidget::clearSelection()
{
	selCaretPos = CaretPosition(0, NONE);
}

bool HexEditorWidget::hasSelection() const
{
	Selection sel = selection();
	return (sel.first != (size_t) -1) && (sel.second != (size_t) -1);
}

HexEditorWidget::Selection HexEditorWidget::selection() const
{
	using std::swap;
	using std::max;
	using std::min;
	
	if (!selCaretPos.valid())
		return Selection(-1, -1);

	CaretPosition a = selCaretPos;
	CaretPosition b = caretPos;

	// Swap so that a is the first caret and b is the second
	if (a.offset > b.offset)
	{
		swap(a, b);
	}
	else if (a.offset == b.offset)
	{
		if (a.inside > b.inside)
		{
			swap(a, b);
		}
	}

	// Now determine whether or not each endpoint should be included
	size_t start = a.offset;
	size_t end = b.offset;
	if (a.inside > RD)
		start += 1;
	if (b.inside < LD)
		end -= 1;

	// Make sure there is a selection
	if (start > end)
		return Selection(-1, -1);

	// Sanity check -- don't allow the selection to go outside the hex
	end = min<size_t>(max<size_t>(end, 0), totalBytes - 1);
	start = min<size_t>(max<size_t>(start, 0), totalBytes - 1);

	return Selection(start, end);
}

size_t HexEditorWidget::selectionOffset() const
{
	if (!hasSelection())
		return -1;
	
	Selection sel = selection();
	return sel.first;
}

size_t HexEditorWidget::selectionLength() const
{
	if (!hasSelection())
		return 0;
	
	Selection sel = selection();
	return sel.second - sel.first + 1;
}

void HexEditorWidget::startSelection()
{
	if (!hasSelection())
		selCaretPos = caretPos;
}

bool HexEditorWidget::inSelection(size_t offset) const
{
	if (!hasSelection())
		return false;
		
	Selection sel = selection();
	return offset >= sel.first && offset <= sel.second;
}

size_t HexEditorWidget::caretOffset() const
{
	return caretPos.offset;
}

void HexEditorWidget::setCaretOffset(size_t offset)
{
	using std::max;
	using std::min;

	offset = max(min(offset, totalBytes), size_t(0));
	caretPos = CaretPosition(offset, LD);
	selCaretPos = CaretPosition(0, NONE);

	// The caret should pulse
	restartTimer();
	scrollToCaret();
}

bool HexEditorWidget::insertMode() const
{
	return caretMode == INSERT;
}

bool HexEditorWidget::overwriteBytes(const QByteArray& bytes)
{
	using std::min;
	
	if (!doc)
		return false;
	if (bytes.size() < 1)
		return false;
	
	// Data pointer
	const uint8_t* ptr = (const uint8_t*) bytes.constData();
	size_t length = bytes.size();
	
	// Replace the selection
	if (hasSelection())
	{
		length = min(length, selectionLength());
		
		doc->overwrite(selectionOffset(), ptr, length);
	}
	else
	{
		doc->overwrite(caretPos.offset, ptr, length);
	}
	
	// TODO Do we want to include this here? Our caller might also call
	// and update function, then we'd be updating twice.
	updateFileSize();
	updateEverything();
	return true;
}

bool HexEditorWidget::putBytes(const QByteArray& bytes)
{
	if (!doc)
		return false;
	if (bytes.size() < 1)
		return false;
	
	// Data pointer
	const uint8_t* ptr = (const uint8_t*) bytes.constData();
	size_t length = bytes.size();
	
	// Replace the selection
	if (hasSelection())
	{
		Selection sel = selection();
		size_t selLength = selectionLength();
		
		if (caretMode == INSERT)
		{
			doc->remove(sel.first, selLength);
			doc->insert(sel.first, ptr, length);
		}
		else if (caretMode == OVERWRITE)
		{
			doc->overwrite(sel.first, ptr, length);
		}
		clearSelection();
		
		// Caret to the end of the insert
		caretPos = CaretPosition(sel.first + length, LD);
	}
	else
	{
		if (caretMode == INSERT)
		{
			doc->insert(caretPos.offset, ptr, length);
		}
		else if (caretMode == OVERWRITE)
		{
			doc->overwrite(caretPos.offset, ptr, length);
		}
		
		// Caret to the end of the insert
		caretPos = CaretPosition(caretPos.offset + length, LD);
	}
	
	// TODO Do we want to include this here? Our caller might also call
	// and update function, then we'd be updating twice.
	updateFileSize();
	updateEverything();
	return true;
}

void HexEditorWidget::deleteBytes()
{
	deleteByteNearCaret(false, true);
	updateEverything();
}

QByteArray HexEditorWidget::getBytes(size_t maxLength) const
{
	using std::min;
	
	if (!doc)
		return QByteArray();
	if (!hasSelection())
		return QByteArray();
		
	Selection sel = selection();
	size_t length = sel.second - sel.first + 1;

	if (maxLength > 0)
		length = min(length, maxLength);
	
	// Read the data 
	QByteArray data(length, '\0');
	BytesMap::Reader reader = doc->reader();
	reader.seek(sel.first);
	reader.read(data.data(), length);
	
	// If the read doesn't completely succeed for some reason or another,
	// the remaining bytes (length minus read) will be filled with the
	// null character.
	return data;
}

QByteArray HexEditorWidget::getBytes(size_t offset, size_t length) const
{
	if (!doc)
		return QByteArray();

	// Read the data 
	QByteArray data(length, '\0');
	BytesMap::Reader reader = doc->reader();
	reader.seek(offset);
	reader.read(data.data(), length);
	
	// If the read doesn't completely succeed for some reason or another,
	// the remaining bytes (length minus read) will be filled with the
	// null character.
	return data;
}

static uint32_t colorDistance(uint32_t x, uint32_t y)
{
	int32_t r = ((x >> 16) & 0xFF) - ((y >> 16) & 0xFF);
	int32_t g = ((x >> 8) & 0xFF) - ((y >> 8) & 0xFF);
	int32_t b = (x & 0xFF) - (y & 0xFF);
	return r * r + g * g + b * b;
}

uint32_t HexEditorWidget::suggestMarkColor(size_t offset, size_t length) const
{
	uint32_t tolerance = 200 * 200;
	uint32_t color;

	for (unsigned int i = 0; i < 50; i++)
	{
		color = (rand() & 0xFF) | ((rand() & 0xFF) << 8) | ((rand() & 0xFF) << 16);

		// TODO This function really needs to be improved
		/*if (colorDistance(color, 0xFFFFFF) < tolerance)
			continue;*/
		if (colorDistance(color, 0x000000) < tolerance)
			continue;

		// We found a color that was a sufficient distance from everything else
		break;
	}
	return 0xFF000000 | color;
}

bool HexEditorWidget::event(QEvent* event)
{
	if (event->type() == QEvent::KeyPress)
	{
		QKeyEvent* ke = static_cast<QKeyEvent*>(event);
		if (ke->key() == Qt::Key_Tab)
		{
			keyPressEvent(ke);
			return true;
		}
	}
	else if (event->type() == QEvent::ToolTip)
	{
		QHelpEvent* he = static_cast<QHelpEvent*>(event);

		handleTooltip(he);
		return true;
	}
	return QAbstractScrollArea::event(event);
}

void HexEditorWidget::timerEvent(QTimerEvent* event)
{
	assert(event);
	if (event->timerId() == caretTimer)
	{
		caretBlack = !caretBlack;
		event->accept();
		viewport()->update();
	}
	else
	{
		event->ignore();
	}
}

void HexEditorWidget::focusInEvent(QFocusEvent* event)
{
	QAbstractScrollArea::focusInEvent(event);
	viewport()->update();
}

void HexEditorWidget::focusOutEvent(QFocusEvent* event)
{
	QAbstractScrollArea::focusOutEvent(event);
	viewport()->update();
}

void HexEditorWidget::contextMenuEvent(QContextMenuEvent* event)
{
	if (customContextMenuFlag)
	{
		event->ignore();
	}
	else
	{
		event->accept();
		contextMenu->exec(event->globalPos());
	}
}

std::pair<HexEditorWidget::Pane, HexEditorWidget::CaretPosition>
	HexEditorWidget::hitTest(int x, int y, Pane forcePane) const
{
	using std::pair;
	using std::min;
	using std::max;
	
	pair<Pane, CaretPosition> result;

	// TODO There's a bug in the forcePane code (fixed already?)
	
	// To elaborate: when making a selection (which means we will be
	// forcing a pane) dragging the cursor to the left of the pane will
	// produce incorrect results in the wrong selection by one row. This
	// happens for both the hex and chars pane. Dragging the cursor to
	// the right is not bugged.

	// Wow, I think I fixed it. I'm stupid. This function is so terrible
	// because of the conversions between signed <-> unsigned and
	// between int <-> long. The bugged line was:
	//
	// long offset = max<long>(x - charsOffset, 0);
	//
	// Both x and charsOffset are ints, but x is signed. I think the
	// compiler is making x unsigned in this statement causing a large
	// positive number when there should be a negative. The fix was to
	// cast both to long
	
	// TODO Insert mode should use a much better hit testing algo
	
	// Get the row index of the first row visible on the screen and
	// use this to calculate the row index where we clicked.
	const ptrdiff_t topVisibleRow = verticalScrollBar()->value();
	const ptrdiff_t currentRowOffset = (y / (ptrdiff_t) lineHeight + topVisibleRow) * bytesPerLine;

	if (x <= (int) addressHexDividerOffset && forcePane == NOPANE)
	{
		result.first = HEX;
		result.second = CaretPosition(min<ptrdiff_t>(currentRowOffset, totalBytes), LP);
	}
	else if ((x <= (int) hexCharsDividerOffset && forcePane == NOPANE) || forcePane == HEX)
	{
		// Well, this was a lot prettier in Python...
		const uint width = hexDigitFullWidth;
		ptrdiff_t offset = max<ptrdiff_t>(ptrdiff_t(x) - ptrdiff_t(hexOffset) + ptrdiff_t(padding / 2), 0);
		offset = min<ptrdiff_t>(offset, width * bytesPerLine - 1);
		const ptrdiff_t positionInRow = offset / width;
		const ptrdiff_t insideOffset = offset % width;

		// Determine exactly which part of the hex digits was clicked
		InsideSpecifier inside;
		if (insideOffset < padding / 2)
			inside = LP;
		else if (insideOffset < (padding / 2 + digitWidth))
			inside = LD;
		else if (insideOffset < (padding / 2 + digitWidth * 2))
			inside = RD;
		else
			inside = RP;
			
		// Make sure we don't set a negative offset
		size_t pos;
		if (currentRowOffset + positionInRow < 0)
		{
			pos = 0;
			inside = LD;
		}
		else
		{
			pos = (size_t) (currentRowOffset + positionInRow);
		}
		
		// Now make sure we don't set an offset past the end of the file
		if (pos >= totalBytes)
		{
			pos = totalBytes;
			inside = LD;
		}

		result.first = HEX;
		result.second = CaretPosition(pos, inside);
	}
	else if (forcePane == NOPANE || forcePane == CHARS)
	{
		const uint width = charWidth;
		ptrdiff_t offset = max<ptrdiff_t>(ptrdiff_t(x) - ptrdiff_t(charsOffset), 0);
		offset = min<ptrdiff_t>(offset, width * bytesPerLine - 1);
		const ptrdiff_t positionInRow = offset / width;
		InsideSpecifier inside = LD;

		size_t pos = min<size_t>(max<ptrdiff_t>(currentRowOffset + positionInRow, 0), totalBytes);
		if (pos >= totalBytes)
			inside = LD;
			
		result.first = CHARS;
		result.second = CaretPosition(pos, inside);
	}
	else
	{
		// User clicked to the right of everything
		result.first = NOPANE;
	}
	return result;
}

void HexEditorWidget::mousePressEvent(QMouseEvent* event)
{
	using std::pair;
	
	assert(event);
	event->ignore();

	// Hit test the cursor position
	pair<Pane, CaretPosition> hitTestResult = hitTest(event->x(), event->y());
	Pane pane = hitTestResult.first;
	CaretPosition newCaretPos = hitTestResult.second;

	// Mouse buttons
	const bool leftButton = (event->button() == Qt::LeftButton);
	const bool rightButton = (event->button() == Qt::RightButton);
	
	if (leftButton || rightButton)
	{
		if (pane != NOPANE)
		{
			// If the shift key is down go right into selection mode
			if (event->modifiers() & Qt::ShiftModifier)
			{
				mouseSelecting = true;
				startSelection();
			}
			else
			{
				if (leftButton || !inSelection(newCaretPos.offset))
				{
					clearSelection();
				}
				else
				{
					// We don't want to change the caret position
					newCaretPos = caretPos;
				}
			}
			
			focusedPane = pane;
			caretPos = newCaretPos;
			lastHitTestSucceeded = true;
			restartTimer();
			event->accept();
		}
		else
		{
			lastHitTestSucceeded = false;
		}
	}
	updateEverything();
}

void HexEditorWidget::mouseDoubleClickEvent(QMouseEvent* event)
{
	assert(event);
	event->ignore();

	if (lastHitTestSucceeded)
	{
		event->accept();
		startSelection();
		updateEverything();
	}
}

void HexEditorWidget::mouseMoveEvent(QMouseEvent* event)
{
	using std::pair;
	
	assert(event);
	event->ignore();

	// Mouse buttons
	const bool leftButton = (event->buttons() & Qt::LeftButton);

	if (leftButton && lastHitTestSucceeded)
	{
		// Determine whether or not we need to scroll up
		QScrollBar* scrollBar = verticalScrollBar();
		if (event->y() < 0)
		{
			scrollBar->setValue(scrollBar->value() - 1);
		}
		if (event->y() > viewport()->height())
		{
			scrollBar->setValue(scrollBar->value() + 1);
		}
		
		// Hit test
		pair<Pane, CaretPosition> hitTestResult = hitTest(event->x(), event->y(), focusedPane);
		CaretPosition newCaretPos = hitTestResult.second;
		
		if (!mouseSelecting)
		{
			// TODO Update this so that selecting across the two
			// digits will work but selecting the empty space won't.
			// Modified so that it doesn't create a selection if the
			// user moves the mouse on the current byte.
			if (caretPos.offset != newCaretPos.offset)
			{
				mouseSelecting = true;
				selCaretPos = caretPos;
			}
		}

		if (mouseSelecting)
			caretPos = newCaretPos;

		event->accept();
		
		// This function will not redraw unless the caret position has
		// changed; we don't have to call updateEverything().
		updateCaretAndSelection();
	}
}

void HexEditorWidget::mouseReleaseEvent(QMouseEvent* event)
{
	assert(event);
	event->ignore();

	if (mouseSelecting)
	{
		mouseSelecting = false;
		event->accept();
	}
	updateEverything();
}

void HexEditorWidget::keyPressEvent(QKeyEvent* event)
{
	assert(event);
	event->ignore();
	
	switch (event->key())
	{
	case Qt::Key_Tab:
		if (focusedPane == HEX)
			focusedPane = CHARS;
		else if (focusedPane == CHARS)
			focusedPane = HEX;
			
		restartTimer();
		caretBlack = false; // Give immediate visual feedback to the user
		event->accept();
		break;
			
	case Qt::Key_Insert:
		if (caretMode == OVERWRITE)
			caretMode = INSERT;
		else if (caretMode == INSERT)
			caretMode = OVERWRITE;
			
		restartTimer();
		event->accept();
		break;
			
	case Qt::Key_Left:
	case Qt::Key_Right:
	case Qt::Key_Down:
	case Qt::Key_Up:
		directionKeyPress(event->modifiers(), event->key());
		event->accept();
		break;
		
	case Qt::Key_Backspace:
		deleteByteNearCaret(true);
		event->accept();
		break;
	
	case Qt::Key_Delete:
		deleteByteNearCaret(false);
		event->accept();
		break;

	case Qt::Key_PageUp:
		verticalScrollBar()->triggerAction(QAbstractSlider::SliderPageStepSub);
		break;

	case Qt::Key_PageDown:
		verticalScrollBar()->triggerAction(QAbstractSlider::SliderPageStepAdd);
		break;
		
	default:
		if (event->key() >= 0 && event->key() <= 255 && !mouseSelecting)
		{
			modifyByteAtCaret(event->key(), event->text());
			event->accept();
		}
		break;
	}

	updateEverything();
}

void HexEditorWidget::resizeEvent(QResizeEvent* event)
{
	assert(event);
	event->accept();
	updateFileSize();
}

void HexEditorWidget::paintEvent(QPaintEvent* event)
{
	//QuickBenchmark draw("draw");
	
	assert(event);
	QPainter p(viewport());
	p.setClipRect(event->rect());
	p.setClipping(true);

	drawDisplay(p, event->rect());
	
	//draw.stop();
}

QString HexEditorWidget::charString(unsigned char c) const
{
	QTextCodec* codec = QTextCodec::codecForName("IBM 850");
	if (c < 0x20)
	{
		return ".";
	}
	else
	{
		//return QString(QChar(c));
		return codec->toUnicode((const char*) &c, 1);
	}
}

void HexEditorWidget::clearEditorState()
{
	focusedPane = HEX;
	caretMode = OVERWRITE;
	caretPos = CaretPosition(0, LD);
	selCaretPos = CaretPosition(0, NONE);
	lastHitTestSucceeded = false;
	mouseSelecting = false;
	caretBlack = false;
	
	last.caretPos = CaretPosition(0, LD);
	last.selCaretPos = CaretPosition(0, NONE);
}

void HexEditorWidget::createFonts()
{
	int modifiedWeight;

#ifdef Q_WS_WIN
	Json::Value& fontJson = theme["fontWin"];
	//font = QFont("Lucida Console", 10);
	//modifiedWeight = QFont::DemiBold;
	font = QFont(
		fontJson.get("face", "Courier New").asString().c_str(),
		fontJson.get("size", 10).asInt()
	);
	modifiedWeight = fontJson.get("modifiedWeight", QFont::Bold).asInt();
#else
	Json::Value& fontJson = theme["fontNix"];
	font = QFont(
		fontJson.get("face", "Liberation Mono").asString().c_str(),
		fontJson.get("size", 10).asInt()
	);
	//font = QFont("Ubuntu Mono", 12);
	modifiedWeight = fontJson.get("modifiedWeight", QFont::Bold).asInt();
#endif

	font.setStyleHint(QFont::Courier);
	font.setStyleStrategy(QFont::PreferQuality);

	fontModified = QFont(font);
	fontModified.setWeight(modifiedWeight);
}

void HexEditorWidget::updateFileSize()
{
	// Doh. This is way too ugly
	totalBytes = doc ? doc->size() : 0;
	totalRows = totalBytes / bytesPerLine + 1;
	
	const unsigned int height = viewport()->height();
	const unsigned int addressHeight = QFontMetrics(font).height();

	verticalScrollBar()->setPageStep(10);
	verticalScrollBar()->setRange(0, totalRows - height / addressHeight);
}

void HexEditorWidget::updateLayout()
{
	using std::max;
	
	QFontMetrics fm(font);

	// Take the width of the largest digit (mostly for proportional fonts)
	hexDigitWidth = 0;
	for (uint i = 0; i < 16; i++)
	{
		const uint width = fm.width(QString::number(i, 16).toUpper());
		hexDigitWidth = max(hexDigitWidth, width);
	}

	// Create static text object which will help us render text faster
	for (uint i = 0; i < 16; i++)
	{
		QStaticText& st = digitStatics[i];
		st = QStaticText(QString::number(i, 16).toUpper());
		st.setTextFormat(Qt::PlainText);
		st.setTextOption(QTextOption(Qt::AlignHCenter | Qt::AlignVCenter));
		st.setPerformanceHint(QStaticText::AggressiveCaching);
		st.setTextWidth(hexDigitWidth);
		
		QStaticText& stMod = digitStaticsModified[i];
		stMod = QStaticText(QString::number(i, 16).toUpper());
		stMod.setTextFormat(Qt::PlainText);
		stMod.setTextOption(QTextOption(Qt::AlignHCenter | Qt::AlignVCenter));
		stMod.setPerformanceHint(QStaticText::AggressiveCaching);
		stMod.setTextWidth(hexDigitWidth);
	}

	// Take the width of the largest letter. We ignore the chars that
	// are greater than 127 because they can potentially skew the width.
	charWidth = 0;
	for (uint i = 'A'; i <= 'Z'; i++)
	{
		const uint width = fm.width(charString(i));
		charWidth = max(charWidth, width);
	}

	for (uint i = 0; i < 256; i++)
	{
		QStaticText& st = charStatics[i];
		st = QStaticText(charString(i));
		st.setTextFormat(Qt::PlainText);
		st.setTextOption(QTextOption(Qt::AlignHCenter | Qt::AlignVCenter));
		st.setPerformanceHint(QStaticText::AggressiveCaching);
		st.setTextWidth(charWidth);
		
		QStaticText& stMod = charStaticsModified[i];
		stMod = QStaticText(charString(i));
		stMod.setTextFormat(Qt::PlainText);
		stMod.setTextOption(QTextOption(Qt::AlignHCenter | Qt::AlignVCenter));
		stMod.setPerformanceHint(QStaticText::AggressiveCaching);
		stMod.setTextWidth(charWidth);
	}

	// Layout calculations
	lineHeight = fm.height();
	padding = 10;
	addressOffset = 0;
	//addressDigitCount = 8; // TODO Allow this to change with the file size
	addressWidth = hexDigitWidth * addressDigitCount;
	addressHexDividerOffset = addressOffset + addressWidth + padding;
	hexOffset = addressHexDividerOffset + padding;
	digitWidth = hexDigitWidth;
	hexDigitFullWidth = digitWidth * 2 + padding;
	hexWidth = (digitWidth * 2) * bytesPerLine + padding * (bytesPerLine - 1);
	hexCharsDividerOffset = hexOffset + hexWidth + padding;
	charsOffset = hexCharsDividerOffset + padding;
	charsWidth = charWidth * bytesPerLine;
	totalWidth = charsOffset + charsWidth + padding;

	// Various things
	selEdgePadding = padding / 2;
}

void HexEditorWidget::restartTimer()
{
	if (caretTimer != 0)
		killTimer(caretTimer);

	caretBlack = true;
	caretTimer = startTimer(500);
}

bool HexEditorWidget::shouldMoveFullByte(bool shift) const
{
	bool selecting = shift || mouseSelecting || hasSelection();
	return selecting || focusedPane == CHARS;
}

void HexEditorWidget::caretToSelectionStart()
{
	if (hasSelection())
	{
		caretPos.offset = selectionOffset();
		caretPos.inside = LD;
		clearSelection();
	}
}

void HexEditorWidget::directionKeyPress(Qt::KeyboardModifiers mods, int key)
{
	using std::min;
	using std::max;
	
	// Keep track of previous caret position the same and move a new
	// one.
	CaretPosition newCaretPos(caretPos.offset, caretPos.inside);

	// Will moving the cursor change the selection?
	bool willSelect = mods & Qt::ShiftModifier;

	// Determine if we should move an entire byte or just a nibble
	bool movesFullByte = shouldMoveFullByte(willSelect);

	// TODO Okay, there's a lot of code duplication here. Refactor?
	if (key == Qt::Key_Left)
	{
		if (movesFullByte)
		{
			if (newCaretPos.offset > 0)
				newCaretPos.offset -= 1;

			newCaretPos.inside = LD;
		}
		else if (newCaretPos.inside <= LD)
		{
			if (newCaretPos.offset > 0)
			{
				newCaretPos.offset -= 1;
				newCaretPos.inside = RD;
			}
		}
		else
		{
			newCaretPos.inside = LD;
		}
	}
	else if (key == Qt::Key_Right)
	{
		if (movesFullByte)
		{
			if (newCaretPos.offset < totalBytes)
				newCaretPos.offset += 1;
				
			newCaretPos.inside = LD;
		}
		else if (newCaretPos.inside >= RD)
		{
			if (newCaretPos.offset < totalBytes)
			{
				newCaretPos.offset += 1;
				newCaretPos.inside = LD;
			}
		}
		else
		{
			newCaretPos.inside = RD;
		}
	}
	else if (key == Qt::Key_Up)
	{
		if (newCaretPos.offset >= bytesPerLine)
		{
			newCaretPos.offset = newCaretPos.offset - bytesPerLine;
		}
	}
	else if (key == Qt::Key_Down)
	{
		if (newCaretPos.offset + bytesPerLine <= totalBytes)
		{
			newCaretPos.offset += bytesPerLine;
		}
	}

	// If we're at the end of the file, we always want stay in the
	// first nibble.
	if (newCaretPos.offset >= totalBytes)
		newCaretPos.inside = LD;

	if (newCaretPos.offset != caretPos.offset)
	{
		// If the shift key is down, modify the selection
		if (willSelect)
		{
			if (!hasSelection())
			{
				// Couldn't we use self.startSelection here?
				selCaretPos = CaretPosition(caretPos.offset, LD);
			}
		}
		else if (!mouseSelecting)
		{
			clearSelection();
		}
	}

	caretPos = newCaretPos;

	// The caret should pulse
	restartTimer();
	if (willSelect)
		caretBlack = false;

	scrollToCaret();
}

bool HexEditorWidget::modifyByteAtCaret(int key, const QString& text)
{
	using std::min;
	
	CaretPosition newCaretPos(caretPos.offset, caretPos.inside);
	
	// If no document is open we can't do anything
	if (!doc)
		return false;
		
	// If we're read only don't do anything
	// TODO Provide some indication that we're read only
	if (doc->isReadOnly())
	{
		statusMessage(tr("file is marked read-only"));
		return false;
	}
		
	// Are we at the end of file marker
	const bool atEnd = (newCaretPos.offset == totalBytes);
	
	if (focusedPane == HEX)
	{
		bool okay = false;
		const uint8_t value = text.toUInt(&okay, 16) & 0xF;
		
		// Return false if this isn't a hex char
		if (!okay)
			return false;
		
		// We want to start at the beginning of the selection
		caretToSelectionStart();
		
		// In some cases we need to read the existing byte
		BytesMap::Reader reader(doc->reader());
		uint8_t existingValue;
		
		reader.seek(newCaretPos.offset);
		reader.read(&existingValue, 1);
		
		// If we're in the second nibble of a byte, always overwrite
		if (caretPos.inside >= RD)
		{
			assert(!atEnd);
			
			// Overwrite low nibble
			const uint8_t b = (existingValue & 0xF0) | value;
			if (doc->overwrite(newCaretPos.offset, b))
			{
				newCaretPos.offset = min<size_t>(newCaretPos.offset + 1, totalBytes);
				newCaretPos.inside = LD;
			}
		}
		else if (caretMode == OVERWRITE && !atEnd)
		{
			// Overwrite high nibble
			const uint8_t b = (existingValue & 0x0F) | (value << 4);
			if (doc->overwrite(newCaretPos.offset, b))
			{
				newCaretPos.inside = RD;
			}
		}
		else if (caretMode == INSERT || atEnd)
		{
			// Set the high nibble, let the low nibble be 0
			const uint8_t b = value << 4;
			if (doc->insert(newCaretPos.offset, b))
			{
				updateFileSize();
				newCaretPos.inside = RD;
			}
		}
	}
	else if (focusedPane == CHARS)
	{
		// This will only take one byte of the character. In particular,
		// unicode characters will be completely broken.
		// TODO Insert/overwrite multiple bytes 
		const uint8_t value = text.toLocal8Bit().at(0);
		
		// We want to start at the beginning of the selection
		caretToSelectionStart();

		// Overwrite or insert
		bool success = false;
		if (caretMode == OVERWRITE && !atEnd)
		{
			success = doc->overwrite(newCaretPos.offset, value);
		}
		else if (caretMode == INSERT || atEnd)
		{
			success = doc->insert(newCaretPos.offset, value);
			updateFileSize();
		}

		if (success)
		{
			newCaretPos.offset = min<size_t>(newCaretPos.offset + 1, totalBytes);
			newCaretPos.inside = LD;
		}
	}
	
	caretPos = newCaretPos;
	restartTimer();
	scrollToCaret();
	return true;
}

void HexEditorWidget::deleteByteNearCaret(bool before, bool forceDelete)
{
	CaretPosition newCaretPos(caretPos.offset, caretPos.inside);
	
	// If no document is open we can't do anything
	if (!doc)
		return;
		
	// If we're read only don't do anything
	// TODO Provide some indication that we're read only
	if (doc->isReadOnly())
	{
		statusMessage(tr("file is marked read-only"));
		return;
	}
		
	if (caretMode == OVERWRITE && !forceDelete)
	{
		// TODO Alright, there's the question of whether we should
		// delete the selection here. The current behavior is rather
		// weird, but I'm not sure I want to delete anything in
		// overwrite mode.
		if (before)
		{
			// Move the caret to the left
			if (newCaretPos.offset > 0)
				newCaretPos.offset -= 1;

			newCaretPos.inside = LD;
			
			clearSelection();
		}
	}
	else if (caretMode == INSERT || forceDelete)
	{
		if (hasSelection())
		{
			doc->remove(selectionOffset(), selectionLength());
			
			newCaretPos = CaretPosition(selectionOffset(), LD);
			clearSelection();
		}
		else
		{
			// A special case: when the caret is between the two digits in
			// the hex pane, delete the current char not the previous. We
			// increment the offset here so that when we decrement it,
			// we'll be in the right place.
			if (before && focusedPane == HEX && newCaretPos.inside >= RD)
			{
				newCaretPos.offset += 1;
			}
			
			if (before)
			{
				// Move the caret to the left
				if (newCaretPos.offset > 0)
				{
					newCaretPos.offset -= 1;
				}
				else
				{
					// For the sake of consitency we have to do the same
					// things as when we exit this function.
					// TODO Restructure if-statement to make this not 
					// duplicated.
					restartTimer();
					scrollToCaret();
					return;
				}
			}
			
			doc->remove(newCaretPos.offset);
		}
		
		updateFileSize();
		
		newCaretPos.inside = LD;
	}
	
	caretPos = newCaretPos;
	restartTimer();
	scrollToCaret();
}

void HexEditorWidget::scrollToCaret()
{
	QScrollBar& scrollBar = *verticalScrollBar();

	// Determine whether or not the caret is visible
	const size_t firstVisibleRow = scrollBar.value();
	const uint visibleRowCount = viewport()->height() / lineHeight;
	const size_t lastVisibleRow = firstVisibleRow + visibleRowCount;
	const size_t caretRow = caretPos.offset / bytesPerLine;

	if (caretRow < firstVisibleRow)
	{
		scrollBar.setValue(caretRow);
	}
	else if (caretRow >= lastVisibleRow)
	{
		scrollBar.setValue(firstVisibleRow + caretRow - lastVisibleRow + 1);
	}
}

QBrush HexEditorWidget::themeBrush(const QString& name, QBrush def)
{
	Json::Value& brushes = theme["brushes"];
	if (brushes.isMember(name.toUtf8().constData()))
	{
		Json::Value brush = brushes[name.toUtf8().constData()];
		QColor color = QColor(brush[0].asString().c_str());
		color.setAlpha(brush.get(1, 0xFF).asInt());
		return QBrush(color);
	}
	else
	{
		// TODO This is only for testing {
		Json::Value brush;
		brush[0u] = def.color().name().toUtf8().constData();
		brush[1u] = def.color().alpha();
		brushes[name.toUtf8().constData()] = brush;
		// }
		return def;
	}
}

void HexEditorWidget::createBrushes()
{	
	backgroundBrush = themeBrush("backgroundBrush", QBrush(QColor(255, 255, 255, 255)));
	stripeBrush = themeBrush("stripeBrush", QBrush(QColor(0, 255, 0, 20)));
	addressBrush = themeBrush("addressBrush", QBrush(QColor(192+32, 192+32, 192+32)));
	charsBrush = themeBrush("charsBrush", QBrush(QColor(192+32, 192+32, 255)));
	caretBrush = themeBrush("caretBrush", QBrush(QColor(0, 0, 0)));
	selectionBrush = themeBrush("selectionBrush", QBrush(QColor(0, 60, 255, 127)));
	selectionBrushUnfocused = themeBrush("selectionBrushUnfocused", QBrush(QColor(100, 100, 127, 127)));
	/*selectionBrush = QBrush(QColor(0, 60, 255, 127), Qt::Dense4Pattern);
	selectionBrushUnfocused = QBrush(QColor(167, 167, 167, 127), Qt::Dense4Pattern);*/
	markBrush = themeBrush("markBrush", QBrush(QColor(255, 0, 0)));
}

QPen HexEditorWidget::themePen(const QString& name, QPen def)
{
	Json::Value& pens = theme["pens"];
	if (pens.isMember(name.toUtf8().constData()))
	{
		Json::Value pen = pens[name.toUtf8().constData()];
		QColor color = QColor(pen[0].asString().c_str());
		color.setAlpha(pen.get(1, 0xFF).asInt());
		QPen penObj(color);
		//penObj.setWidthF(pen.get(2, 1.0).asFloat());
		return penObj;
	}
	else
	{
		// TODO This is only for testing {
		Json::Value pen;
		pen[0u] = def.color().name().toUtf8().constData();
		pen[1u] = def.color().alpha();
		pen[2u] = def.widthF();
		pens[name.toUtf8().constData()] = pen;
		// }
		return def;
	}
}

void HexEditorWidget::createPens()
{
	// TODO Add a modified/unmodified pen
	
	thinDividerPen = themePen("thinDividerPen", QPen(QColor(192, 192, 192)));
	thinDividerPen.setWidthF(1);
	addressPen = themePen("addressPen", QPen(QColor(0, 0, 0)));
	bytePen = themePen("bytePen", QPen(QColor(0, 0, 0)));
	byteCaretPen = themePen("byteCaretPen", QPen(QColor(255, 255, 255)));
	byteInsertPen = themePen("byteInsertPen", QPen(QColor(0, 0, 0)));
	byteInsertPen.setWidthF(1.75);
	charPen = themePen("charPen", QPen(QColor(0, 0, 0)));
	charCaretPen = themePen("charCaretPen", QPen(QColor(255, 255, 255)));
	charInsertPen = themePen("charInsertPen", QPen(QColor(0, 0, 0)));
	charInsertPen.setWidthF(1.75);
	sepLinePen = themePen("sepLinePen", QPen(QColor(192, 192, 192)));
	sepLinePen.setWidthF(2);
	selectionBorderPen = themePen("selectionBorderPen", QPen(QColor(0, 60, 255, 255)));
	selectionBorderPenUnfocused = themePen("selectionBorderPenUnfocused", QPen(QColor(0, 60, 255, 192)));
}

struct HexEditorWidget::DrawStruct
{
	size_t topVisibleRow;
	uint visibleRowCount;
	size_t vertScroll;
	uint visiblePixelsToFileEnd;
	
	uint drawnRowCount;
	size_t topDrawnRow;
	size_t endRow;
	
	uint lineTop;
	size_t offsetTop;
};

void HexEditorWidget::drawDisplay(QPainter& p, const QRect& rect)
{
	using std::min;
	using std::max;
	
	p.setFont(font);

	// We make some of the variables part of a struct so that we don't
	// have to pass them to every child function.
	DrawStruct ds;
	
	// Perhaps this should go somewhere else? I'm too tired
	totalHeight = totalRows * lineHeight;

	// Calculate how much of the screen we have to draw
	ds.topVisibleRow = verticalScrollBar()->value();
	ds.visibleRowCount = viewport()->height() / lineHeight + 1;
	ds.vertScroll = ds.topVisibleRow * lineHeight;

	// How many vertical pixels are shown, but stay less than the total number of pixels
	ds.visiblePixelsToFileEnd = uint(min<size_t>(totalHeight - ds.vertScroll, viewport()->height()));
	
	// Determine which rows we need to iterate over
	ds.drawnRowCount = rect.height() / lineHeight + 1;
	ds.topDrawnRow = ds.topVisibleRow + max((int) (rect.y() / lineHeight) - 1, 0);
	ds.endRow = min(totalRows, ds.topDrawnRow + ds.drawnRowCount);

	ds.drawnRowCount = ds.endRow - ds.topDrawnRow;
	ds.lineTop = (ds.topDrawnRow - ds.topVisibleRow) * lineHeight;
	ds.offsetTop = ds.topDrawnRow * bytesPerLine;

	//QuickBenchmark backDiv("backDiv");
	
	p.fillRect(rect, backgroundBrush);
	
	// Draw things
	p.setRenderHints(QPainter::TextAntialiasing, true);
	drawBackgroundStripes(p, ds);
	
	// We want thin lines, so unfortunately we have to disable
	// antialiasing temporarily.
	p.setRenderHints(QPainter::Antialiasing, false);
	drawThinDividers(p, ds);
	p.setRenderHints(QPainter::Antialiasing, true);

	drawAddressBackground(p, ds);
	drawAddressDivider(p, ds);
	drawCharsHexDivider(p, ds);
	//backDiv.stop();
	//QuickBenchmark addresses("addresses");
	drawAddresses(p, ds);
	//addresses.stop();

	drawMarks(p, ds);

	//QuickBenchmark selection("selection");
	drawSelection(p, ds);
	//selection.stop();

	//QuickBenchmark hexPane("hexPane");
	drawHexPane(p, ds);
	//hexPane.stop();

	//QuickBenchmark charPane("charPane");
	drawCharsPane(p, ds);
	//charPane.stop();
}

void HexEditorWidget::drawBackgroundStripes(QPainter& p, const DrawStruct& ds)
{
	for (uint i = (ds.topDrawnRow + 1) & 1; i < ds.drawnRowCount; i += 2)
	{
		const QRectF rect(0, i * lineHeight, totalWidth, lineHeight);
		p.fillRect(rect, stripeBrush);
	}
}

void HexEditorWidget::drawThinDividers(QPainter& p, const DrawStruct& ds)
{
	p.setPen(thinDividerPen);
	for (uint i = 4; i < bytesPerLine; i += 4)
	{
		const uint left = hexDividerOffset(i);
		p.drawLine(left, 0, left, ds.visiblePixelsToFileEnd);
	}
}

void HexEditorWidget::drawAddressBackground(QPainter& p, const DrawStruct& ds)
{
	const uint left = addressHexDividerOffset;
	const QRectF rect(0, 0, left, ds.visiblePixelsToFileEnd + 1);
	p.fillRect(rect, addressBrush);
}

void HexEditorWidget::drawAddressDivider(QPainter& p, const DrawStruct& ds)
{
	const uint left = addressHexDividerOffset;
	p.setPen(sepLinePen);
	p.drawLine(left + 1, 0, left + 1, ds.visiblePixelsToFileEnd);
}

void HexEditorWidget::drawCharsHexDivider(QPainter& p, const DrawStruct& ds)
{
	const uint left = hexCharsDividerOffset;
	p.setPen(sepLinePen);
	p.drawLine(left - 1, 0, left - 1, ds.visiblePixelsToFileEnd);
	p.drawLine(left + 2, 0, left + 2, ds.visiblePixelsToFileEnd);
}

void HexEditorWidget::drawAddresses(QPainter& p, const DrawStruct& ds)
{
	p.setPen(addressPen);

	// This is a lot messier than I would like
	QString address;
	QTextStream textStream(&address);
	textStream.setIntegerBase(16);
	textStream.setFieldWidth(addressDigitCount);
	textStream.setPadChar('0');
	textStream.setNumberFlags(textStream.numberFlags() | QTextStream::UppercaseDigits);

	// Iterate over all rows
	uint line = ds.lineTop;
	size_t offset = ds.offsetTop;
	
	for (size_t row = ds.topDrawnRow; row < ds.endRow; row++)
	{
		address.clear();
		textStream << offset;
		
		const QRectF rect(
			addressOffset,
			line,
			addressHexDividerOffset,
			lineHeight
		);

		p.drawText(rect, Qt::AlignHCenter | Qt::AlignTop, address);
			
		line += lineHeight;
		offset += bytesPerLine;
	}
}

void HexEditorWidget::drawMarks(QPainter& p, const DrawStruct& ds)
{
	using std::min;

	// TODO This should not be here
	//MarksHierarchy marksHier;
	//marksHier.build(doc->marks().all());
	recolorMarks(doc->marksHierarchy().root());
	
	// Determine the window of marks to draw. We give it an extra row of slack
	// on the top and bottom.
	size_t startOffset = bytesPerLine * (ds.topDrawnRow > 0 ? ds.topDrawnRow - 1 : ds.topDrawnRow);
	size_t endOffset = bytesPerLine * (ds.endRow + 1);
	vector<MarkPtr> marks = doc->marks().range(startOffset, endOffset);
	
	// TODO Draw the butts

	for (const MarkPtr& mark : marks)
	{
		// Get the mark's color
		QColor color(mark->actualColor());
		color.setAlpha((mark->actualColor() >> 24) & 0xFF);

		// Don't allow marks to draw off the screen
		size_t offset = mark->offset();
		size_t endingOffset = mark->endingOffset();

		if (offset > totalBytes)
			continue;

		offset = min(offset, totalBytes);
		endingOffset = min(endingOffset, totalBytes);
		
		// Draw the mark in the hex pane
		HexMarkPathGen hexMarkPathGen(*this, ds.vertScroll);
		makeRangePath(offset, endingOffset, hexMarkPathGen);
		p.setPen(Qt::transparent);
		p.setBrush(color);
		p.drawPath(hexMarkPathGen.getPath());

		// Draw the mark in the chars pane
		CharsMarkPathGen charsMarkPathGen(*this, ds.vertScroll);
		makeRangePath(offset, endingOffset, charsMarkPathGen);
		p.drawPath(charsMarkPathGen.getPath());
	}
}

void HexEditorWidget::drawHexPane(QPainter& p, const DrawStruct& ds)
{		
	// Both nibbles
	QString nibbleChars[2];
	
	// Iterate over all rows
	uint line = ds.lineTop;
	size_t offset = ds.offsetTop;

	// Have to check
	if (!doc)
		return;

	BytesMap::Reader reader(doc->reader());
	reader.seek(offset);

	// Iterate through each byte
	for (size_t row = ds.topDrawnRow; row < ds.endRow; row++)
	{
		uint j;
		for (j = 0; j < bytesPerLine && offset <= totalBytes; j++)
		{
			uint8_t byte = '\0';
			bool showAsModified = false;
			if (offset < totalBytes)
				reader.read(&byte, 1, &showAsModified);

			// Split the byte into two nibbles
			const uint nibbleValue[2] = {
				(byte & 0xF0u) >> 4,
				(byte & 0x0Fu)
			};

			// Convert the byte into two separate nibble strings
			nibbleChars[0] = QString::number(nibbleValue[0], 16).toUpper();
			nibbleChars[1] = QString::number(nibbleValue[1], 16).toUpper();
						
			// Determine on which nibble the caret is
			int nonCaretNibble;
			if (caretPos.inside == RP || caretPos.inside == RD)
				nonCaretNibble = 0;
			else
				nonCaretNibble = 1;
				
			// If the chars pane is focused, draw the caret over both
			// nibbles.
			if (shouldMoveFullByte(false) && caretMode == OVERWRITE)
				nonCaretNibble = -1;
			
			for (int nibble = 0; nibble < 2; nibble++)
			{
				// Rectangle which the digit is to be drawn in
				QRectF rect(
					hexByteOffset(j, nibble),
					line,
					digitWidth,
					lineHeight
				);
				
				// Set the pen, which might be changed by drawing the
				// caret.
				p.setPen(bytePen);

				// Draw the caret
				if (offset == caretPos.offset &&
					nibble != nonCaretNibble &&
					(((caretBlack || !hasFocus()) && !mouseSelecting) || focusedPane != HEX))
				{
					drawHexPaneCaret(p, ds, line, j, nibble);
				}
				
#if QT_VERSION < 0x040800
				// For some reason static text using raster mode on
				// Qt 4.7 creates a bigger bounding rect that it should
				// be. We move the text slightly to compensate for this.
				// TODO Check for raster mode?
				rect.adjust(2, 2, 0, 0);
#endif

				// If we're selected draw with white
				if (inSelection(offset))
				{
					p.setPen(byteCaretPen);
				}

				// Draw the hex bytes or end of file
				if (offset < totalBytes)
				{
					if (drawTextStatics)
					{
						if (showAsModified)
						{
							p.setFont(fontModified);
							p.drawStaticText(rect.topLeft(), digitStaticsModified[nibbleValue[nibble]]);
							p.setFont(font);
						}
						else
						{
							p.drawStaticText(rect.topLeft(), digitStatics[nibbleValue[nibble]]);
						}
					}
					else
					{
						p.drawText(rect, Qt::AlignLeft | Qt::AlignTop, nibbleChars[nibble]);
					}
				}
				else if (offset == totalBytes)
				{
					drawHexPaneEndFile(p, ds, line, j, nibble);
				}
				
			}
			offset++;
		}
		line += lineHeight;
	}
}

void HexEditorWidget::drawHexPaneCaret(QPainter& p, const DrawStruct& ds, uint line, uint j, int nibble)
{
	if (caretMode == OVERWRITE)
	{
		const QRectF rect(
			hexByteOffset(j, nibble),
			line,
			digitWidth,
			lineHeight
		);
		
		p.fillRect(rect, caretBrush);
		p.setPen(byteCaretPen);
	}
	else if (caretMode == INSERT)
	{
		const QRectF rect(
			hexByteOffset(j, nibble),
			line + 1,
			0,
			lineHeight - 1
		);
		
		p.setPen(byteInsertPen);
		p.drawLine(rect.topLeft(), rect.bottomLeft());
		p.setPen(bytePen);
	}
}

void HexEditorWidget::drawHexPaneEndFile(QPainter& p, const DrawStruct& ds, uint line, uint j, int nibble)
{
	const uint h = line + lineHeight - 3;
	const uint extra = 0;
	QPointF p1; 
	QPointF p2;
	
	if (nibble == 0)
	{
		p1 = QPointF(hexByteOffset(j, 0) + extra, h);
		p2 = QPointF(hexByteOffset(j, 1), h);
	}
	else
	{
		p1 = QPointF(hexByteOffset(j, 1) + extra, h);
		p2 = QPointF(hexByteOffset(j, 2) - extra, h);
	}
	
	// Save the old render hints and disable antialiasing
	QPainter::RenderHints old = p.renderHints();
	p.setRenderHints(QPainter::Antialiasing, false);
	
	p.drawLine(p1, p2);
	
	p.setRenderHints(old);
}

void HexEditorWidget::drawCharsPane(QPainter& p, const DrawStruct& ds)
{
	// Iterate over all rows
	uint line = ds.lineTop;
	size_t offset = ds.offsetTop;

	// Have to check
	if (!doc)
		return;

	BytesMap::Reader reader(doc->reader());
	reader.seek(offset);

	// Iterate through each byte
	for (size_t row = ds.topDrawnRow; row < ds.endRow; row++)
	{
		for (uint j = 0; j < bytesPerLine && offset <= totalBytes; j++)
		{
			unsigned char byte = '\0';
			bool showAsModified = false;
			if (offset < totalBytes)
				reader.read(&byte, 1, &showAsModified);

			QRectF rect(
				charsByteOffset(j),
				line,
				charWidth,
				lineHeight
			);
			
			// Set the pen in case the caret drawing function doesn't
			p.setPen(charPen);

			// Decide whether or not to draw the caret
			if (offset == caretPos.offset && ((caretBlack || !hasFocus()) || focusedPane != CHARS))
			{
				drawCharsPaneCaret(p, ds, line, j);
			}

#if QT_VERSION < 0x040800
			// TODO Is this necessary?
			rect.adjust(2, 2, 0, 0);
#endif

			// If we're selected draw with white
			if (inSelection(offset))
			{
				p.setPen(charCaretPen);
			}

			// Draw the character or end of file
			if (offset < totalBytes)
			{
				if (drawTextStatics)
				{
					if (showAsModified)
					{
						p.setFont(fontModified);
						p.drawStaticText(rect.topLeft(), charStaticsModified[(uint) byte & 0xFF]);
						p.setFont(font);
					}
					else
					{
						p.drawStaticText(rect.topLeft(), charStatics[(uint) byte & 0xFF]);
					}
				}
				else
				{
					p.drawText(rect, Qt::AlignLeft | Qt::AlignTop, charString(byte));
				}
			}
			else if (offset == totalBytes)
			{
				drawCharsPaneEndFile(p, ds, line, j);
			}
			
			offset++;
		}
		line += lineHeight;
	}
}

void HexEditorWidget::drawCharsPaneCaret(QPainter& p, const DrawStruct& ds, uint line, uint j)
{
	if (caretMode == OVERWRITE)
	{
		const QRectF rect(
			charsByteOffset(j),
			line,
			charWidth,
			lineHeight
		);
		
		p.fillRect(rect, caretBrush);
		p.setPen(charCaretPen);
	}
	else if (caretMode == INSERT)
	{
		const QRectF rect(
			charsByteOffset(j),
			line + 1,
			0,
			lineHeight - 1
		);
		
		p.setPen(charInsertPen);
		p.drawLine(rect.topLeft(), rect.bottomLeft());
		p.setPen(charPen);
	}
}

void HexEditorWidget::setSelectionPenAndBrush(QPainter& p, Pane currentPane)
{
	if (focusedPane == currentPane)
	{
		p.setPen(selectionBorderPen);
		p.setBrush(selectionBrush);
	}
	else
	{
		p.setPen(selectionBorderPenUnfocused);
		p.setBrush(selectionBrushUnfocused);
	}
}

void HexEditorWidget::drawCharsPaneEndFile(QPainter& p, const DrawStruct& ds, uint line, uint j)
{
	const uint h = line + lineHeight - 3;
	QPointF p1(charsByteOffset(j), h);
	QPointF p2(charsByteOffset(j + 1), h);
	
	// Save the old render hints and disable antialiasing
	QPainter::RenderHints old = p.renderHints();
	p.setRenderHints(QPainter::Antialiasing, false);
	
	p.drawLine(p1, p2);
	
	p.setRenderHints(old);
}

void HexEditorWidget::drawSelection(QPainter& p, const DrawStruct& ds)
{
	if (hasSelection())
	{
		Selection sel = selection();

		// Draw the selection in the hex pane
		HexSelPathGen hexSelPathGen(*this, ds.vertScroll);
		makeRangePath(sel.first, sel.second, hexSelPathGen);
		setSelectionPenAndBrush(p, HEX);
		p.drawPath(hexSelPathGen.getPath());

		// Draw the selection in the chars pane
		CharsSelPathGen charsSelPathGen(*this, ds.vertScroll);
		makeRangePath(sel.first, sel.second, charsSelPathGen);
		setSelectionPenAndBrush(p, CHARS);
		p.drawPath(charsSelPathGen.getPath());
	}
}

void HexEditorWidget::makeRangePath(size_t a, size_t b, RangePathGen& output) const
{
	using std::swap;
	
	// In case the coordinates are in the wrong order
	if (b < a)
		swap(a, b);

	// Calculate the top and bottom rows and chomp, which is the
	// size of the piece that is "chomped" out of the left top and
	// also the right bottom.
	const size_t chompTop = a % bytesPerLine;
	const size_t topRow = (a - chompTop) / bytesPerLine;

	const size_t chompBottom = (bytesPerLine - 1) - (b % bytesPerLine);
	const size_t bottomRow = (b + chompBottom) / bytesPerLine;
	
	// Now for the cases
	if (topRow == bottomRow)
	{
		// The selection is simply a rectangle
		output.leftPoint(chompTop, topRow + 1, RangePathGen::BL);
		output.leftPoint(chompTop, topRow, RangePathGen::TL);
		output.rightPoint(chompBottom, topRow, RangePathGen::TR);
		output.rightPoint(chompBottom, topRow + 1, RangePathGen::BR);
		output.closeSubpath();
	}
	else if (b - a + 1 <= bytesPerLine)
	{
		// The selection is composed of two disconnected parts on
		// separate rows.
		output.leftPoint(chompTop, topRow + 1, RangePathGen::BL);
		output.leftPoint(chompTop, topRow, RangePathGen::TL);
		output.rightPoint(0, topRow, RangePathGen::TR);
		output.rightPoint(0, topRow + 1, RangePathGen::BR);
		output.closeSubpath();

		output.leftPoint(0, bottomRow + 1, RangePathGen::BL);
		output.leftPoint(0, bottomRow, RangePathGen::TL);
		output.rightPoint(chompBottom, bottomRow, RangePathGen::TR);
		output.rightPoint(chompBottom, bottomRow + 1, RangePathGen::BR);
		output.closeSubpath();
	}
	else
	{
		// The selection has multiple rows
		if (chompTop > 0)
		{
			output.leftPoint(0, topRow + 1, RangePathGen::TL);
			output.leftPoint(chompTop, topRow + 1);
		}

		output.leftPoint(chompTop, topRow, RangePathGen::TL);
		output.rightPoint(0, topRow, RangePathGen::TR);

		if (chompBottom > 0)
		{
			output.rightPoint(0, bottomRow, RangePathGen::BR);
			output.rightPoint(chompBottom, bottomRow);
		}

		output.rightPoint(chompBottom, bottomRow + 1, RangePathGen::BR);
		output.leftPoint(0, bottomRow + 1, RangePathGen::BL);
		output.closeSubpath();
	}
}

void HexEditorWidget::updateEverything()
{
	if (!doc)
		return;
	
	last.caretPos = caretPos;
	last.selCaretPos = selCaretPos;

	// Update actions/menus
	bool hasSel = hasSelection();
	cutAct->setEnabled(hasSel);
	copyAct->setEnabled(hasSel);
	
	stateChanged();
	viewport()->update();
}

void HexEditorWidget::updateCaretAndSelection()
{
	// If the state has changed at all
	if (caretPos != last.caretPos || selCaretPos != last.selCaretPos)
	{
		// This will update the last struct for us
		updateEverything();
	}
}

void HexEditorWidget::recolorMarks(MarksHierarchy::Node& node) const
{
	MarkPtr parent = node.ptr();
	
	// Blend current node with background color
	if (parent)
	{
		QColor a(parent->color());
		QColor b(Qt::white);
		
		QColor finalColor(
			(a.red() + b.red()) / 2,
			(a.green() + b.green()) / 2,
			(a.blue() + b.blue()) / 2
		);
		
		a.setAlpha(128);
		
		//parent->setActualColor(finalColor.rgba());
		parent->setActualColor(a.rgba());
	}
	
	for (MarksHierarchy::Node& child : node)
	{
		MarkPtr mark = child.ptr();
		
		// Don't recolor top-level marks
		if (parent)
		{
			QColor a(parent->color());
			QColor b(Qt::white);
			
			QColor finalColor(
				(a.red() + b.red() * 2) / 3,
				(a.green() + b.green() * 2) / 3,
				(a.blue() + b.blue() * 2) / 3
			);
			
			//mark->setActualColor(finalColor.rgba());
			
			a.setAlpha(85);
			parent->setActualColor(a.rgba());
		}
		
		recolorMarks(child);
	}
}

void HexEditorWidget::handleTooltip(QHelpEvent* event)
{
	std::pair<Pane, CaretPosition> result;
	size_t offset;
	
	result = hitTest(event->x(), event->y());
	offset = result.second.offset;
	
	if ((result.first != NOPANE) && (offset < doc->size()))
	{
		// Find all marks at the offset
		std::vector<MarkPtr> marks;
		marks = doc->marks().range(offset, offset);

		if (marks.size() >= 1)
		{
			MarkPtr mark = marks.back();
			QString text;

			text = tr("<font color='%2'>&#9632;</font> <b>%3</b> %1").arg(
				mark->name().c_str(),
				QColor(mark->color()).name(),
				mark->defaultInterpretation()->name().c_str()
			);
			
			QToolTip::showText(event->globalPos(), text, this);
		}
		else
		{
			QToolTip::hideText();
		}
	}
	else
	{
		QToolTip::hideText();
	}
}

void HexEditorWidget::createContextMenu()
{
	// Create actions
	cutAct = new QAction(tr("Cu&t"), this);
	cutAct->setShortcuts(QKeySequence::Cut);
	connect(cutAct, SIGNAL(triggered()), this, SLOT(cutSlot()));

	copyAct = new QAction(tr("&Copy"), this);
	copyAct->setShortcuts(QKeySequence::Copy);
	connect(copyAct, SIGNAL(triggered()), this, SLOT(copySlot()));

	pasteAct = new QAction(tr("&Paste"), this);
	pasteAct->setShortcuts(QKeySequence::Paste);
	connect(pasteAct, SIGNAL(triggered()), this, SLOT(pasteSlot()));
	
	deleteAct = new QAction(tr("&Delete"), this);
	deleteAct->setShortcuts(QKeySequence::Delete);
	connect(deleteAct, SIGNAL(triggered()), this, SLOT(deleteSlot()));

	selectAllAct = new QAction(tr("Select &All"), this);
	selectAllAct->setShortcuts(QKeySequence::SelectAll);
	connect(selectAllAct, SIGNAL(triggered()), this, SLOT(selectAllSlot()));
	
	insertModeAct = new QAction(tr("&Insert Mode"), this);
	//insertModeAct->setShortcut(QKeySequence(tr("Ins")));
	connect(insertModeAct, SIGNAL(triggered()), this, SLOT(insertModeSlot()));

	// Make sure the shortcuts get associated
	if (!customContextMenuFlag)
	{
		addAction(cutAct);
		addAction(copyAct);
		addAction(pasteAct);
		addAction(deleteAct);
		addAction(selectAllAct);
		addAction(insertModeAct);
	}

	// Create the context menu itself
	contextMenu = new QMenu(this);
	contextMenu->addAction(cutAct);
	contextMenu->addAction(copyAct);
	contextMenu->addAction(pasteAct);
	contextMenu->addAction(deleteAct);
	contextMenu->addSeparator();
	contextMenu->addAction(selectAllAct);
	contextMenu->addAction(insertModeAct);
}

void HexEditorWidget::setCustomContextMenu(bool flag)
{
	customContextMenuFlag = flag;
}

bool HexEditorWidget::isCustomContextMenu() const
{
	return customContextMenuFlag;
}

void HexEditorWidget::cutSlot()
{
	if (copyPasteHandler.copy())
	{
		deleteBytes();
	}
}

void HexEditorWidget::copySlot()
{
	copyPasteHandler.copy();
}

void HexEditorWidget::pasteSlot()
{
	copyPasteHandler.paste();
}

void HexEditorWidget::deleteSlot()
{
	deleteBytes();
}

void HexEditorWidget::selectAllSlot()
{
	setSelection(HexEditorWidget::Selection(0, doc->size()));
	updateCaretAndSelection();
}

void HexEditorWidget::insertModeSlot()
{
	// Considered an editor function, pass it to the key handler
	QKeyEvent ke(QEvent::KeyPress, Qt::Key_Insert, 0);
	keyPressEvent(&ke);
}

