/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Helper classes to create a selection or mark
 */
#include "RangePathGen.hpp"
#include "HexEditorWidget.hpp"
#include <iostream>
#include <cassert>


RangePathGen::RangePathGen(const HexEditorWidget& editor, size_t vertScroll):
	editor(editor),
	vertScroll(vertScroll),
	needMoveTo(true)
{
}

void RangePathGen::closeSubpath()
{
	path.closeSubpath();
	needMoveTo = true;
}

QPainterPath& RangePathGen::getPath()
{
	return path;
}

ptrdiff_t RangePathGen::lineY(size_t row) const
{
	// We make sure to cast vertScroll to a ptrdiff_t otherwise that
	// expression will be evaluated unsigned.
	return ptrdiff_t(row) * ptrdiff_t(editor.lineHeight) - ptrdiff_t(vertScroll);
}


SelPathGen::SelPathGen(const HexEditorWidget& editor, size_t vertScroll):
	RangePathGen(editor, vertScroll)
{
}

void SelPathGen::closeSubpath()
{
	path.addPolygon(polygon);
	polygon = QPolygonF();
	path.closeSubpath();
	needMoveTo = true;
}

HexSelPathGen::HexSelPathGen(const HexEditorWidget& editor, size_t vertScroll):
	SelPathGen(editor, vertScroll)
{
}

void HexSelPathGen::leftPoint(size_t byteOffset, size_t row, Rounded rounded)
{
	polygon.append(QPointF(
		editor.hexByteOffset(byteOffset, 0) - editor.selEdgePadding,
		lineY(row)
	));
}

void HexSelPathGen::rightPoint(size_t byteOffset, size_t row, Rounded rounded)
{
	byteOffset = editor.bytesPerLine - 1 - byteOffset;
	polygon.append(QPointF(
		editor.hexByteOffset(byteOffset, 2) + editor.selEdgePadding,
		lineY(row)
	));
}


CharsSelPathGen::CharsSelPathGen(const HexEditorWidget& editor, size_t vertScroll):
	SelPathGen(editor, vertScroll)
{
}

void CharsSelPathGen::leftPoint(size_t byteOffset, size_t row, Rounded rounded)
{
	polygon.append(QPointF(
		editor.charsByteOffset(byteOffset),
		lineY(row)
	));
}

void CharsSelPathGen::rightPoint(size_t byteOffset, size_t row, Rounded rounded)
{
	byteOffset = editor.bytesPerLine - 1 - byteOffset;
	polygon.append(QPointF(
		editor.charsByteOffset(byteOffset + 1),
		lineY(row)
	));
}


MarkPathGen::MarkPathGen(const HexEditorWidget& editor, size_t vertScroll):
	RangePathGen(editor, vertScroll)
{
}

void MarkPathGen::addPoint(QPointF point, Rounded rounded, float arcW, float arcH)
{
	// If we're not drawing a rounded corner, add a line
	if (rounded == NONE)
	{
		if (needMoveTo)
		{
			path.moveTo(point);
			needMoveTo = false;
		}
		else
		{
			path.lineTo(point);
		}
		return;
	}
		
	// Otherwise add an arc
	float rw = arcW;
	float rh = arcH;
	float angle = 0.0f;
	QPointF start;
	QRectF r;
	r.setTopLeft(point);

	switch (rounded)
	{
	case BL:
		angle = -90.0f;
		r.setWidth(rw);
		r.setHeight(-rh);
		start = QPointF(r.left() + r.width() / 2, r.top());
		break;
		
	case TL:
		angle = -180.0f;
		r.setWidth(rw);
		r.setHeight(rh);
		start = QPointF(r.left(), r.top() + r.height() / 2);
		break;
		
	case TR:
		angle = -270.0f;
		r.setWidth(-rw);
		r.setHeight(rh);
		start = QPointF(r.left() + r.width() / 2, r.top());
		break;
		
	case BR:
		angle = -360.0f;
		r.setWidth(-rw);
		r.setHeight(-rh);
		start = QPointF(r.left(), r.top() + r.height() / 2);
		break;

	default:
		assert(0);
		break;
	}
	
	// Move the pen to the starting point if there are no elements
	if (needMoveTo)
	{
		path.moveTo(start);
		needMoveTo = false;
	}
		
	path.arcTo(r.normalized(), angle, -90.0);
}


HexMarkPathGen::HexMarkPathGen(const HexEditorWidget& editor, size_t vertScroll):
	MarkPathGen(editor, vertScroll)
{
}

void HexMarkPathGen::leftPoint(size_t byteOffset, size_t row, Rounded rounded)
{
	QPointF point(
		editor.hexByteOffset(byteOffset, 0) - editor.selEdgePadding,
		lineY(row)
	);
		
	addPoint(point, rounded, 6.0f, 6.0f);
}

void HexMarkPathGen::rightPoint(size_t byteOffset, size_t row, Rounded rounded)
{
	byteOffset = editor.bytesPerLine - 1 - byteOffset;
	QPointF point(
		editor.hexByteOffset(byteOffset, 2) + editor.selEdgePadding,
		lineY(row)
	);
		
	addPoint(point, rounded, 6.0f, 6.0f);
}


CharsMarkPathGen::CharsMarkPathGen(const HexEditorWidget& editor, size_t vertScroll):
	MarkPathGen(editor, vertScroll)
{
}

void CharsMarkPathGen::leftPoint(size_t byteOffset, size_t row, Rounded rounded)
{
	QPointF point(
		editor.charsByteOffset(byteOffset),
		lineY(row)
	);
		
	addPoint(point, rounded, 4.0f, 4.0f);
}

void CharsMarkPathGen::rightPoint(size_t byteOffset, size_t row, Rounded rounded)
{
	byteOffset = editor.bytesPerLine - 1 - byteOffset;
	QPointF point(
		editor.charsByteOffset(byteOffset + 1),
		lineY(row)
	);
		
	addPoint(point, rounded, 4.0f, 4.0f);
}
