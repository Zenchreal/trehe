/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Helper classes to create a selection or mark
 */
#pragma once
#include <cstddef>
#include <utility>
#include <QPainterPath>
#include <QPolygon>

class HexEditorWidget;


/**
 * The base class from which all other 'range paths' are derived. A
 * range path is any kind of fill which starts at one offset and ends
 * at another. For example a selection or a mark. These classes contain
 * the drawing code.
 */
class RangePathGen
{
public:
	enum Rounded
	{
		NONE,
		BL,
		TL,
		TR,
		BR
	};
	
	RangePathGen(const HexEditorWidget& editor, size_t vertScroll);
	virtual void leftPoint(size_t byteOffset, size_t row, Rounded rounded=NONE) = 0;
	virtual void rightPoint(size_t byteOffset, size_t row, Rounded rounded=NONE) = 0;
	virtual void closeSubpath();
	QPainterPath& getPath();

protected:
	ptrdiff_t lineY(size_t row) const;

protected:
	const HexEditorWidget& editor;
	size_t vertScroll;
	QPainterPath path;
	bool needMoveTo;
};


class SelPathGen : public RangePathGen
{
public:
	SelPathGen(const HexEditorWidget& editor, size_t vertScroll);
	virtual void closeSubpath();

protected:
	QPolygonF polygon;
};


class HexSelPathGen : public SelPathGen
{
public:
	HexSelPathGen(const HexEditorWidget& editor, size_t vertScroll);
	virtual void leftPoint(size_t byteOffset, size_t row, Rounded rounded=NONE);
	virtual void rightPoint(size_t byteOffset, size_t row, Rounded rounded=NONE);
};


class CharsSelPathGen : public SelPathGen
{
public:
	CharsSelPathGen(const HexEditorWidget& editor, size_t vertScroll);
	virtual void leftPoint(size_t byteOffset, size_t row, Rounded rounded=NONE);
	virtual void rightPoint(size_t byteOffset, size_t row, Rounded rounded=NONE);
};


class MarkPathGen : public RangePathGen
{
public:
	MarkPathGen(const HexEditorWidget& editor, size_t vertScroll);
	
protected:
	void addPoint(QPointF point, Rounded rounded, float arcW, float arcH);
};


class HexMarkPathGen : public MarkPathGen
{
public:
	HexMarkPathGen(const HexEditorWidget& editor, size_t vertScroll);
	virtual void leftPoint(size_t byteOffset, size_t row, Rounded rounded=NONE);
	virtual void rightPoint(size_t byteOffset, size_t row, Rounded rounded=NONE);
};


class CharsMarkPathGen : public MarkPathGen
{
public:
	CharsMarkPathGen(const HexEditorWidget& editor, size_t vertScroll);
	virtual void leftPoint(size_t byteOffset, size_t row, Rounded rounded=NONE);
	virtual void rightPoint(size_t byteOffset, size_t row, Rounded rounded=NONE);
};
