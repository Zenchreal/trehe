/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#include "ColumnsDialog.hpp"
#include "ui_ColumnsDialog.h"
#include <cassert>
#include <QListView>
#include <QStandardItem>
#include <QStandardItemModel>


ColumnsDialog::ColumnsDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::ColumnsDialog)
{
	ui->setupUi(this);

	model = new QStandardItemModel();
	ui->listView->setModel(model);
	clear();

	connect(this, SIGNAL(accepted()), SLOT(saveResult()));
}

ColumnsDialog::~ColumnsDialog()
{
	delete ui;
}

void ColumnsDialog::clear()
{
	model->clear();
}

void ColumnsDialog::addColumn(const QString& name, bool checked)
{
	QStandardItem* item = new QStandardItem(name);
	item->setCheckable(true);
	item->setCheckState(checked ? Qt::Checked : Qt::Unchecked);
	model->appendRow(item);
}

bool ColumnsDialog::columnChecked(const QString& name) const
{
	for (int i = 0; i < model->rowCount(); i++)
	{
		QStandardItem* item = model->item(i);
		if (item->text() == name)
			return item->checkState() == Qt::Checked;
	}
	assert(0 && "ColumnsDialog::columnChecked item not found");
}

void ColumnsDialog::saveResult()
{
	//
}
