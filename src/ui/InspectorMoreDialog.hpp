/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#pragma once
#include <cstddef>
#include <vector>
#include <QDialog>
#include "bytes/Interpretation.hpp"
#include "SmartPtr.hpp"

class QStandardItemModel;
namespace Ui
{
	class InspectorMoreDialog;
}


class InspectorMoreDialog : public QDialog
{
	Q_OBJECT
    
public:
	explicit InspectorMoreDialog(QWidget* parent = nullptr);
	virtual ~InspectorMoreDialog();

	/**
	 * Set the default endian.
	 */
	void setDefaultEndian(Interpretation::Endian flag);
	
	/**
	 * Get the default endian.
	 */
	Interpretation::Endian defaultEndian() const;

	/**
	 * Set the list of all interpretations this dialog box will show
	 */
	void updateList(const std::vector<shared_ptr<Interpretation>>& list,
		QByteArray bytes);
	
	size_t result() const;
    
protected slots:
	void saveResult();

private:
	Ui::InspectorMoreDialog* ui;
	QStandardItemModel* model;
	Interpretation::Endian defaultEndianValue;
};
