/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * The hex editor widget
 */
#pragma once
#include <cstddef>
#include <cstdint>
#include <utility>
#include <QtCore>
#include <QString>
#include <QAbstractScrollArea>
#include <QBrush>
#include <QPen>
#include <QFont>
#include <QStaticText>
#include <json/json.h>
#include "bytes/MarksHierarchy.hpp"
#include "SmartPtr.hpp"
#include "CopyPasteHandler.hpp"

class QAction;
class QMenu;
class QRect;
class QPainter;
class QHelpEvent;
class Document;
class RangePathGen;
class HexSelPathGen;
class CharsSelPathGen;


class HexEditorWidget : public QAbstractScrollArea
{
	Q_OBJECT

public:
	/**
	 * Construct a hex editor widget. The custContextMenu flag indicates
	 * whether or not to skip creating our own context menu and let the
	 * parent window do it.
	 */
	HexEditorWidget(QWidget* parent, bool custContextMenu = false,
		const Json::Value& theme = Json::Value());
	
	/**
	 * Set the specified value as the theme. An empty JSON value will
	 * result in the default theme.
	 */
	void reloadTheme(const Json::Value& theme = Json::Value());

	/**
	 * Set the document that the editor is currently using as a data
	 * source/store.
	 */
	void setDocument(shared_ptr<Document> doc);

	/**
	 * Get the document that the editor is using.
	 */
	shared_ptr<Document> document() const;
	
	/**
	 * Set the current line length (bytes per line) to an integer
	 * between 1 and 1024, inclusive. We won't redraw the widget.
	 */
	void setLineLength(unsigned int size);
	
	/**
	 * Get the current line length (bytes per line).
	 */
	unsigned int lineLength() const;

	/**
	 * Set the current address width (usually 8). Zeros will be added to the front
	 * of the address if the address doesn't fit the entire width.
	 */
	void setAddressLength(unsigned int count);

	/**
	 * Get the current address width.
	 */
	unsigned int addressLength();

	/**
	 * Represent a selection using and std::pair. The first member is
	 * the offset of the first byte in the selection and the second is
	 * the offset of the last byte. (In particular that means the size
	 * of a given selection is second - first + 1.)
	 */
	typedef std::pair<size_t, size_t> Selection;

	/**
	 * Set the current selection.
	 */
	void setSelection(Selection sel);

	/**
	 * Clear the current selection.
	 */
	void clearSelection();

	/**
	 * Determine whether any bytes are selected.
	 */
	bool hasSelection() const;

	/**
	 * Get the current selection. If there is no selection, both the
	 * first and second members will be -1.
	 */
	Selection selection() const;

	/**
	 * Returns the offset of the current selection.
	 */
	size_t selectionOffset() const;
	
	/**
	 * Returns the length of the current selection.
	 */
	size_t selectionLength() const;

	/**
	 * Get the current offset of the caret.
	 */
	size_t caretOffset() const;

	/**
	 * Set the current offset of the caret. Scroll the display so that the new
	 * offset is visible.
	 */
	void setCaretOffset(size_t offset);

	/**
	 * Are we in insert mode or not?
	 */
	bool insertMode() const;

	/**
	 * If any bytes are selected, overwrite them (only, not any past the
	 * end of the selecton) or overwrite any bytes at the current caret
	 * position. Doesn't change the caret position or selection. This
	 * will send the update signal.
	 */
	bool overwriteBytes(const QByteArray& bytes);
	
	/**
	 * Insert or overwrite the selection or at the current caret
	 * position with the given array of bytes depending on the state of
	 * the editor. The caret is moved to the end of the
	 * insertion/overwrite. This will send the update signal.
	 */
	bool putBytes(const QByteArray& bytes);

	/**
	 * Delete the current selection or caret position regardles of insert
	 * or overwrite mode. This will send the update signal.
	 */
	void deleteBytes();
	
	/**
	 * Return the substring of bytes currently in the selection. Returns
	 * an empty byte array if there is no selection. If maxLength is
	 * greator than zero, limit the number of bytes we read to that.
	 */
	QByteArray getBytes(size_t maxLength = 0) const;

	/**
	 * Return the substring of bytes at the given offset and extending
	 * length bytes.
	 */
	QByteArray getBytes(size_t offset, size_t length) const;

	/**
	 * Suggest a suitable mark color based on the current color scheme. We will
	 * attempt to pick a color that isn't used by any other marks around the given
	 * one or the background/foreground.
	 */
	uint32_t suggestMarkColor(size_t offset, size_t length) const;

	void setCustomContextMenu(bool flag);

	bool isCustomContextMenu() const;

	virtual QSize sizeHint() const;
	
signals:
	void stateChanged();
	void statusMessage(const QString& msg);

public slots:
	void cutSlot();
	void copySlot();
	void pasteSlot();
	void deleteSlot();
	void selectAllSlot();
	void insertModeSlot();

public:
	// These actions have been made public to allow any containing class
	// to add them to their menu. Don't modify.
	// TODO This should be refactored somehow, and then we won't need these public
	QAction* cutAct;
	QAction* copyAct;
	QAction* pasteAct;
	QAction* deleteAct;
	QAction* selectAllAct;
	QAction* insertModeAct;

protected:
	virtual bool event(QEvent* event);
	virtual void timerEvent(QTimerEvent* event);
	virtual void focusInEvent(QFocusEvent* event);
	virtual void focusOutEvent(QFocusEvent* event);
	virtual void contextMenuEvent(QContextMenuEvent* event);
	virtual void mousePressEvent(QMouseEvent* event);
	virtual void mouseDoubleClickEvent(QMouseEvent* event);
	virtual void mouseMoveEvent(QMouseEvent* event);
	virtual void mouseReleaseEvent(QMouseEvent* event);
	virtual void keyPressEvent(QKeyEvent* event);
	virtual void resizeEvent(QResizeEvent* event);
	virtual void paintEvent(QPaintEvent* event);

private:
	struct DrawStruct;

	QString charString(unsigned char c) const;
	void clearEditorState();
	void startSelection();
	bool inSelection(size_t offset) const;
	void createFonts();
	void updateFileSize();
	void updateLayout();
	void restartTimer();
	bool shouldMoveFullByte(bool shift) const;
	void caretToSelectionStart();
	void directionKeyPress(Qt::KeyboardModifiers mods, int key);
	bool modifyByteAtCaret(int key, const QString& text);
	void deleteByteNearCaret(bool before, bool forceDelete=false);
	void scrollToCaret();
	QBrush themeBrush(const QString& name, QBrush def);
	void createBrushes();
	QPen themePen(const QString& name, QPen def);
	void createPens();
	void drawDisplay(QPainter& p, const QRect& rect);
	void drawBackgroundStripes(QPainter& p, const DrawStruct& ds);
	void drawThinDividers(QPainter& p, const DrawStruct& ds);
	void drawAddressBackground(QPainter& p, const DrawStruct& ds);
	void drawAddressDivider(QPainter& p, const DrawStruct& ds);
	void drawCharsHexDivider(QPainter& p, const DrawStruct& ds);
	void drawAddresses(QPainter& p, const DrawStruct& ds);
	void drawMarks(QPainter& p, const DrawStruct& ds);
	void drawHexPane(QPainter& p, const DrawStruct& ds);
	void drawHexPaneCaret(QPainter& p, const DrawStruct& ds, uint line, uint j, int nibble);
	void drawHexPaneEndFile(QPainter& p, const DrawStruct& ds, uint line, uint j, int nibble);
	void drawCharsPane(QPainter& p, const DrawStruct& ds);
	void drawCharsPaneCaret(QPainter& p, const DrawStruct& ds, uint line, uint j);
	void drawCharsPaneEndFile(QPainter& p, const DrawStruct& ds, uint line, uint j);
	void drawSelection(QPainter& p, const DrawStruct& ds);
	void makeRangePath(size_t a, size_t b, RangePathGen& output) const;
	void updateEverything();
	void updateCaretAndSelection();
	void recolorMarks(MarksHierarchy::Node& node) const;
	void handleTooltip(QHelpEvent* event);
	void createContextMenu();

	// This class needs to be able to access the layout
	friend class RangePathGen;
	friend class HexSelPathGen;
	friend class CharsSelPathGen;
	friend class HexMarkPathGen;
	friend class CharsMarkPathGen;

	// Layout inlines
	inline uint hexByteOffset(uint index, int extra) const
	{
		return hexOffset + (digitWidth * 2) * index + padding * index + digitWidth * extra;
	}

	inline uint hexDividerOffset(uint index) const
	{
		return hexOffset + (digitWidth * 2) * index + padding * index - padding / 2;
	}

	inline uint charsByteOffset(uint index) const
	{
		return charsOffset + charWidth * index;
	}

	// Some enums
	enum Pane
	{
		NOPANE,
		HEX,
		CHARS
	};

	enum Mode
	{
		INSERT,
		OVERWRITE
	};

	enum InsideSpecifier
	{
		LP=0,
		LD=1,
		RD=2,
		RP=3,
		NONE
	};

	// A class to keep track of the caret's location
	class CaretPosition
	{
	public:
		inline CaretPosition():
			offset(0),
			inside(LP)
		{
		}
		
		inline CaretPosition(size_t offset, InsideSpecifier inside):
			offset(offset),
			inside(inside)
		{
		}

		inline bool valid() const
		{
			return inside != NONE;
		}
		
		inline bool operator==(const CaretPosition& other) const
		{
			return offset == other.offset && inside == other.inside;
		}
		
		inline bool operator!=(const CaretPosition& other) const
		{
			return !(*this == other);
		}

		size_t offset;
		InsideSpecifier inside;
	};

	// Private functions that rely on those types
	void setSelectionPenAndBrush(QPainter& p, Pane currentPane);

	/**
	 * Determine in which pane and offset the given x, y coordinates are
	 * located, possibly forcing us to consider them in the given pane.
	 * If the coordinates are not in a pane (and we're not forcing a
	 * pane) the first value will be NOPANE.
	 */
	std::pair<Pane, CaretPosition> hitTest(int x, int y, Pane forcePane = NOPANE) const;

	// Our document object
	shared_ptr<Document> doc;
	Json::Value theme;

	// Very important settings
	uint bytesPerLine;
	size_t totalBytes;
	size_t totalRows;
	size_t totalHeight;
	bool drawTextStatics;

	// Current state of the editor
	Pane focusedPane;
	Mode caretMode;
	CaretPosition caretPos;
	CaretPosition selCaretPos;
	bool lastHitTestSucceeded;
	bool mouseSelecting;
	bool caretBlack;
	int caretTimer;
	
	// Previous state of the editor
	struct
	{
		CaretPosition caretPos;
		CaretPosition selCaretPos;
	} last;

	// Context menu items
	bool customContextMenuFlag;
	QMenu* contextMenu;
	CopyPasteHandler copyPasteHandler;

	// Variables used by paintEvent and children
	size_t topDrawnRow;
	size_t endRow;
	uint lineTop;

	// Layout
	uint hexDigitWidth;
	uint lineHeight;
	uint padding;
	uint addressOffset;
	uint addressDigitCount;
	uint addressWidth;
	uint addressHexDividerOffset;
	uint hexOffset;
	uint digitWidth;
	uint hexDigitFullWidth;
	uint hexWidth;
	uint hexCharsDividerOffset;
	uint charsOffset;
	uint charWidth;
	uint charsWidth;
	uint totalWidth;
	uint selEdgePadding;

	// Fonts
	QFont font;
	QFont fontModified;
	//QPixmap digitPixmaps[16]; // QPixmapCache anyone?
	QStaticText digitStatics[16];
	QStaticText digitStaticsModified[16];
	QStaticText charStatics[256];
	QStaticText charStaticsModified[256];

	// Brushes
	QBrush backgroundBrush;
	QBrush stripeBrush;
	QBrush addressBrush;
	QBrush charsBrush;
	QBrush caretBrush;
	QBrush selectionBrush;
	QBrush selectionBrushUnfocused;
	QBrush markBrush;

	// Pens
	QPen thinDividerPen;
	QPen addressPen;
	QPen bytePen;
	QPen byteCaretPen;
	QPen byteInsertPen;
	QPen charPen;
	QPen charCaretPen;
	QPen charInsertPen;
	QPen sepLinePen;
	QPen selectionBorderPen;
	QPen selectionBorderPenUnfocused;
};
