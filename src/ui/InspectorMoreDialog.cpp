/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#include "InspectorMoreDialog.hpp"
#include "ui_InspectorMoreDialog.h"
#include <QStandardItemModel>
#include <QStandardItem>
#include <QSortFilterProxyModel>
#include <bytes/Interpretation.hpp>


/**
 * Storage for per-QStandardItem metadata (i.e. keep track of what
 * Interpretation an item is using).
 */
struct IMItemUserData
{
	IMItemUserData():
		isValue(false)
	{
	}
	
	IMItemUserData(bool isValue, shared_ptr<Interpretation> interp):
		isValue(isValue),
		interp(interp)
	{
	}
	
	bool isValue;
	shared_ptr<Interpretation> interp;
};

Q_DECLARE_METATYPE(IMItemUserData);


InspectorMoreDialog::InspectorMoreDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::InspectorMoreDialog),
    defaultEndianValue(Interpretation::LITTLE)
{
	ui->setupUi(this);
	ui->buttonBox->addButton(ui->addButton, QDialogButtonBox::ActionRole);

	// Set up the tree view
	model = new QStandardItemModel();
	model->setHorizontalHeaderLabels(QStringList() << tr("Type") << tr("Value"));
	
	QSortFilterProxyModel* proxy = new QSortFilterProxyModel();
	proxy->setSourceModel(model);

	ui->tree->setModel(proxy);
	ui->tree->setSortingEnabled(true);
	ui->tree->setIndentation(0);
	ui->tree->setDragEnabled(false);
	ui->tree->setAcceptDrops(false);

	connect(this, SIGNAL(accepted()), SLOT(saveResult()));
}

InspectorMoreDialog::~InspectorMoreDialog()
{
	delete ui;
}

void InspectorMoreDialog::setDefaultEndian(Interpretation::Endian flag)
{
	defaultEndianValue = flag;
}

Interpretation::Endian InspectorMoreDialog::defaultEndian() const
{
	return defaultEndianValue;
}

void InspectorMoreDialog::updateList(const std::vector<shared_ptr<Interpretation>>& list, QByteArray bytes)
{
	for (shared_ptr<Interpretation> interp : list)
	{
		// Correct endian for the interp
		interp->setEndian(defaultEndianValue);

		std::string valueString;
		Interpretation::Result result;
		
		result = interp->bytesToStringBounded(
			(const uint8_t*) bytes.data(),
			bytes.size(),
			valueString,
			64
		);
		
		if (result != Interpretation::SUCCESS)
		{
			valueString = "oops";
		}
		
		QStandardItem* name = new QStandardItem(interp->name().c_str()); // TODO
		QStandardItem* value = new QStandardItem(valueString.c_str());

		// Type name should be bold
		QFont font = name->font();
		font.setBold(true);
		name->setFont(font);

		// Right alignment, centered vertically
		name->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

		name->setFlags(name->flags() & ~(Qt::ItemIsDropEnabled | Qt::ItemIsEditable));
		value->setFlags(value->flags() & ~(Qt::ItemIsDropEnabled | Qt::ItemIsEditable));

		name->setData(QVariant::fromValue(IMItemUserData(false, interp)));
		value->setData(QVariant::fromValue(IMItemUserData(true, interp)));

		model->appendRow(QList<QStandardItem*>() << name << value);
	}

	ui->tree->setSortingEnabled(true);
}

size_t InspectorMoreDialog::result() const
{
	return 0;
}

void InspectorMoreDialog::saveResult()
{
}
