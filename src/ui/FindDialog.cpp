/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#include "FindDialog.hpp"
#include "ui_FindDialog.h"
#include <iostream>
#include <QKeyEvent>
#include "bytes/Document.hpp"


FindDialog::FindDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::FindDialog),
    backwardsResult(false),
    caseInsensitiveResult(false)
{
	ui->setupUi(this);

	ui->bytesEdit->setLineLength(8);
	ui->bytesEdit->setAddressLength(2);

	connect(this, SIGNAL(accepted()), SLOT(saveResult()));
}

FindDialog::~FindDialog()
{
	delete ui;
}

void FindDialog::setBackwards(bool flag)
{
	ui->backwardsCheckBox->setChecked(flag);
}

void FindDialog::setCaseInsensitive(bool flag)
{
	ui->caseInsensitiveCheckBox->setChecked(flag);
}

void FindDialog::setBytes(const QByteArray& data)
{
	// Toss out the old Document and create a new one
	ui->bytesEdit->setDocument(make_shared<Document>("", true));
	ui->bytesEdit->putBytes(data);
}

bool FindDialog::backwards() const
{
	return backwardsResult;
}

bool FindDialog::caseInsensitive() const
{
	return caseInsensitiveResult;
}

QByteArray FindDialog::bytes() const
{
	return resultBytes;
}

void FindDialog::saveResult()
{
	backwardsResult = ui->backwardsCheckBox->isChecked();
	caseInsensitiveResult = ui->caseInsensitiveCheckBox->isChecked();
	
	resultBytes = ui->bytesEdit->getBytes(0, ui->bytesEdit->document()->size());
}
