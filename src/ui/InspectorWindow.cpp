/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Window containing the inspector widget
 */
#include "InspectorWindow.hpp"
#include <string>
#include <iostream>
#include <QtCore>
#include <QVBoxLayout>
#include <QTreeView>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QSortFilterProxyModel>
#include <QKeyEvent>
#include <QMetaType>

#include "bytes/Interpretation.hpp"
#include "bytes/BasicInterp.hpp"
#include "InspectorMoreDialog.hpp"

using std::cout;
using std::endl;


/**
 * Storage for per-QStandardItem metadata (i.e. keep track of what
 * Interpretation an item is using).
 */
struct ItemUserData
{
	ItemUserData():
		isValue(false),
		oldValueText(make_shared<std::string>())
	{
	}
	
	ItemUserData(bool isValue, shared_ptr<Interpretation> interp):
		isValue(isValue),
		interp(interp),
		oldValueText(make_shared<std::string>())
	{
	}
	
	bool isValue;
	shared_ptr<Interpretation> interp;
	shared_ptr<std::string> oldValueText;
};

Q_DECLARE_METATYPE(ItemUserData);

QDataStream &operator<<(QDataStream &out, const ItemUserData &myObj)
{
	Json::Value data;
	if (myObj.interp)
	{
		myObj.interp->serialize(data);
	}
	QString str(Json::FastWriter().write(data).c_str());
	out << myObj.isValue << str << QString(myObj.oldValueText->c_str());
	return out;
}

QDataStream &operator>>(QDataStream &in, ItemUserData &myObj)
{
	QString json;
	QString oldValueText;
	in >> myObj.isValue >> json >> oldValueText;
	Json::Value data;
	Json::Reader().parse(json.toUtf8().constData(), data);
	myObj.interp = Interpretation::deserialize(data);
	myObj.oldValueText = make_shared<std::string>(oldValueText.toUtf8().constData());
	return in;
}


InspectorWindow::InspectorWindow(QWidget* parent):
	QDockWidget(parent, Qt::Tool | Qt::CustomizeWindowHint |
		Qt::WindowTitleHint | Qt::WindowCloseButtonHint),
	model(nullptr),
	defaultEndianValue(Interpretation::LITTLE),
	blockItemChanged(false),
	contextMenu(this)
{
	// TODO Only call this once
	qRegisterMetaTypeStreamOperators<ItemUserData>("ItemUserData");

	tree = new QTreeView();

	model = new ItemModel();
	model->setHorizontalHeaderLabels(QStringList() << tr("Type") << tr("Value"));

	tree->setModel(model);
	tree->setIndentation(0);
	tree->setDragEnabled(false);
	tree->setAcceptDrops(false);
	tree->showDropIndicator();
	tree->setDragDropMode(QAbstractItemView::InternalMove);

	setWidget(tree);

	tree->setFocusPolicy(Qt::ClickFocus);

	setWindowTitle(tr("Inspector"));

	createItems();
	createContextMenu();

	connect(model, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(itemChanged(QStandardItem*)));
}

void InspectorWindow::setDefaultEndian(Interpretation::Endian flag)
{
	defaultEndianValue = flag;
	
	// Iterate through all of the items, changing the endian
	for (int i = 0; i < model->rowCount(); i++)
	{
		QStandardItem* valueItem = model->item(i, 1);
		if (!valueItem)
			continue;

		// Get our user data from the QStandardItem
		QVariant userData = valueItem->data();
		ItemUserData item = userData.value<ItemUserData>();
		if (!item.interp)
			continue;
			
		item.interp->setEndian(defaultEndianValue);
	}
}

Interpretation::Endian InspectorWindow::defaultEndian() const
{
	return defaultEndianValue;
}

void InspectorWindow::keyPressEvent(QKeyEvent* event)
{
	assert(event);

	QModelIndexList sel;

	switch (event->key())
	{
	case Qt::Key_Enter:
	case Qt::Key_Return:
		// TODO Make this a shortcut to start editing an item
		event->ignore();
		break;

	default:
		event->ignore();
		break;
	}
}

void InspectorWindow::contextMenuEvent(QContextMenuEvent* event)
{
	assert(event);
	event->accept();
	contextMenu.exec(event->globalPos());
}

void InspectorWindow::createContextMenu()
{
	hideAct = new QAction(tr("&Hide"), this);
	//hideAct->setShortcuts(QKeySequence::New);
	//connect(hideAct, SIGNAL(triggered()), this, SLOT(new_()));

	moreAct = new QAction(tr("&More..."), this);
	moreAct->setShortcut(QKeySequence(tr("Ctrl+Shift+I")));
	connect(moreAct, SIGNAL(triggered()), this, SLOT(more()));

	addAction(hideAct);
	addAction(moreAct);

	contextMenu.addAction(hideAct);
	contextMenu.addSeparator();
	contextMenu.addAction(moreAct);
}

void InspectorWindow::createItems()
{
	appendItem(make_shared<UInt8Interp>());
	appendItem(make_shared<Int8Interp>());
	appendItem(make_shared<UInt16Interp>());
	appendItem(make_shared<Int16Interp>());
	appendItem(make_shared<UInt32Interp>());
	appendItem(make_shared<Int32Interp>());
	//appendSpacerItem();
	appendItem(make_shared<FloatInterp>());
	appendItem(make_shared<DoubleInterp>());
}

void InspectorWindow::appendItem(shared_ptr<Interpretation> interp)
{
	assert(interp);
	
	// Correct endian for the interp
	interp->setEndian(defaultEndianValue);

	// Make sure the item changed signal knows that it's just us
	// changing the text, not the user.
	blockItemChanged = true;
	
	QStandardItem* name = new QStandardItem(interp->name().c_str()); // TODO
	QStandardItem* value = new QStandardItem("...");

	// Type name should be bold
	QFont font = name->font();
	font.setBold(true);
	name->setFont(font);

	// Right alignment, centered vertically
	name->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);

	name->setFlags(name->flags() & ~(Qt::ItemIsDropEnabled | Qt::ItemIsEditable));
	value->setFlags(value->flags() & ~(Qt::ItemIsDropEnabled));

	name->setData(QVariant::fromValue(ItemUserData(false, interp)));
	value->setData(QVariant::fromValue(ItemUserData(true, interp)));

	model->appendRow(QList<QStandardItem*>() << name << value);

	blockItemChanged = false;
}

void InspectorWindow::appendSpacerItem()
{
	// Make sure the item changed signal knows that it's just us
	// changing the text, not the user.
	blockItemChanged = true;

	QStandardItem* name = new QStandardItem("");
	QStandardItem* value = new QStandardItem("");

	name->setFlags(name->flags() & ~(Qt::ItemIsDropEnabled | Qt::ItemIsEditable));
	value->setFlags(value->flags() & ~(Qt::ItemIsDropEnabled | Qt::ItemIsEditable));

	model->appendRow(QList<QStandardItem*>() << name << value);

	blockItemChanged = false;
}

void InspectorWindow::updateValues(QByteArray bytes)
{
	lastBytes = bytes;

	for (int i = 0; i < model->rowCount(); i++)
	{
		QStandardItem* valueItem = model->item(i, 1);
		if (!valueItem)
			continue;

		// Get our user data from the QStandardItem
		QVariant userData = valueItem->data();
		ItemUserData item = userData.value<ItemUserData>();
		if (!item.interp)
			continue;

		std::string valueString;
		Interpretation::Result result;
		
		result = item.interp->bytesToStringBounded(
			(const uint8_t*) bytes.data(),
			bytes.size(),
			valueString,
			64
		);

		*item.oldValueText = valueString;

		// Make sure the item changed signal knows that it's just us
		// changing the text, not the user.
		blockItemChanged = true;
		
		if (result == Interpretation::SUCCESS)
		{
			valueItem->setText(valueString.c_str());
		}
		else
		{
			valueItem->setText("oops");
		}
		
		blockItemChanged = false;
	}
}

void InspectorWindow::loadState(const Json::Value& in)
{
	QByteArray state(in.get("columnHeaderState", "").asCString());
	tree->header()->restoreState(QByteArray::fromBase64(state));
	
	// Load the list of the inspector items
	const Json::Value& items = in["items"];

	if (items.isArray() && items.size() > 0)
	{
		model->removeRows(0, model->rowCount());
		
		for (unsigned int i = 0; i < items.size(); i++)
		{
			shared_ptr<Interpretation> interp;
			interp = Interpretation::deserialize(items[i]);
			
			if (interp)
				appendItem(interp);
		}
	}
}

void InspectorWindow::saveState(Json::Value& out) const
{
	out["columnHeaderState"] = tree->header()->saveState().toBase64().constData();
	
	// Save the list of the inspector items
	Json::Value items(Json::arrayValue);
	unsigned int j = 0;

	for (int i = 0; i < model->rowCount(); i++)
	{
		QStandardItem* valueItem = model->item(i, 1);
		if (!valueItem)
			continue;

		// Get our user data from the QStandardItem
		QVariant userData = valueItem->data();
		ItemUserData item = userData.value<ItemUserData>();
		if (!item.interp)
			continue;
			
		item.interp->serialize(items[j]);
		j++;
	}

	out["items"] = items;
}

void InspectorWindow::itemChanged(QStandardItem* valueItem)
{
	assert(valueItem);
	
	// Assumption: this only works if signals are emitted in the same
	// thread as everything else.
	if (blockItemChanged)
		return;

	// Get our user data from the QStandardItem
	QVariant userData = valueItem->data();
	ItemUserData item = userData.value<ItemUserData>();
	if (!item.interp)
		return;

	if (!item.isValue)
		return;

	std::string newValue(valueItem->text().toUtf8().constData());

	if (*item.oldValueText == newValue)
		return;

	Interpretation::Result result;
	QByteArray bytes(64, '\0');
	size_t length = bytes.size();

	result = item.interp->stringToBytes(
		newValue,
		(uint8_t*) bytes.data(),
		length
	);

	bytes.truncate(length);

	if (result == Interpretation::SUCCESS)
	{
		updateBytes(bytes);
	}
	else
	{
		cout << "failed to set item" << endl;
	}
}

void InspectorWindow::more()
{
	InspectorMoreDialog dlg(this);
	std::vector<shared_ptr<Interpretation>> list;
	
	Interpretation::listAll(list);
	dlg.updateList(list, lastBytes);
	dlg.exec();
}

Qt::ItemFlags InspectorWindow::ItemModel::flags(const QModelIndex& index) const
{
	Qt::ItemFlags result = QStandardItemModel::flags(index);
	if (index.column() != -1)
	{
		result &= ~Qt::ItemIsDropEnabled;
	}
	return result;
}

bool InspectorWindow::ItemModel::dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent)
{
	// Set column to 0 so that all drops will go into the first column
	return QStandardItemModel::dropMimeData(data, action, row, 0, parent);
}


