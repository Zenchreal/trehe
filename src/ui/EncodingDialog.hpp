/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#pragma once
#include <cstddef>
#include <QDialog>
#include <QByteArray>

class QStandardItemModel;

namespace Ui
{
	class EncodingDialog;
}


class EncodingDialog : public QDialog
{
	Q_OBJECT
    
public:
	explicit EncodingDialog(QWidget* parent = nullptr);
	virtual ~EncodingDialog();
    
protected slots:
	void addEncodings();
	void saveResult();

private:
	Ui::EncodingDialog* ui;
	QStandardItemModel* model;
};
