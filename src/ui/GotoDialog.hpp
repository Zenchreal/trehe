/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#pragma once
#include <cstddef>
#include <QDialog>

namespace Ui
{
	class GotoDialog;
}


class GotoDialog : public QDialog
{
	Q_OBJECT
    
public:
	explicit GotoDialog(QWidget* parent = nullptr);
	virtual ~GotoDialog();
	size_t result() const;
    
protected slots:
	void saveResult();

private:
	Ui::GotoDialog* ui;
	size_t resultOffset;
};
