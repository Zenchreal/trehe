/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#pragma once
#include <cstddef>
#include <QDialog>

namespace Ui
{
	class SelectBlockDialog;
}


class SelectBlockDialog : public QDialog
{
	Q_OBJECT
    
public:
	explicit SelectBlockDialog(QWidget* parent = nullptr);
	virtual ~SelectBlockDialog();
	bool backwards() const;
	size_t selectionSize() const;
    
protected slots:
	void saveResult();

private:
	Ui::SelectBlockDialog* ui;
	bool backwardsResult;
	size_t sizeResult;
};
