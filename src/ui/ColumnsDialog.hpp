/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#pragma once
#include <cstddef>
#include <map>
#include <QDialog>

class QStandardItemModel;
namespace Ui
{
	class ColumnsDialog;
}


class ColumnsDialog : public QDialog
{
	Q_OBJECT
    
public:
	explicit ColumnsDialog(QWidget* parent = nullptr);
	virtual ~ColumnsDialog();
	void clear();
	void addColumn(const QString& name, bool checked);
	bool columnChecked(const QString& name) const;
    
protected slots:
	void saveResult();

private:
	Ui::ColumnsDialog* ui;
	QStandardItemModel* model;
	size_t resultOffset;
};
