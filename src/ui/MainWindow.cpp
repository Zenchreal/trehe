/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Main hex editor window
 */
#include "MainWindow.hpp"
#include <iostream>
#include <algorithm>
#include <QtGui>
#include <QFileInfo>
#include <QMessageBox>
#include <QFileDialog>
#include <QTextStream>
#include <QClipboard>
#include <QMimeData>
#include <QStatusBar>
#include <QMenuBar>
#include <QLabel>
#include "Application.hpp"
#include "SmartPtr.hpp"
#include "HexEditorWidget.hpp"
#include "InspectorWindow.hpp"
#include "MarksWindow.hpp"
#include "FindDialog.hpp"
#include "GotoDialog.hpp"
#include "CopyPasteHandler.hpp"
#include "SelectBlockDialog.hpp"
#include "bytes/Document.hpp"
#include "bytes/BasicInterp.hpp"

#define DOCK_STATE_VERSION 1

using std::cout;
using std::endl;


MainWindow::MainWindow():
	app(nullptr),
	editor(nullptr),
	findBackwards(false),
	findCaseInsensitive(false),
	inspector(nullptr),
	marksWindow(nullptr)
{
	// The default qApp seems to be a total hack, so why not?
	app = static_cast<Application*>(qApp);

	setWindowTitle(tr("Trehe"));
	createEditor();
	createInspector();
	createMarksWindow();
	createActions();
	createMenus();
	createStatusBar();
	restoreLast();
	updateDocument();
	
	setAcceptDrops(true);
}

MainWindow::~MainWindow()
{
}

void MainWindow::openFile(const QString& filename)
{
	using std::ios_base;
	
	if (!checkModified())
		return;
	
	try
	{
		shared_ptr<Document> newDoc;
		newDoc = make_shared<Document>(filename.toUtf8().constData());
		newDoc->reload();

		// Presumably by this point we've succeeded in loading the file
		doc = newDoc;
		editor->setDocument(doc);
	}
	catch (ios_base::failure)
	{
		showOpenFileError(filename);
	}
	catch (std::exception)
	{
		// TODO Determine whether the file is empty or too big
		QMessageBox::warning(this, tr("Error"), tr("File too big '") + filename + tr("'. (Maybe you already have a big file open?)"));
	}
	
	updateDocument();
}

bool MainWindow::saveFile()
{
	if (doc->isTempPath())
	{
		QString filename = QFileDialog::getSaveFileName(
			this,
			tr("Save File")
		);
		
		if (filename.count() <= 0)
			return false;
			
		doc->setPath(filename.toUtf8().constData());
	}
	
	try
	{
		doc->save();
	}
	catch (std::exception)
	{
		// TODO Better error message here
		QMessageBox::warning(this, tr("Error"), tr("Failed to save '") + QString(doc->path().c_str()) + tr("'."));
	}
	
	updateDocument();
	return true;
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
	Json::Value& windConfig = app->last()["mainwindow"];
		
	if (!isMaximized())
	{
		// Save width and height
		Json::Value windSize(Json::arrayValue);
		windSize[0] = size().width();
		windSize[1] = size().height();
		windConfig["size"] = windSize;
		windConfig["maximized"] = false;
	}
	else
	{
		windConfig["maximized"] = true;
	}
}

void MainWindow::closeEvent(QCloseEvent* event)
{
	if (!checkModified())
	{
		// Don't close the window
		event->ignore();
	}
	else
	{
		Json::Value& windConfig = app->last()["mainwindow"];
		
		// Docking configuration
		windConfig["dockState"] = saveState(DOCK_STATE_VERSION).toBase64().constData();
		
		inspector->saveState(windConfig["inspector"]);
		marksWindow->saveState(windConfig["marksWindow"]);
		
		// Make sure that the document is unloaded when the window is
		// closed.
		if (event->isAccepted())
		{
			doc = shared_ptr<Document>();
		}
	}
}

void MainWindow::restoreLast()
{
	Json::Value& windConfig = app->last()["mainwindow"];

	// Width and height
	Json::Value& windSize = windConfig["size"];
	if (windSize.isArray() && windSize.size() == 2)
	{
		resize(QSize(windSize[0].asInt(), windSize[1].asInt()));

		if (windConfig.get("maximized", false) == true)
		{
			setWindowState(Qt::WindowMaximized);
		}
	}

	// Docking configuration
	QByteArray dockState(windConfig.get("dockState", "").asCString());
	restoreState(QByteArray::fromBase64(dockState), DOCK_STATE_VERSION);
	
	inspector->loadState(windConfig["inspector"]);
	marksWindow->loadState(windConfig["marksWindow"]);
}

void MainWindow::new_()
{
	MainWindow* window = new MainWindow();
	window->setAttribute(Qt::WA_DeleteOnClose);
	window->show();
}

void MainWindow::open()
{
	QString filename = QFileDialog::getOpenFileName(
		this,
		tr("Open File")
	);
		
	if (filename.count() > 0)
		openFile(filename);
		
	updateDocument();
}

void MainWindow::openFormat()
{
	todo("openFormat");
}

void MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
	event->acceptProposedAction();
}

void MainWindow::dragMoveEvent(QDragMoveEvent* event)
{
	event->acceptProposedAction();
}

void MainWindow::dragLeaveEvent(QDragLeaveEvent* event)
{
	event->accept();
}

void MainWindow::dropEvent(QDropEvent* event)
{
	const QMimeData* mime = event->mimeData();
	assert(mime);

	if (mime->hasUrls())
	{
		QList<QUrl> urlList = mime->urls();

		// Don't open more than 32 files at a time
		for (int i = 0; i < urlList.size() && i < 32; i++)
		{
			QString path = urlList.at(i).toLocalFile();
			
			if (i == 0)
			{
				openFile(path);
			}
			else
			{
				MainWindow* window = new MainWindow();
				window->setAttribute(Qt::WA_DeleteOnClose);
				window->openFile(path);
				window->show();
			}
		}
	}
}

void MainWindow::save()
{
	saveFile();
}

void MainWindow::saveAs()
{
	QString filename = QFileDialog::getSaveFileName(
		this,
		tr("Save File As")
	);
	
	if (filename.count() <= 0)
		return;
		
	doc->setPath(filename.toUtf8().constData());
	
	try
	{
		doc->save();
	}
	catch (std::exception)
	{
		// TODO Better error message here
		QMessageBox::warning(this, tr("Error"), tr("Failed to save '") + filename + tr("'."));
	}
	
	updateDocument();
}

void MainWindow::reload()
{
	using std::ios_base;
	
	// We don't really want to toss out the user's changes without
	// notification.
	if (checkModified())
	{
		// If it's a temp path we can't really do anything if the
		// user hasn't saved it
		if (doc->isTempPath())
			return;
			
		try
		{
			doc->reload();

			// This is a workaround which causes editor to update the
			// total file size. If this line were not here, if when we
			// reloaded the file the size was not the same, the editor
			// would not get updated. This is because editor doesn't
			// call updateFileSize on regular updates, only special ones
			// (which setLineLength happens to be). Hacky, I know.
			editor->setLineLength(editor->lineLength());

			updateDocument();
		}
		catch (ios_base::failure)
		{
			showOpenFileError(doc->path().c_str());
		}
	}
}

void MainWindow::readOnly()
{
	assert(doc);
	
	doc->setReadOnly(readOnlyAct->isChecked());
}

void MainWindow::quit()
{
	close();
}

void MainWindow::undo()
{
	todo("undo");
}

void MainWindow::redo()
{
	todo("redo");
}

void MainWindow::cut()
{
	notImplemented();
}

void MainWindow::delete_()
{
	notImplemented();
}

void MainWindow::copy()
{
	notImplemented();
}

void MainWindow::copyAsValue()
{
	CopyPasteHandler handler(editor);
	handler.copyAsValue();
}

void MainWindow::copyAsHexString()
{
	CopyPasteHandler handler(editor);
	handler.copyAsHexString();
}

void MainWindow::copyAsEscString()
{
	CopyPasteHandler handler(editor);
	handler.copyAsEscString();
}

void MainWindow::copyAsBase64String()
{
	CopyPasteHandler handler(editor);
	handler.copyAsBase64String();
}

void MainWindow::pasteAsValue()
{
	CopyPasteHandler handler(editor);
	handler.pasteAsValue();
}

void MainWindow::pasteAsHexString()
{
	CopyPasteHandler handler(editor);
	handler.pasteAsHexString();
}

void MainWindow::pasteAsBase64String()
{
	CopyPasteHandler handler(editor);
	handler.pasteAsBase64String();
}

void MainWindow::printMimeTypes(const QMimeData* mimeData) const
{
	cout << "-------------------" << endl;
	for (const QString& format : mimeData->formats())
	{
		QByteArray data = mimeData->data(format);
		cout << "  " << format.toUtf8().constData() << " - " << data.size() << endl;
	}
	cout << "-------------------" << endl;
}

void MainWindow::paste()
{
	notImplemented();
}

void MainWindow::selectAll()
{
	editor->setSelection(HexEditorWidget::Selection(0, doc->size()));
	updatePosAndSel();
	editor->viewport()->update();
}

void MainWindow::saveToFile()
{
	// Make sure something is at least selected
	if (!editor->hasSelection())
		return;
		
	QString filename = QFileDialog::getSaveFileName(
		this,
		tr("Save File As")
	);
	
	if (filename.count() <= 0)
		return;
	
	try
	{
		doc->saveToFile(filename.toUtf8().constData(),
			editor->selectionOffset(),
			editor->selectionLength()
		);
	}
	catch (std::exception)
	{
		// TODO Better error message here
		QMessageBox::warning(this, tr("Error"), tr("Failed to save '") + filename + tr("'."));
	}
}

void MainWindow::insertMode()
{
	todo("insertMode");
}

void MainWindow::find()
{
	FindDialog dlg(this);

	if (editor->hasSelection() && editor->selectionLength() < 64)
	{
		QByteArray preFind = editor->getBytes(64);
		dlg.setBytes(preFind);
	}

	dlg.setBackwards(findBackwards);
	dlg.setCaseInsensitive(findCaseInsensitive);

	if (dlg.exec() == QDialog::Accepted)
	{
		findBackwards = dlg.backwards();
		findCaseInsensitive = dlg.caseInsensitive();
		
		findPattern = dlg.bytes();
		if (findPattern.size() < 1)
		{
			//statusMessage(tr("empty string"));
			return;
		}

		size_t found = doc->find(editor->caretOffset(), (const uint8_t*) findPattern.constData(), findPattern.size(), !findBackwards);
	
		if (found != size_t(-1))
		{
			// This is somewhat a hack: if we don't set the caret position, the screen
			// won't scroll down/up. A more elegent way to do this might be to add
			// a new function to set the caret/selection and scroll the window. Then
			// we could center the selection on the screen or do something else.
			editor->setCaretOffset(found + findPattern.size());
			editor->setCaretOffset(found);

			editor->setSelection(HexEditorWidget::Selection(found + findPattern.size() - 1, found));
		
			updatePosAndSel();
			editor->viewport()->update();
		}
		else
		{
			if (findBackwards)
				QMessageBox::information(this, tr("Find Upwards"), tr("No preceding instances of the string in this file."));
			else
				QMessageBox::information(this, tr("Find Downwards"), tr("No further instances of the string in this file."));
		}
	}
}

void MainWindow::findNext()
{
	if (findPattern.size() < 1)
	{
		//statusMessage(tr("empty string"));
		return;
	}

	size_t nextOffset;
	if (editor->hasSelection())
	{
		nextOffset = editor->selectionOffset() + 1;
	}
	else
	{
		nextOffset = editor->caretOffset();
	}

	size_t found = doc->find(nextOffset, (const uint8_t*) findPattern.constData(), findPattern.size(), true);
	
	if (found != size_t(-1))
	{
		// This is somewhat a hack: if we don't set the caret position, the screen
		// won't scroll down/up. A more elegent way to do this might be to add
		// a new function to set the caret/selection and scroll the window. Then
		// we could center the selection on the screen or do something else.
		editor->setCaretOffset(found + findPattern.size());
		editor->setCaretOffset(found);

		editor->setSelection(HexEditorWidget::Selection(found + findPattern.size() - 1, found));
		
		updatePosAndSel();
		editor->viewport()->update();
	}
	else
	{
		QMessageBox::information(this, tr("Find Downwards"), tr("No further instances of the string in this file."));
	}
}

void MainWindow::findPrev()
{
	if (findPattern.size() < 1)
	{
		//statusMessage(tr("empty string"));
		return;
	}

	size_t nextOffset;
	if (editor->hasSelection())
	{
		nextOffset = editor->selectionOffset();
	}
	else
	{
		nextOffset = editor->caretOffset();
	}

	size_t found = doc->find(nextOffset, (const uint8_t*) findPattern.constData(), findPattern.size(), false);
	
	if (found != size_t(-1))
	{
		// This is somewhat a hack: if we don't set the caret position, the screen
		// won't scroll down/up. A more elegent way to do this might be to add
		// a new function to set the caret/selection and scroll the window. Then
		// we could center the selection on the screen or do something else.
		editor->setCaretOffset(found + findPattern.size());
		editor->setCaretOffset(found);

		editor->setSelection(HexEditorWidget::Selection(found + findPattern.size() - 1, found));
		
		updatePosAndSel();
		editor->viewport()->update();
	}
	else
	{
		QMessageBox::information(this, tr("Find Upwards"), tr("No preceding instances of the string in this file."));
	}
}

void MainWindow::goto_()
{
	GotoDialog dlg(this);
	if (dlg.exec() == QDialog::Accepted)
	{
		editor->setCaretOffset(dlg.result());
		updatePosAndSel();
	}
}

void MainWindow::selectBlock()
{
	SelectBlockDialog dlg(this);
	if (dlg.exec() == QDialog::Accepted)
	{
		HexEditorWidget::Selection sel;
		
		if (editor->hasSelection())
			sel = editor->selection();
		else
			sel = HexEditorWidget::Selection(editor->caretOffset(), editor->caretOffset());
		
		if (!dlg.backwards())
		{
			if (dlg.selectionSize() > 0)
			{
				sel.second = sel.first + dlg.selectionSize() - 1;
				editor->setCaretOffset(sel.second);
				editor->setSelection(sel);
			}
			else
			{
				editor->setCaretOffset(sel.first);
			}
		}
		else
		{
			if (dlg.selectionSize() > 0)
			{
				sel.first = sel.second - (dlg.selectionSize() - 1);
				editor->setCaretOffset(sel.first);
				editor->setSelection(sel);
			}
			else
			{
				editor->setCaretOffset(sel.second);
			}
		}

		updatePosAndSel();
	}
}

void MainWindow::quickMark()
{
	MarkPtr mark = make_shared<Mark>();
	size_t offset;
	size_t size;

	if (editor->hasSelection())
	{
		offset = editor->selectionOffset();
		size = editor->selectionLength();
	}
	else
	{
		offset = editor->caretOffset();
		size = 1;
	}

	mark->setColor(editor->suggestMarkColor(offset, size));
	
	// Here's a quick test
	// TODO Improve this
	switch (size)
	{
	case 1:
		mark->setInterpretation(make_shared<UInt8Interp>());
		break;
	case 2:
		mark->setInterpretation(make_shared<UInt16Interp>());
		break;
	case 4:
		mark->setInterpretation(make_shared<UInt32Interp>());
		break;
	}

	doc->marks().add(mark, offset, size);
	doc->marksChanged();

	updateDocument();
}

void MainWindow::mark()
{
	todo("mark");
}

void MainWindow::removeMarks()
{
	size_t offset;
	size_t size;

	if (editor->hasSelection())
	{
		offset = editor->selectionOffset();
		size = editor->selectionLength();
	}
	else
	{
		offset = editor->caretOffset();
		size = 1;
	}
	
	// TODO might want to switch this to something else
	std::vector<MarkPtr> marks = doc->marks().contained(offset, offset + size - 1);
	if (marks.empty())
	{
		marks = doc->marks().range(offset, offset + size - 1);
	}
	
	for (MarkPtr& mark : marks)
	{
		doc->marks().remove(mark);
	}

	doc->marksChanged();
	
	updateDocument();
}

void MainWindow::viewInspector()
{
	assert(inspector);
	
	inspector->setVisible(!inspector->isVisible());
}

void MainWindow::viewMarks()
{
	assert(marksWindow);
	
	marksWindow->setVisible(!marksWindow->isVisible());
}

void MainWindow::littleEndian()
{
	doc->setDefaultEndian(Interpretation::LITTLE);
	inspector->setDefaultEndian(doc->defaultEndian());
	updateDocument();
}

void MainWindow::bigEndian()
{
	doc->setDefaultEndian(Interpretation::BIG);
	inspector->setDefaultEndian(doc->defaultEndian());
	updateDocument();
}

void MainWindow::increaseLineLength()
{
	editor->setLineLength(editor->lineLength() + 1);
	updateDocument();
	
	statusMessage(QString(tr("line length is now %1")).arg(editor->lineLength()));
}

void MainWindow::decreaseLineLength()
{
	editor->setLineLength(editor->lineLength() - 1);
	updateDocument();
	
	statusMessage(QString(tr("line length is now %1")).arg(editor->lineLength()));
}

void MainWindow::resetLineLength()
{
	// TODO Read value from config
	editor->setLineLength(16);
	updateDocument();
	
	statusMessage(QString(tr("line length is reset to %1")).arg(editor->lineLength()));
}

void MainWindow::about()
{
	QMessageBox::about(this, tr("About Trehe"),
		tr("<center><h2>Trehe</h2></center>The Reverse Engineering Hex Editor<br/>"
		"<em>Created to help disect unknown file formats.</em><br/>"
		"<br/>Website: http://trehe.com<br/>"
		"Email: contact@trehe.com<br/>"
		"<br/>Copyright &copy; 2014 Ben Moench"));
}

void MainWindow::notImplemented()
{
	QMessageBox::about(this, tr("Not Implemented Yet"),
		tr("This functionality has not been implemented yet. Please direct all complaints to contact@trehe.com"));
}

void MainWindow::editorStateChanged()
{
	updatePosAndSel();
}

void MainWindow::statusMessage(const QString& msg)
{
	statusBar()->showMessage(msg, 2000);
}

void MainWindow::overwriteBytes(QByteArray bytes)
{
	if (bytes.size() > 0)
	{
		editor->overwriteBytes(bytes);
	}

	updateInspector();
}

void MainWindow::selectMark(MarkPtr mark)
{
	editor->clearSelection();
	editor->setCaretOffset(mark->offset());

	// Can't call updateDocument here because it will call updateMarks.
	// This is a problem because updateMarks will refresh the marks
	// tree, meaning we can't double click in the marks window.
	updatePosAndSel();
	editor->viewport()->update();
}

void MainWindow::createEditor()
{
	// Create the editor
	editor = new HexEditorWidget(this, true, app->theme());
	editor->setObjectName("editor");
	editor->setCustomContextMenu(true);
	setCentralWidget(editor);

	// Create a new document with the filename "untitled"
	doc = make_shared<Document>(tr("untitled").toUtf8().constData());
	editor->setDocument(doc);
	
	// Connect us to the editor to get the updates
	connect(editor, SIGNAL(stateChanged()), this, SLOT(editorStateChanged()));
	connect(editor, SIGNAL(statusMessage(const QString&)), this, SLOT(statusMessage(const QString&)));
}

void MainWindow::createInspector()
{
	inspector = new InspectorWindow(nullptr);
	inspector->setObjectName("inspector");
	inspector->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	
	addDockWidget(Qt::RightDockWidgetArea, inspector);

	connect(inspector, SIGNAL(updateBytes(QByteArray)), this, SLOT(overwriteBytes(QByteArray)));
}

void MainWindow::createMarksWindow()
{
	marksWindow = new MarksWindow(nullptr);
	marksWindow->setObjectName("marksWindow");
	marksWindow->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	
	addDockWidget(Qt::RightDockWidgetArea, marksWindow);

	connect(marksWindow, SIGNAL(markClicked(MarkPtr)), this, SLOT(selectMark(MarkPtr)));
}

void MainWindow::createActions()
{
	// File menu actions
	newAct = new QAction(tr("&New"), this);
	newAct->setShortcuts(QKeySequence::New);
	connect(newAct, SIGNAL(triggered()), this, SLOT(new_()));

	openAct = new QAction(tr("&Open..."), this);
	openAct->setShortcuts(QKeySequence::Open);
	connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

	openFormatAct = new QAction(tr("Open &Format..."), this);
	openFormatAct->setShortcut(QKeySequence(tr("Ctrl+Shift+O")));
	connect(openFormatAct, SIGNAL(triggered()), this, SLOT(openFormat()));

	saveAct = new QAction(tr("&Save"), this);
	saveAct->setShortcuts(QKeySequence::Save);
	connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));
	
	saveAsAct = new QAction(tr("Save &As..."), this);
	saveAsAct->setShortcuts(QKeySequence::SaveAs);
	connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));
	
	reloadAct = new QAction(tr("&Reload"), this);
	reloadAct->setShortcut(QKeySequence(tr("Ctrl+R")));
	connect(reloadAct, SIGNAL(triggered()), this, SLOT(reload()));
	
	readOnlyAct = new QAction(tr("Rea&d Only"), this);
	readOnlyAct->setCheckable(true);
	connect(readOnlyAct, SIGNAL(triggered()), this, SLOT(readOnly()));
	
	quitAct = new QAction(tr("&Exit"), this);
	quitAct->setShortcuts(QKeySequence::Quit);
	connect(quitAct, SIGNAL(triggered()), this, SLOT(quit()));

	// Edit menu actions
	
	undoAct = new QAction(tr("&Undo"), this);
	undoAct->setShortcuts(QKeySequence::Undo);
	connect(undoAct, SIGNAL(triggered()), this, SLOT(notImplemented()));
	
	redoAct = new QAction(tr("&Redo"), this);
	redoAct->setShortcuts(QKeySequence::Redo);
	connect(redoAct, SIGNAL(triggered()), this, SLOT(notImplemented()));

	copyAsValueAct = new QAction(tr("&Value"), this);
	connect(copyAsValueAct, SIGNAL(triggered()), this, SLOT(copyAsValue()));

	copyAsHexStringAct = new QAction(tr("&Hex String"), this);
	connect(copyAsHexStringAct, SIGNAL(triggered()), this, SLOT(copyAsHexString()));

	copyAsEscStringAct = new QAction(tr("&Esc String"), this);
	connect(copyAsEscStringAct, SIGNAL(triggered()), this, SLOT(copyAsEscString()));

	copyAsBase64Act = new QAction(tr("&Base64"), this);
	connect(copyAsBase64Act, SIGNAL(triggered()), this, SLOT(copyAsBase64String()));

	pasteAsValueAct = new QAction(tr("&Value"), this);
	connect(pasteAsValueAct, SIGNAL(triggered()), this, SLOT(pasteAsValue()));

	pasteAsHexStringAct = new QAction(tr("&Hex String"), this);
	connect(pasteAsHexStringAct, SIGNAL(triggered()), this, SLOT(pasteAsHexString()));

	pasteAsBase64Act = new QAction(tr("&Base64"), this);
	connect(pasteAsBase64Act, SIGNAL(triggered()), this, SLOT(pasteAsBase64String()));

	/*cutAct = new QAction(tr("Cu&t"), this);
	cutAct->setShortcuts(QKeySequence::Cut);
	connect(cutAct, SIGNAL(triggered()), this, SLOT(cut()));*/
	cutAct = editor->cutAct;

	/*copyAct = new QAction(tr("&Copy"), this);
	copyAct->setShortcuts(QKeySequence::Copy);
	connect(copyAct, SIGNAL(triggered()), this, SLOT(copy()));*/
	copyAct = editor->copyAct;

	/*pasteAct = new QAction(tr("&Paste"), this);
	pasteAct->setShortcuts(QKeySequence::Paste);
	connect(pasteAct, SIGNAL(triggered()), this, SLOT(paste()));*/
	pasteAct = editor->pasteAct;

	/*pasteFormatAct = new QAction(tr("Paste &Format..."), this);
	pasteFormatAct->setShortcut(QKeySequence(tr("Ctrl+Shift+V")));
	connect(pasteFormatAct, SIGNAL(triggered()), this, SLOT(pasteFormat()));*/
	//pasteFormatAct = editor->pasteFormatAct;
	
	/*deleteAct = new QAction(tr("&Delete"), this);
	deleteAct->setShortcuts(QKeySequence::Delete);
	connect(deleteAct, SIGNAL(triggered()), this, SLOT(delete_()));*/
	deleteAct = editor->deleteAct;

	/*selectAllAct = new QAction(tr("Select &All"), this);
	selectAllAct->setShortcuts(QKeySequence::SelectAll);
	connect(selectAllAct, SIGNAL(triggered()), this, SLOT(selectAll()));*/
	selectAllAct = editor->selectAllAct;
	
	/*insertModeAct = new QAction(tr("&Insert Mode"), this);
	//insertModeAct->setShortcut(QKeySequence(tr("Ins")));
	connect(insertModeAct, SIGNAL(triggered()), this, SLOT(insertMode()));*/
	insertModeAct = editor->insertModeAct;
	insertModeAct->setCheckable(true);

	insertFileAct = new QAction(tr("I&nsert File..."), this);
	connect(insertFileAct, SIGNAL(triggered()), this, SLOT(notImplemented()));

	saveToFileAct = new QAction(tr("&Save to File..."), this);
	connect(saveToFileAct, SIGNAL(triggered()), this, SLOT(saveToFile()));
	
	findAct = new QAction(tr("&Find..."), this);
	findAct->setShortcut(QKeySequence(tr("Ctrl+F")));
	connect(findAct, SIGNAL(triggered()), this, SLOT(find()));

	findNextAct = new QAction(tr("Find &Next"), this);
	findNextAct->setShortcut(QKeySequence(tr("F3")));
	connect(findNextAct, SIGNAL(triggered()), this, SLOT(findNext()));

	findPrevAct = new QAction(tr("Find &Previous"), this);
	findPrevAct->setShortcut(QKeySequence(tr("Shift+F3")));
	connect(findPrevAct, SIGNAL(triggered()), this, SLOT(findPrev()));

	gotoAct = new QAction(tr("&Goto..."), this);
	gotoAct->setShortcut(QKeySequence(tr("Ctrl+G")));
	connect(gotoAct, SIGNAL(triggered()), this, SLOT(goto_()));
	
	selectBlockAct = new QAction(tr("Select &Block..."), this);
	selectBlockAct->setShortcut(QKeySequence(tr("Ctrl+B")));
	connect(selectBlockAct, SIGNAL(triggered()), this, SLOT(selectBlock()));

	quickMarkAct = new QAction(tr("Quick &Mark"), this);
	quickMarkAct->setShortcut(QKeySequence(tr("Ctrl+E")));
	connect(quickMarkAct, SIGNAL(triggered()), this, SLOT(quickMark()));

	markAct = new QAction(tr("M&ark..."), this);
	markAct->setShortcut(QKeySequence(tr("Ctrl+Shift+E")));
	connect(markAct, SIGNAL(triggered()), this, SLOT(mark()));
	
	removeMarksAct = new QAction(tr("&Remove Marks"), this);
	connect(removeMarksAct, SIGNAL(triggered()), this, SLOT(removeMarks()));
	
	inspectorAct = new QAction(tr("&Inspector"), this);
	inspectorAct->setShortcut(QKeySequence(tr("Ctrl+I")));
	connect(inspectorAct, SIGNAL(triggered()), this, SLOT(viewInspector()));
	
	marksAct = new QAction(tr("&Marks"), this);
	marksAct->setShortcut(QKeySequence(tr("Ctrl+M")));
	connect(marksAct, SIGNAL(triggered()), this, SLOT(viewMarks()));
	
	littleEndianAct = new QAction(tr("&Little Endian"), this);
	littleEndianAct->setCheckable(true);
	connect(littleEndianAct, SIGNAL(triggered()), this, SLOT(littleEndian()));
	
	bigEndianAct = new QAction(tr("&Big Endian"), this);
	bigEndianAct->setCheckable(true);
	connect(bigEndianAct, SIGNAL(triggered()), this, SLOT(bigEndian()));
	
	increaseLineLengthAct = new QAction(tr("I&ncrease Line Length"), this);
	increaseLineLengthAct->setShortcut(QKeySequence(tr("Ctrl++")));
	connect(increaseLineLengthAct, SIGNAL(triggered()), this, SLOT(increaseLineLength()));

	decreaseLineLengthAct = new QAction(tr("&Decrease Line Length"), this);
	decreaseLineLengthAct->setShortcut(QKeySequence(tr("Ctrl+-")));
	connect(decreaseLineLengthAct, SIGNAL(triggered()), this, SLOT(decreaseLineLength()));
	
	resetLineLengthAct = new QAction(tr("&Reset Line Length"), this);
	resetLineLengthAct->setShortcut(QKeySequence(tr("Ctrl+*")));
	connect(resetLineLengthAct, SIGNAL(triggered()), this, SLOT(resetLineLength()));

	// Help menu actions
	aboutAct = new QAction(tr("&About"), this);
	connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));
}

void MainWindow::createMenus()
{
	fileMenu = menuBar()->addMenu(tr("&File"));
	fileMenu->addAction(newAct);
	fileMenu->addAction(openAct);
	fileMenu->addAction(reloadAct);
	fileMenu->addAction(saveAct);
	fileMenu->addAction(saveAsAct);
	fileMenu->addSeparator();
	fileMenu->addAction(readOnlyAct);
	fileMenu->addSeparator();
	fileMenu->addAction(quitAct);

	editMenu = menuBar()->addMenu(tr("&Edit"));
	editMenu->addAction(undoAct);
	editMenu->addAction(redoAct);
	editMenu->addSeparator();
	editMenu->addAction(cutAct);
	editMenu->addAction(copyAct);
	copyAsMenu = editMenu->addMenu(tr("C&opy As"));
	editMenu->addAction(pasteAct);
	pasteAsMenu = editMenu->addMenu(tr("Pa&ste As"));
	editMenu->addAction(deleteAct);
	editMenu->addSeparator();
	editMenu->addAction(selectAllAct);
	editMenu->addSeparator();
	editMenu->addAction(insertFileAct);
	editMenu->addAction(saveToFileAct);
	editMenu->addSeparator();
	editMenu->addAction(insertModeAct);

	copyAsMenu->addAction(copyAsValueAct);
	copyAsMenu->addAction(copyAsHexStringAct);
	copyAsMenu->addAction(copyAsBase64Act);
	copyAsMenu->addAction(copyAsEscStringAct);

	pasteAsMenu->addAction(pasteAsValueAct);
	pasteAsMenu->addAction(pasteAsHexStringAct);
	pasteAsMenu->addAction(pasteAsBase64Act);

	searchMenu = menuBar()->addMenu(tr("&Search"));
	searchMenu->addAction(findAct);
	searchMenu->addAction(findNextAct);
	searchMenu->addAction(findPrevAct);
	searchMenu->addSeparator();
	searchMenu->addAction(gotoAct);
	searchMenu->addAction(selectBlockAct);

	marksMenu = menuBar()->addMenu(tr("&Marks"));
	marksMenu->addAction(quickMarkAct);
	marksMenu->addAction(markAct);
	marksMenu->addAction(removeMarksAct);
	
	viewMenu = menuBar()->addMenu(tr("&View"));
	viewMenu->addAction(inspectorAct);
	viewMenu->addAction(marksAct);
	viewMenu->addSeparator();
	viewMenu->addAction(littleEndianAct);
	viewMenu->addAction(bigEndianAct);
	viewMenu->addSeparator();
	viewMenu->addAction(increaseLineLengthAct);
	viewMenu->addAction(decreaseLineLengthAct);
	viewMenu->addAction(resetLineLengthAct);

	helpMenu = menuBar()->addMenu(tr("&Help"));
	helpMenu->addAction(aboutAct);
}

void MainWindow::contextMenuEvent(QContextMenuEvent* event)
{
	assert(event);
	
	QMenu menu(this);
	menu.addAction(cutAct);
	menu.addAction(copyAct);
	menu.addMenu(copyAsMenu);
	menu.addAction(pasteAct);
	menu.addMenu(pasteAsMenu);
	menu.addAction(deleteAct);
	menu.addSeparator();
	menu.addAction(selectAllAct);
	menu.addSeparator();
	menu.addAction(insertFileAct);
	menu.addAction(saveToFileAct);
	menu.addSeparator();
	menu.addAction(quickMarkAct);
	menu.addAction(markAct);
	menu.addAction(removeMarksAct);
	menu.exec(event->globalPos());
}

void MainWindow::createStatusBar()
{
	// The text of these labels is just a placeholder which will be
	// quickly overwritten.
	mainStatus = new QLabel("offset", this);
	overwriteStatus = new QLabel("ovr", this);
	fileSizeStatus = new QLabel("filesize", this);
	
	statusBar()->addWidget(mainStatus, 10);
	statusBar()->addWidget(overwriteStatus, 1);
	statusBar()->addWidget(fileSizeStatus, 5);
}

static QString easyNumber(size_t number)
{
	QString result;
	QTextStream stream(&result);
	stream << "0x" << uppercasedigits << hex << number << " | " << dec << number << "";
	return result;
}

void MainWindow::updateDocument()
{
	assert(doc);

	updateMarks();
	
	// Update the title bar with the name of the document.
	const QString name = QFileInfo(doc->path().c_str()).fileName();
	setWindowTitle(QString("%1[*] - Trehe").arg(name));
	
	// Update any menu items
	readOnlyAct->setChecked(doc->isReadOnly());
	updateEndianCheckmark();
	
	// Position and selection
	updatePosAndSel();
	
	// Redraw the editor window
	editor->viewport()->update();
}

void MainWindow::updatePosAndSel()
{
	assert(doc);
	
	// Update the modified flag. Qt will automagically put a star in
	// the right place in the title.
	setWindowModified(doc->isModified());
	
	// Update the state of the cut and copy menu items
	bool hasSel = editor->hasSelection();
	cutAct->setEnabled(hasSel);
	copyAct->setEnabled(hasSel);
	copyAsMenu->setEnabled(hasSel);
	saveToFileAct->setEnabled(hasSel);

	insertModeAct->setChecked(editor->insertMode());

	// Update the state of the mark menu items
	bool caretOnByteOrSel = editor->hasSelection() ||
		(editor->caretOffset() < doc->size());

	quickMarkAct->setEnabled(caretOnByteOrSel);
	markAct->setEnabled(caretOnByteOrSel);

	// Update the inspector window
	updateInspector();

	// TODO Why is this here?
	// Update only mark values
	//marksWindow->updateValues(*doc);
	
	// Update the status bar
	QString main;
	if (editor->hasSelection())
	{
		HexEditorWidget::Selection sel = editor->selection();
		size_t start = sel.first;
		size_t end = sel.second + 1;
		size_t size = editor->selectionLength();
		
		main = tr("<b>selected:</b> %1 to %2; size %3 byte%4").arg(
			easyNumber(start),
			easyNumber(end),
			easyNumber(size),
			size != 1 ? "s" : ""
		);
		
	}
	else
	{
		main = tr("<b>offset:</b> %1").arg(
			easyNumber(editor->caretOffset())
		);
	}
	
	QString overwrite;
	if (editor->insertMode())
		overwrite = tr("<b>ins</b>");
	else
		overwrite = tr("<b>ovr</b>");
	
	QString fileSize = tr("<b>filesize:</b> %1").arg(
		easyNumber(doc->size())
	);
	
	mainStatus->setText(main);
	overwriteStatus->setText(overwrite);
	fileSizeStatus->setText(fileSize);
}

void MainWindow::updateInspector()
{
	const size_t maxLength = 64;
	
	QByteArray data;
	if (editor->hasSelection())
	{
		data = editor->getBytes(maxLength);
	}
	else
	{
		// Either 64 bytes or as many bytes left in the file
		size_t length = std::min(doc->size() - editor->caretOffset(), maxLength);
		data = editor->getBytes(editor->caretOffset(), length);
	}		
	inspector->updateValues(data);
}

void MainWindow::updateMarks()
{
	marksWindow->updateAll(doc->marks().all(), *doc);
	marksWindow->updateValues(*doc);
}

void MainWindow::updateEndianCheckmark()
{
	Interpretation::Endian value = doc->defaultEndian();
	
	littleEndianAct->setChecked(value == Interpretation::LITTLE);
	bigEndianAct->setChecked(value == Interpretation::BIG);
}

bool MainWindow::checkModified()
{
	if (doc->isModified())
	{
		QMessageBox::StandardButton ret;
		ret = QMessageBox::warning(this, tr("Save File"),
			tr("The document has been modified.\n"
			"Do you want to save your changes?"),
			QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel
		);
		
		if (ret == QMessageBox::Save)
			return saveFile();
		else if (ret == QMessageBox::Cancel)
			return false;
     }
     return true;
}

void MainWindow::showOpenFileError(const QString& filename)
{
	QMessageBox::warning(this, tr("Error"), tr("Unable to load file '") + filename + tr("'."));
}

void MainWindow::showFileTooLargeError(const QString& filename)
{
	QMessageBox::warning(this, tr("Error"), tr("Unable to load file '") + filename + tr("'."));
}

void MainWindow::todo(const QString& what)
{
	QMessageBox::information(this, tr("TODO"), tr("TODO: ") + what);
}
