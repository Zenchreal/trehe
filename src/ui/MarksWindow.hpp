/*
 * Put license here.
 *
 * Copyright (C) 2013 Ben Moench.
 */
#pragma once
#include <vector>
#include <set>
#include <QDockWidget>
#include <QMenu>
#include <json/json.h>
#include "bytes/Mark.hpp"
#include "bytes/Document.hpp"
#include "SmartPtr.hpp"

class QTreeView;
class QStandardItem;
class QStandardItemModel;
class QModelIndex;
class QContextMenuEvent;
class QItemSelection;


class MarksWindow : public QDockWidget
{
	Q_OBJECT
	
public:
	MarksWindow(QWidget* parent);

	/**
	 * This function should be depricated.
	 */
	void updateAll(const std::vector<MarkPtr>& marks, const Document& doc);
	void updateHierarchy(const MarksHierarchy& marksHier);
	void updateValues(const Document& doc);
	
	/**
	 * Load the widget's state from the given configuration value.
	 */
	void loadState(const Json::Value& in);
	
	/**
	 * Save the widget's state to the given configuration value.
	 */
	void saveState(Json::Value& out) const;

signals:
	void markClicked(MarkPtr mark);

protected slots:
	void itemChanged(QStandardItem* item);
	void itemClicked(const QModelIndex& index);
	void itemExpanded(const QModelIndex& index);
	void itemCollapsed(const QModelIndex& index);
	void selectionChanged(const QItemSelection& selected, const QItemSelection& deselected);

	void editClicked();
	void removeClicked();
	void copyClicked();
	void columnsClicked();

protected:
	virtual void contextMenuEvent(QContextMenuEvent* event);

	std::set<MarkPtr> selectedMarks() const;
	
private:
	void createContextMenu();
	MarkPtr getMark(const QModelIndex& index) const;
	MarkPtr getMark(const QStandardItem* item) const;
	
	/**
	 * Recursively add QStandardItems to the model based on the given
	 * MarksHierarchy.
	 */
	void doUpdateHierarchy(const MarksHierarchy::Node& node,
		QStandardItem* name);

	void doUpdateValues(const Document& doc,
		QStandardItem* name,
		QStandardItem* type,
		QStandardItem* value);

	QTreeView* tree;
	QStandardItemModel* model;
	bool blockItemChanged;

	// Context menu actions
	QAction* editAct;
	QAction* resizeAct;
	QAction* removeAct;
	QAction* cutTreeAct;
	QAction* copyTreeAct;
	QAction* pasteTreeAct;
	QAction* removeTreeAct;
	QAction* columnsAct;
	QMenu contextMenu;
};
