/*
 * Put license here.
 *
 * Copyright (C) 2014 Ben Moench.
 *
 * Facilitate copy and paste functionality for multiple formats
 */
#include "CopyPasteHandler.hpp"
#include "HexEditorWidget.hpp"
#include <QClipboard>
#include <QMimeData>
#include <QApplication>
#include <QMessageBox>
#include <QDebug>
#include <QByteArray>


CopyPasteHandler::CopyPasteHandler(HexEditorWidget* editor):
	editor(editor),
	copyWarningSize(50 * 1024 * 1024),
	copyMaximumSize(512 * 1024 * 1024)
{
	assert(editor != nullptr);
}

bool CopyPasteHandler::copy() const
{
	if (!checkSelectionSize())
		return false;

	// Get bytes and convert to hex
	QByteArray data = editor->getBytes();
	QString hexString;
	valueToHex(data, hexString);

	// Create mime data
	QMimeData* mime = new QMimeData();
	mime->setData("application/octet-stream", data);
	mime->setText(hexString);

	setClipboard(mime);
	return true;
}

bool CopyPasteHandler::copyAsValue() const
{
	if (!checkSelectionSize())
		return false;
		
	// Get bytes and convert to hex
	QByteArray data = editor->getBytes();

	// Create mime data
	QMimeData* mime = new QMimeData();
	mime->setText(data);

	setClipboard(mime);
	return true;
}

bool CopyPasteHandler::copyAsHexString() const
{
	if (!checkSelectionSize())
		return false;
		
	// Get bytes and convert to hex
	QByteArray data = editor->getBytes();
	QString hexString;
	valueToHex(data, hexString);

	// Create mime data
	QMimeData* mime = new QMimeData();
	mime->setText(hexString);

	setClipboard(mime);
	return true;
}

bool CopyPasteHandler::copyAsEscString() const
{
	if (!checkSelectionSize())
		return false;
		
	// Get bytes and convert to hex
	QByteArray data = editor->getBytes();
	QString hexString;
	valueToEscString(data, hexString);

	// Create mime data
	QMimeData* mime = new QMimeData();
	mime->setText(hexString);

	setClipboard(mime);
	return true;
}

bool CopyPasteHandler::copyAsBase64String() const
{
	if (!checkSelectionSize())
		return false;
		
	// Get bytes and convert to hex
	QByteArray data = editor->getBytes();
	QString encodedString;
	valueToBase64(data, encodedString);

	// Create mime data
	QMimeData* mime = new QMimeData();
	mime->setText(encodedString);

	setClipboard(mime);
	return true;
}

bool CopyPasteHandler::paste()
{
	const QMimeData* mime = clipboard();
	QByteArray bytes;
	
	// This is the order in which we should check the clipboard:
	if (mime->hasFormat("application/octet-stream"))
	{
		bytes = mime->data("application/octet-stream");
	}
	else if (mime->hasText())
	{
		bytes = mime->text().toUtf8();
	}

	if (bytes.size() > 0)
	{
		editor->putBytes(bytes);
		return true;
	}
	return false;
}

bool CopyPasteHandler::pasteAsValue()
{
	const QMimeData* mime = clipboard();
	QByteArray bytes;
	
	// This is the order in which we should check the clipboard:
	if (mime->hasFormat("application/octet-stream"))
	{
		bytes = mime->data("application/octet-stream");
	}
	else if (mime->hasText())
	{
		bytes = mime->text().toUtf8();
	}

	if (bytes.size() > 0)
	{
		editor->putBytes(bytes);
		return true;
	}
	return false;
}

bool CopyPasteHandler::pasteAsHexString()
{
	const QMimeData* mime = clipboard();
	QString hexStr;
	QByteArray bytes;
	
	// This is the order in which we should check the clipboard:
	if (mime->hasFormat("application/octet-stream"))
	{
		hexStr = mime->data("application/octet-stream");
	}
	else if (mime->hasText())
	{
		hexStr = mime->text();
	}

	hexToValue(hexStr, bytes);

	if (bytes.size() > 0)
	{
		editor->putBytes(bytes);
		return true;
	}
	return false;
}

bool CopyPasteHandler::pasteAsBase64String()
{
	const QMimeData* mime = clipboard();
	QString encodedStr;
	QByteArray bytes;
	
	// This is the order in which we should check the clipboard:
	if (mime->hasFormat("application/octet-stream"))
	{
		encodedStr = mime->data("application/octet-stream");
	}
	else if (mime->hasText())
	{
		encodedStr = mime->text();
	}

	base64ToValue(encodedStr, bytes);

	if (bytes.size() > 0)
	{
		editor->putBytes(bytes);
		return true;
	}
	return false;
}

void CopyPasteHandler::setClipboard(QMimeData* mime) const
{
	QClipboard* clip = QApplication::clipboard();
	clip->setMimeData(mime);
}

const QMimeData* CopyPasteHandler::clipboard() const
{
	QClipboard* clip = QApplication::clipboard();
	return clip->mimeData();
}

bool CopyPasteHandler::checkSelectionSize() const
{
	size_t size = editor->selectionLength();

	if (size < 1)
	{
		// Can't have empty selection
		return false;
	}
	else if (size > copyMaximumSize)
	{
		// Selection too big, simply fail
		QMessageBox::warning(editor, editor->tr("Selection too big"), editor->tr(
			"You are trying to copy %1 MiB, which is too large to fit on the clipboard. "
			"Try saving the selection as a file instead.").arg(size / (1024 * 1024)));
			
		return false;
	}
	else if (size > copyWarningSize)
	{
		// Selection questionably large, question the user
		QMessageBox::StandardButton reply;
		reply = QMessageBox::question(editor, editor->tr("Selection too big"), editor->tr(
			"You are trying to copy %1 MiB, which is fairly large for the clipboard. "
			"Are you sure you want to copy this to the clipboard?").arg(size / (1024 * 1024)),
			QMessageBox::Yes | QMessageBox::No);

		if (reply == QMessageBox::Yes)
			return true;
		else
			return false;
	}
	return true;
}

void CopyPasteHandler::valueToHex(const QByteArray& data, QString& hexStr)
{
	QTextStream out(&hexStr);
	
	hexStr.reserve(2 * data.size());
	out << hex << uppercasedigits << qSetFieldWidth(2) << qSetPadChar('0');
	
	for (int i = 0; i < data.size(); i++)
	{
		out << ((uint) data[i] & 0xFF);
	}
}

void CopyPasteHandler::hexToValue(const QString& hexStr, QByteArray& data)
{
	data = QByteArray();
	for (QChar c : hexStr)
	{
		if ((c >= '0' && c <= '9') ||
			(c >= 'A' && c <= 'F') ||
			(c >= 'a' && c <= 'f'))
		{
			data.append(c);
		}
	}

	data = QByteArray::fromHex(data);
}

void CopyPasteHandler::valueToEscString(const QByteArray& data, QString& hexStr)
{
	QTextStream out(&hexStr);
	
	hexStr.reserve(2 * data.size());
	out << hex << uppercasedigits << qSetFieldWidth(1) << qSetPadChar('0');
	
	for (int i = 0; i < data.size(); i++)
	{
		if (data[i] == 0x0D)
		{
			out << "\\r";
		}
		else if (data[i] == 0x0A)
		{
			out << "\\n";
		}
		else if (data[i] >= 0x20 && data[i] <= 0x7E)
		{
			out << QChar(data[i]);
		}
		else
		{
			out << "\\x" << qSetFieldWidth(2) << ((uint) data[i] & 0xFF) << qSetFieldWidth(1);
		}
	}
}

void CopyPasteHandler::valueToBase64(const QByteArray& data, QString& encodedStr)
{
	encodedStr = data.toBase64().constData();
}

void CopyPasteHandler::base64ToValue(const QString& encodedStr, QByteArray& data)
{
	data = QByteArray::fromBase64(QByteArray((const char*) encodedStr.toUtf8().constData(), encodedStr.size()));
}
