/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#include "SelectBlockDialog.hpp"
#include "ui_SelectBlockDialog.h"
#include "parsers/Expression.hpp"


SelectBlockDialog::SelectBlockDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::SelectBlockDialog),
    backwardsResult(false),
    sizeResult(0)
{
	ui->setupUi(this);

	connect(this, SIGNAL(accepted()), SLOT(saveResult()));
}

SelectBlockDialog::~SelectBlockDialog()
{
	delete ui;
}

bool SelectBlockDialog::backwards() const
{
	return backwardsResult;
}

size_t SelectBlockDialog::selectionSize() const
{
	return sizeResult;
}

void SelectBlockDialog::saveResult()
{
	QString str = ui->lineEdit->text();

	backwardsResult = ui->backwardsCheckBox->isChecked();

	std::string input(str.toUtf8().constData());
	if (!parse_size_t(input, sizeResult))
	{
		// TODO show syntax error dialog
	}
}
