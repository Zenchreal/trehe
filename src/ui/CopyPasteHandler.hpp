/*
 * Put license here.
 *
 * Copyright (C) 2014 Ben Moench.
 *
 * Facilitate copy and paste functionality for multiple formats
 */
#pragma once
#include <cstddef>
#include <QString>
#include <QByteArray>

class QMimeData;
class HexEditorWidget;


class CopyPasteHandler
{
public:
	/**
	 * Construct the handler with and instance of the hex editor widget.
	 */
	CopyPasteHandler(HexEditorWidget* editor);

	/**
	 * Copy to the clipboard using default settings.
	 */
	bool copy() const;

	/**
	 * Copy to the clipboard value for value.
	 */
	bool copyAsValue() const;

	/**
	 * Copy to the clipboard encoding as a hex string.
	 */
	bool copyAsHexString() const;

	/**
	 * Copy to the clipboard encoding as an escaped string.
	 */
	bool copyAsEscString() const;

	/**
	 * Copy to the clipboard encoding as a base64 string.
	 */
	bool copyAsBase64String() const;

	/**
	 * Paste to the clipboard using default settings.
	 */
	bool paste();

	/**
	 * Paste from the clipboard value for value.
	 */
	bool pasteAsValue();

	/**
	 * Paste from the clipboard decoding as a hex string.
	 */
	bool pasteAsHexString();

	/**
	 * Paste from the clipboard decoding as a base64 string.
	 */
	bool pasteAsBase64String();

protected:
	HexEditorWidget* editor;
	size_t copyWarningSize;
	size_t copyMaximumSize;

	void setClipboard(QMimeData* mime) const;
	const QMimeData* clipboard() const;
	bool checkSelectionSize() const;
	static void valueToHex(const QByteArray& data, QString& hexStr);
	static void hexToValue(const QString& hexStr, QByteArray& data);
	static void valueToEscString(const QByteArray& data, QString& hexStr);
	//static void escStringToValue(const QString& hexStr, QByteArray& data);
	static void valueToBase64(const QByteArray& data, QString& encodedStr);
	static void base64ToValue(const QString& encodedStr, QByteArray& data);
};
