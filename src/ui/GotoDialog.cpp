/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 */
#include "GotoDialog.hpp"
#include "ui_GotoDialog.h"
#include "parsers/Expression.hpp"


GotoDialog::GotoDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::GotoDialog)
{
	ui->setupUi(this);

	connect(this, SIGNAL(accepted()), SLOT(saveResult()));
}

GotoDialog::~GotoDialog()
{
	delete ui;
}

size_t GotoDialog::result() const
{
	return resultOffset;
}

void GotoDialog::saveResult()
{
	QString str = ui->lineEdit->text();

	std::string input(str.toUtf8().constData());
	if (!parse_size_t(input, resultOffset))
	{
		// show syntax error dialog
	}
}
