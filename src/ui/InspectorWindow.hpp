/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Window containing the inspector widget
 */
#pragma once
#include <cstdint>
#include <vector>
#include <QWidget>
#include <QDockWidget>
#include <QStandardItemModel>
#include <QMenu>
#include <json/json.h>
#include "SmartPtr.hpp"
#include "bytes/Interpretation.hpp"

class QTreeView;
class InspectorWidget;
class QKeyEvent;
class QContextMenuEvent;
class QAction;
class QMimeData;


class InspectorWindow : public QDockWidget
{
	Q_OBJECT
	
public:
	InspectorWindow(QWidget* parent);
	
	/**
	 * Set the default endian.
	 */
	void setDefaultEndian(Interpretation::Endian flag);
	
	/**
	 * Get the default endian.
	 */
	Interpretation::Endian defaultEndian() const;

	/**
	 * Should be called whenever the caret or selection changes, to keep
	 * the inspector window up to date.
	 */
	void updateValues(QByteArray bytes);
	
	/**
	 * Load the widget's state from the given configuration value.
	 */
	void loadState(const Json::Value& in);
	
	/**
	 * Save the widget's state to the given configuration value.
	 */
	void saveState(Json::Value& out) const;

signals:
	/**
	 * Signal emitted whenever the user changes a value in the inspector,
	 * and we should update the bytes in the editor.
	 */
	void updateBytes(QByteArray bytes);

protected:
	virtual void keyPressEvent(QKeyEvent* event);
	virtual void contextMenuEvent(QContextMenuEvent* event);

protected slots:
	void itemChanged(QStandardItem* valueItem);
	void more();
	
private:
	void createContextMenu();
	void createItems();
	void appendItem(shared_ptr<Interpretation> interp);
	void appendSpacerItem();

	/**
	 * Override some methods in QStandardItemModel so that when you
	 * drag an item, you can't drop it into the second column instead of
	 * the first. This nearly caused me to lose my sanity.
	 */
	class ItemModel : public QStandardItemModel
	{
	public:
		virtual Qt::ItemFlags flags(const QModelIndex& index) const;
		virtual bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent);
	};
	
	QTreeView* tree;
	ItemModel* model;
	Interpretation::Endian defaultEndianValue;

	bool blockItemChanged;

	// This is a hack for now. We store the last bytes sent to updateValues so
	// we have then for the more dialog.
	QByteArray lastBytes;

	// Context menu actions
	QAction* hideAct;
	QAction* moreAct;
	QMenu contextMenu;
};



