#include "StringInterp.hpp"


std::string CharInterp::name() const
{
	return "char";
}

Interpretation::Result CharInterp::bytesToStringBounded(const uint8_t* bytes, size_t length, std::string& output, unsigned truncateLength) const
{
	using std::min;
	output.clear();
	length = min<size_t>(length, truncateLength);

	if (length < 1)
		return SUCCESS;
	
	output.resize(length);

	for (size_t i = 0; i < length; i++)
	{
		char c = bytes[i];

		if (c < 0x20)
			c = '.';
		else if (c >= 0x7F)
			c = '.';

		output[i] = c;
	}

	return SUCCESS;
}
