/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Read only memory mapped bytes.
 */
#include "MMappedBytes.hpp"
#include "MMappedFileWrapper.hpp"


MMappedBytes::MMappedBytes():
	offsetRegion(0),
	sizeRegion(0)
{
}

MMappedBytes::MMappedBytes(shared_ptr<MMappedFileWrapper> file):
	offsetRegion(0),
	sizeRegion(file->size()),
	file(file)
{
}

MMappedBytes::MMappedBytes(shared_ptr<MMappedFileWrapper> file, size_t offset, size_t size):
	offsetRegion(offset),
	sizeRegion(size),
	file(file)
{
}

MMappedBytes::MMappedBytes(const MMappedBytes& obj):
	offsetRegion(obj.offsetRegion),
	sizeRegion(obj.sizeRegion),
	file(obj.file)
{
}

MMappedBytes& MMappedBytes::operator=(const MMappedBytes& obj)
{
	offsetRegion = obj.offsetRegion;
	sizeRegion = obj.sizeRegion;
	file = obj.file;
	return *this;
}

MMappedBytes::~MMappedBytes()
{
}

const uint8_t* MMappedBytes::ptr() const
{
	if (file && file->isOpen())
	{
		assert(offsetRegion + sizeRegion <= file->size());
		return (uint8_t*) file->ptr() + offsetRegion;
	}
	else
	{
		return nullptr;
	}
}

size_t MMappedBytes::size() const
{
	return sizeRegion;
}

MMappedBytes::SplitResult MMappedBytes::split(off_t where) const
{
	// Make sure the offset is in range
	if (where < 0 || where >= (off_t) size())
	{
		return SplitResult();
	}

	// Create our result
	SplitResult result;
	result.first = make_shared<MMappedBytes>(file, offsetRegion, where);
	result.second = make_shared<MMappedBytes>(file, offsetRegion + where, sizeRegion - where);
	
	result.first->setShowAsModified(showAsModified());
	result.second->setShowAsModified(showAsModified());
	return result;
}
