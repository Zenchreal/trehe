#pragma once
#include <cstdint>
#include <string>
#include <algorithm>
#include <sstream>
#include <cassert>

#include "Interpretation.hpp"


/**
 * Template class for iterpreting basic types. Inherit from a specialized
 * version of this class with the template argument T being the desired type.
 * The class works by reinterpreting the raw bytes as the type T and
 * using stringstream to serialize that type to a string. This could
 * potentially produce system-dependent code, so for things like floats
 * a custom parser would be ideal.
 */
template <typename T> class BasicInterp : public Interpretation
{
public:
	virtual Result bytesToStringBounded(const uint8_t* bytes, size_t length, std::string& output, unsigned truncateLength) const
	{
		assert(bytes != NULL);
		assert(length >= 0);
		
		uint8_t temp[sizeof(T)];
		size_t realLength = std::min(length, sizeof(T));
		
		std::fill(temp, temp + sizeof(T), 0);
		std::copy(bytes, bytes + realLength, temp);

		if (endian() != systemEndian)
		{
			std::reverse(temp, temp + realLength);
		}
		
		T castedBytes = *reinterpret_cast<T*>(temp);

		std::stringstream strStream(std::stringstream::out);

		// Workaround: any sort of one-byte type will print as a
		// character instead of an integer, so we cast to an int.
		if (sizeof(T) == 1)
			strStream << int(castedBytes);
		else
			strStream << castedBytes;
			
		output = strStream.str();
		return SUCCESS;
	}

	virtual Result stringToBytes(const std::string& input, uint8_t* bytes, size_t& length) const
	{
		assert(bytes != NULL);
		assert(length >= 0);
		
		T uncastedBytes;
		std::stringstream strStream(input, std::stringstream::in);

		// Workaround: any sort of one-byte type will input as a
		// character instead of an integer, so we first input an int
		// and then cast it to a T.
		if (sizeof(T) == 1)
		{
			int temporary;
			strStream >> temporary;
			uncastedBytes = static_cast<T>(temporary);
		}
		else
		{
			strStream >> uncastedBytes;
		}

		uint8_t* castedPtr = reinterpret_cast<uint8_t*>(&uncastedBytes);
		size_t realLength = std::min(length, sizeof(T));

		if (endian() != systemEndian)
		{
			std::reverse(castedPtr, castedPtr + sizeof(T));
		}
		
		std::copy(castedPtr, castedPtr + realLength, bytes);

		length = realLength;
		return SUCCESS;
	}
};

/**
 * Macro to make our list of basic integer types much more concise.
 */
#define BASIC_INTERP(_class, _type, _name)							\
	class _class : public BasicInterp<_type>						\
	{																\
	public:														\
		virtual std::string name() const							\
		{															\
			return _name;											\
		}															\
	};


BASIC_INTERP(UInt8Interp, uint8_t, "uint8");
BASIC_INTERP(Int8Interp, int8_t, "int8");
BASIC_INTERP(UInt16Interp, uint16_t, "uint16");
BASIC_INTERP(Int16Interp, int16_t, "int16");
BASIC_INTERP(UInt32Interp, uint32_t, "uint32");
BASIC_INTERP(Int32Interp, int32_t, "int32");
BASIC_INTERP(UInt64Interp, uint64_t, "uint64");
BASIC_INTERP(Int64Interp, int64_t, "int64");
BASIC_INTERP(FloatInterp, float, "float");
BASIC_INTERP(DoubleInterp, double, "double");

#undef BASIC_INTERP

