#pragma once
#include <cstdint>
#include <cstddef>

class SequentialReader;


size_t boyer_moore(SequentialReader& seq, const uint8_t* pat, uint32_t patlen);
