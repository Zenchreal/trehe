#include "Interpretation.hpp"
#include "BasicInterp.hpp"
#include "StringInterp.hpp"


std::map<std::string, Interpretation::InstFunc> Interpretation::interpList;

/**
 * List all interpretations here if you want them to be included in the
 * UI lists.
 */
void Interpretation::initializeList()
{
	InstFunc allInterps[] =
	{
		[] { return make_shared<UInt8Interp>(); },
		[] { return make_shared<Int8Interp>(); },
		[] { return make_shared<UInt16Interp>(); },
		[] { return make_shared<Int16Interp>(); },
		[] { return make_shared<UInt32Interp>(); },
		[] { return make_shared<Int32Interp>(); },
		[] { return make_shared<UInt64Interp>(); },
		[] { return make_shared<Int64Interp>(); },
		[] { return make_shared<FloatInterp>(); },
		[] { return make_shared<DoubleInterp>(); },
		[] { return make_shared<CharInterp>(); }
	};
	
	interpList.clear();
	
	for (unsigned int i = 0; i < sizeof(allInterps) / sizeof(InstFunc); i++)
	{
		InstFunc f = allInterps[i];
		interpList.insert(std::pair<std::string, InstFunc>(f()->name(), f));
	}
}

shared_ptr<Interpretation> Interpretation::instantiate(const std::string& name)
{
	std::map<std::string, InstFunc>::const_iterator iter;
	iter = interpList.find(name);
	if (iter == interpList.end())
	{
		return shared_ptr<Interpretation>();
	}
	
	return iter->second();
}

void Interpretation::listAll(std::vector<shared_ptr<Interpretation>>& list)
{
	for (const std::map<std::string, InstFunc>::value_type& v : interpList)
	{
		list.push_back(v.second());
	}
}

/**
 * TODO Endianness of the system on which we're currently running.
 */
const Interpretation::Endian Interpretation::systemEndian = LITTLE;

std::string Interpretation::description() const
{
	return "No description.";
}

Interpretation::Result Interpretation::bytesToStringBounded(const uint8_t* bytes, size_t length, std::string& output, unsigned truncateLength) const
{
	return METHOD_NOT_APPLICABLE;
}

//Interpretation::Result Interpretation::bytesToStringUnbounded(Reader reader, std::string& output, uint truncateLength) const;

Interpretation::Result Interpretation::stringToBytes(const std::string& input, uint8_t* bytes, size_t& length) const
{
	return METHOD_NOT_APPLICABLE;
}

void Interpretation::serialize(Json::Value& out) const
{
	out["name"] = name();
	out["endian"] = endian();
}

shared_ptr<Interpretation> Interpretation::deserialize(const Json::Value& in)
{
	shared_ptr<Interpretation> result;
	std::string targetName = in.get("name", "").asString();
	
	result = instantiate(targetName);
	
	if (result)
	{
		result->doDeserialize(in);
		return result;
	}
	
	return shared_ptr<Interpretation>();
}

void Interpretation::doDeserialize(const Json::Value& in)
{
	int end = in["endian"].asInt();
	if (end >= LITTLE && end <= BIG)
	{
		setEndian(Endian(end));
	}
}

