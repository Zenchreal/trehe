/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Store an array of bytes that can be inserted into and deleted from.
 * Currently implemented as an std::vector. If this proves too slow, use
 * malloc, realloc, and free.
 */
#pragma once
#include <cstddef>
#include <cstdint>
#include <vector>
#include "Bytes.hpp"


class MemoryBytes : public Bytes
{
public:
	MemoryBytes();
	MemoryBytes(size_t size);
	MemoryBytes(const uint8_t* bytes, size_t size);
	MemoryBytes(const MemoryBytes& obj);
	MemoryBytes& operator=(const MemoryBytes& obj);
	virtual ~MemoryBytes();
	virtual const uint8_t* ptr() const;
	virtual uint8_t* ptr();
	virtual size_t size() const;
	virtual SplitResult split(off_t where) const;

private:
	std::vector<uint8_t> byteVec;
};
