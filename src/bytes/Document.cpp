/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * A document
 */
#include "Document.hpp"
#include "BytesMap.hpp"
#include "MMappedFileWrapper.hpp"
#include "MMappedBytes.hpp"
#include "MemoryBytes.hpp"
#include "SmartPtr.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdio>
#include <cstddef>
#include <json/json.h>
#include <boost/filesystem.hpp>

#include "BoyerMoore.hpp"


Document::Document(const std::string& path, bool tempPath):
	fullPath(path),
	modifiedFlag(false),
	tempPathFlag(tempPath),
	readOnlyFlag(false),
	defaultEndianValue(Interpretation::LITTLE)
{
}

Document::~Document()
{
	saveMarks();
}

void Document::reload()
{
	using boost::filesystem::exists;
	using boost::filesystem::file_size;
	
	assert(fullPath.size() > 0);
	
	// Check for the existence of the file
	if (!exists(fullPath))
	{
		throw std::ios_base::failure("File does not exist");
	}
	
	// Check file size before opening it
	uintmax_t fileSize = file_size(fullPath);
	shared_ptr<MMappedFileWrapper> file = make_shared<MMappedFileWrapper>();
	
	// Try opening the file if it isn't empty
	if (fileSize != 0)
	{
		file->open(fullPath);
		if (!file->isOpen())
		{
			throw std::ios_base::failure("Failed to memory map file");
		}
	}

	// We've succeeded, so clear out bytesMap
	bytesMap.clear();
	
	// Add memory mapped part if the file isn't empty
	if (fileSize != 0)
		bytesMap.insert(0, make_shared<MMappedBytes>(file));
		
	modifiedFlag = false;
	tempPathFlag = false;
	
	loadMarks();
}

/**
 * Generate a random string of alphanumberic characters with the given
 * length.
 * TODO Make sure that rand() is at least somewhat random
 * TODO Put this in a helper library or something
 */
static std::string randomString(unsigned int length)
{
	using std::string;
	
	static const char alphanum[] =
		"0123456789"
		"abcdefghijklmnopqrstuvwxyz";

	string out(length, '0');
	for (unsigned int i = 0; i < length; i++)
	{
		out[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
	}
	return out;
}

void Document::save()
{
	using std::string;
	using std::ofstream;
	using std::ios_base;
	using std::vector;
	/*using std::cout;
	using std::endl;*/
	
	assert(fullPath.size() > 0);
	
	// TODO Implement the more complicated and efficient way of saving
	// the file -- only write what has changed to disk.
	
	// Open a temporary file to write everything to
	string tempFileName(fullPath + "." + randomString(8));
	ofstream out;
	out.open(tempFileName.c_str(), ios_base::out | ios_base::binary);
	
	// Write everything out
	BytesMap::Reader reader(bytesMap);
	vector<uint8_t> buffer;
	buffer.resize(1024 * 1024);
	
	while (!reader.end())
	{
		size_t count = reader.read(&buffer[0], buffer.size());
		out.write((char*) &buffer[0], count);
	}
	
	out.close();

	// We're now done with the current bytesMap, clear it out to unlock the file
	// that's memory mapped.
	bytesMap.clear();
	
	// Delete the original if it existed and rename the existing file
	::remove(fullPath.c_str());
	if (::rename(tempFileName.c_str(), fullPath.c_str()) != 0)
	{
		throw ios_base::failure("Failed to rename the output file");
	}
	
	saveMarks();
	
	// And finally reload the the file from disk
	reload();
}

BytesMap::Reader Document::reader() const
{
	return BytesMap::Reader(bytesMap);
}

void Document::setPath(const std::string& path)
{
	if (!path.empty())
	{
		fullPath = path;
		tempPathFlag = false;
	}
}

std::string Document::path() const
{
	return fullPath;
}

void Document::setTempPath(bool tempPath)
{
	tempPathFlag = tempPath;
}

bool Document::isTempPath() const
{
	return tempPathFlag;
}

bool Document::isModified() const
{
	return modifiedFlag;
}

void Document::setReadOnly(bool readOnly)
{
	readOnlyFlag = readOnly;
}

bool Document::isReadOnly() const
{
	return readOnlyFlag;
}

void Document::setDefaultEndian(Interpretation::Endian flag)
{
	defaultEndianValue = flag;
}

Interpretation::Endian Document::defaultEndian() const
{
	return defaultEndianValue;
}

size_t Document::size() const
{
	return bytesMap.size();
}

bool Document::overwrite(size_t offset, uint8_t b)
{
	if (!readOnlyFlag)
	{
		shared_ptr<MemoryBytes> bytes = make_shared<MemoryBytes>(&b, 1);
		bytes->setShowAsModified(true);
		
		if (bytesMap.overwrite(offset, bytes))
		{
			modifiedFlag = true;
			return true;
		}
	}
	return false;
}

bool Document::overwrite(size_t offset, const uint8_t* b, size_t length)
{
	if (length < 1)
		return false;

	if (!readOnlyFlag)
	{
		shared_ptr<MemoryBytes> bytes = make_shared<MemoryBytes>(b, length);
		bytes->setShowAsModified(true);
		
		// If it would extend past the end of the file, insert
		if (offset + length >= size())
		{
			bytesMap.remove(offset, length);
			
			if (bytesMap.insert(offset, bytes))
			{
				modifiedFlag = true;
				return true;
			}
		}
		else
		{
			if (bytesMap.overwrite(offset, bytes))
			{
				modifiedFlag = true;
				return true;
			}
		}
	}
	return false;
}

bool Document::insert(size_t offset, uint8_t b)
{
	if (!readOnlyFlag)
	{
		shared_ptr<MemoryBytes> bytes = make_shared<MemoryBytes>(&b, 1);
		bytes->setShowAsModified(true);
		
		if (bytesMap.insert(offset, bytes))
		{
			modifiedFlag = true;
			return true;
		}
	}
	return false;
}

bool Document::insert(size_t offset, const uint8_t* b, size_t length)
{
	if (!readOnlyFlag)
	{
		shared_ptr<MemoryBytes> bytes = make_shared<MemoryBytes>(b, length);
		bytes->setShowAsModified(true);
		
		if (bytesMap.insert(offset, bytes))
		{
			modifiedFlag = true;
			return true;
		}
	}
	return false;
}

bool Document::remove(size_t offset, size_t length)
{
	if (!readOnlyFlag)
	{
		if (bytesMap.remove(offset, length))
		{
			modifiedFlag = true;
			return true;
		}
	}
	return false;
}

size_t Document::find(size_t start, const uint8_t* b, size_t length, bool forward)
{
	// An empty by sequence always returns -1
	if (!b || length < 1)
		return size_t(-1);
		
	if (start < 1)
		start = 0;
		
	if (start >= size())
		start = size();

	// Reverse the pattern if needed
	const uint8_t* pattern = b;
	uint8_t* reversedPattern = nullptr;
	if (!forward)
	{
		reversedPattern = new uint8_t[length];
		std::reverse_copy(b, b + length, reversedPattern);
		pattern = reversedPattern;
	}
	
	// Just create both readers and select the correct one
	BytesMap::Reader forwardReader(bytesMap);
	BytesMap::ReversedReader reversedReader(bytesMap);
	BytesMap::Reader& reader = forward ? forwardReader : reversedReader;

	// Seek to correct location
	reader.seek(start);
	SequentialReader seq(&reader);
	
	size_t found = boyer_moore(seq, pattern, length);
	
	if (found == size_t(-1))
	{
		// Wasn't found
	}
	else
	{
		if (forward)
			found = start + found;
		else
			found = start - found - length;
	}

	// Clean up the reversed pattern
	delete[] reversedPattern;
	reversedPattern = nullptr;
		
	return found;
}

void Document::saveToFile(const std::string& path, size_t offset, size_t length)
{
	using std::ofstream;
	using std::ios_base;
	using std::vector;
	using std::min;
	
	ofstream out;
	out.open(path.c_str(), ios_base::out | ios_base::binary);
	if (!out.is_open())
	{
		throw ios_base::failure("Could not open output file");
	}
	
	// Write everything out
	BytesMap::Reader reader(bytesMap);
	vector<uint8_t> buffer;
	buffer.resize(1024 * 1024);
	reader.seek(offset);
	
	while (!reader.end() && length > 0)
	{
		size_t count = reader.read(&buffer[0], min(buffer.size(), length));
		out.write((char*) &buffer[0], count);
		length -= count;
	}
	
	out.close();
}

MarksMap& Document::marks()
{
	return marksMap;
}

const MarksMap& Document::marks() const
{
	return marksMap;
}

MarksHierarchy& Document::marksHierarchy()
{
	return marksHier;
}

const MarksHierarchy& Document::marksHierarchy() const
{
	return marksHier;
}

void Document::marksChanged()
{
	marksHierarchy().build(marks().all());
}

void Document::loadMarks()
{
	using std::ifstream;
	using std::ios_base;
	
	// Clear any marks out before loading
	marksMap.clear();
	marksChanged();
	
	ifstream input;
	input.open(fullPath + ".marks", ios_base::in);
	if (!input.is_open())
	{
		// TODO Try opening it from local storage (e.g. in case of
		// read-only filesystem).
		return;
	}
	
	Json::Value root;
	Json::Reader reader;
	if (!reader.parse(input, root))
	{
		// TODO Better logging
		std::cerr << reader.getFormattedErrorMessages() << std::endl;
		return;
	}
	
	marksMap.deserialize(root["marks"]);
	
	input.close();

	marksChanged();
}

void Document::saveMarks()
{
	using std::ofstream;
	using std::ios_base;
	
	std::string filename = fullPath + ".marks";
	
	// If there are no marks, remove the marks file
	if (marksMap.empty())
	{
		::remove(filename.c_str());
		return;
	}
	
	ofstream output;
	output.open(filename, ios_base::out);
	if (!output.is_open())
	{
		// TODO Try saving it to local storage
		return;
	}
	
	Json::Value root;
	root["filename"] = fullPath;
	root["filesize"] = Json::Value::UInt64(size());
	marksMap.serialize(root["marks"]);

	output << root;
	output.close();
}
