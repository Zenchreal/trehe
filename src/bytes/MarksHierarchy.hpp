#pragma once
#include <vector>
#include "Mark.hpp"

using std::vector;


class MarksHierarchy
{
public:
	/**
	 * A node in the hierarchy. Each node is associated with a mark, as well as
	 * any children of that mark.
	 */
	class Node
	{
	public:
		Node();
		Node(MarkPtr mark);

		/**
		 * Return a pointer to the mark associated with this node.
		 */
		MarkPtr ptr() const;

		/**
		 * Try to add the current mark from iter to this node. If that mark doesn't
		 * overlap with this node, return.
		 */
		void build(vector<MarkPtr>::const_iterator& iter,
			vector<MarkPtr>::const_iterator& end);

		void print(int indent) const;

		/**
		 * Iterators
		 */
		typedef vector<Node>::iterator iterator;
		typedef vector<Node>::const_iterator const_iterator;

		iterator begin()
		{
			return children.begin();
		}
		iterator end()
		{
			return children.end();
		}
		const_iterator begin() const
		{
			return children.begin();
		}
		const_iterator end() const
		{
			return children.end();
		}

	private:
		MarkPtr mark;
		vector<Node> children;
	};

	/**
	 * Returns the root node.
	 */
	Node& root();
	const Node& root() const;

	/**
	 * Build the marks hierarchy from scratch, clearing any existing data. These
	 * must be in the same order as is returned by MarksMap (lower offset first).
	 */
	void build(const vector<MarkPtr>& marks);

	/**
	 * Iterators
	 */
	typedef Node::iterator iterator;
	typedef Node::const_iterator const_iterator;

	iterator begin()
	{
		return n.begin();
	}
	iterator end()
	{
		return n.end();
	}
	const_iterator begin() const
	{
		return n.begin();
	}
	const_iterator end() const
	{
		return n.end();
	}

private:
	Node n;
};
