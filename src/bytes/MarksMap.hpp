#pragma once
#include <cstddef>
#include <vector>
#include <map>
#include <set>
#include <json/json.h>
#include "SmartPtr.hpp"
#include "Mark.hpp"

using std::vector;


class MarksMap
{
private:
	
public:
	MarksMap();
	void add(MarkPtr mark, size_t offset, size_t size);
	void remove(MarkPtr mark);
	void modify(MarkPtr mark, size_t newOffset, size_t newSize);
	bool empty() const;
	void clear();

	void insertBytes(size_t offset, size_t size);
	void removeBytes(size_t offset, size_t size);

	vector<MarkPtr> all() const;
	vector<MarkPtr> range(size_t startOffset, size_t endOffset) const;
	vector<MarkPtr> contained(size_t startOffset, size_t endOffset) const;

	void serialize(Json::Value& out) const;
	void deserialize(const Json::Value& in);
	
private:
	typedef std::multimap<size_t, MarkPtr> OffsetMarkMap;

	OffsetMarkMap startOffsetMap;
	OffsetMarkMap endOffsetMap;

	// Compare marks first by their starting offset, then by their ending offset
	struct OffsetEndOffsetCompare
	{
		inline bool operator() (const MarkPtr& lhs, const MarkPtr& rhs)
		{
			if (lhs->startOffset == rhs->startOffset)
			{
				if (lhs->endOffset == rhs->endOffset)
					return lhs < rhs;
				else
					return lhs->endOffset < rhs->endOffset;
			}
			else
				return lhs->startOffset < rhs->startOffset;
		}
	};

	// Compare marks by size (biggest first)
	struct SizeCompare
	{
		inline bool operator() (const MarkPtr& lhs, const MarkPtr& rhs)
		{
			size_t lhsSize = lhs->endOffset - lhs->startOffset + 1;
			size_t rhsSize = rhs->endOffset - rhs->startOffset + 1;

			if (lhsSize == rhsSize)
				return lhs < rhs;
			else
				return lhsSize > rhsSize;
		}
	};

	typedef std::set<MarkPtr, SizeCompare> Chunk;
	typedef std::map<size_t, Chunk> ChunkMap;

	ChunkMap map;

	ChunkMap::iterator newChunkAtOffset(size_t offset);
	ChunkMap::iterator chunkLowerBound(size_t offset);
	ChunkMap::iterator chunkUpperBound(size_t offset);
	ChunkMap::const_iterator chunkLowerBound(size_t offset) const;
	ChunkMap::const_iterator chunkUpperBound(size_t offset) const;
};
