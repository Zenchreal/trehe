#include "Mark.hpp"
#include <json/json.h>
#include "Interpretation.hpp"
#include "BasicInterp.hpp"


bool Mark::overlaps(const Mark& other) const
{
	if (other.offset() > endingOffset())
	{
		return false;
	}
	if (other.endingOffset() < offset())
	{
		return false;
	}
	return true;
}

bool Mark::contains(const Mark& other) const
{
	if (other.offset() < offset())
	{
		return false;
	}
	if (other.endingOffset() > endingOffset())
	{
		return false;
	}
	return true;
}

shared_ptr<Interpretation> Mark::interpretation() const
{
	return interp;
}

void Mark::setInterpretation(shared_ptr<Interpretation> newInterp)
{
	interp = newInterp;
}

shared_ptr<Interpretation> Mark::defaultInterpretation() const
{
	if (interp)
		return interp;
	
	switch (size())
	{
	case 1:
		return make_shared<Int8Interp>();
	case 2:
		return make_shared<Int16Interp>();
	case 4:
		return make_shared<Int32Interp>();
	case 8:
		return make_shared<Int64Interp>();
	default:
		return make_shared<Int8Interp>();
	}
}

Mark::Mark(const Json::Value& in):
	startOffset(-1),
	endOffset(-1)
{
	markColor = in.get("markColor", 0xFFFF0000).asUInt();
	markName = in.get("markName", "").asString();
	stretchableFlag = in.get("stretchableFlag", false).asBool();
	collapsedFlag = in.get("collapsedFlag", false).asBool();
	if (in.isMember("interp"))
		interp = Interpretation::deserialize(in["interp"]);
	
	actualMarkColor = markColor;
}

void Mark::serialize(Json::Value& out) const
{
	out["startOffset"] = Json::Value::UInt64(startOffset);
	out["endOffset"] = Json::Value::UInt64(endOffset);
	out["markColor"] = markColor;
	if (hasName())
		out["markName"] = markName;
	if (stretchableFlag)
		out["stretchableFlag"] = stretchableFlag;
	if (collapsedFlag)
		out["collapsedFlag"] = collapsedFlag;
	if (interpretation())
		interpretation()->serialize(out["interp"]);
}
