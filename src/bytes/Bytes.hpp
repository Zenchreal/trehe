/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Base class for an array of bytes from any source. Once added to a
 * BytesMap, the size ABSOLUTELY MUST NOT change.
 */
#pragma once
#include <cstddef>
#include <cstdint>
#include <utility>
#include "SmartPtr.hpp"

using std::pair;


class Bytes
{
public:
	typedef pair< shared_ptr<Bytes>, shared_ptr<Bytes> > SplitResult;
	
	Bytes();
	virtual const uint8_t* ptr() const = 0;
	//virtual uint8_t* ptr() = 0;
	virtual size_t size() const = 0;
	virtual SplitResult split(off_t where) const = 0; // TODO Make this a size_t
	bool showAsModified() const;
	void setShowAsModified(bool modified);
	
protected:
	bool showAsModifiedFlag;
};
