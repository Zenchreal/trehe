/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Store a map of bytes that can be read, inserted onto, deleted from,
 * etc. as efficiently as possible.
 */
#pragma once
#include <cstddef>
#include <iostream>
#include <map>
#include "SmartPtr.hpp"

class Bytes;

#if !defined(ssize_t)
typedef ptrdiff_t ssize_t;
#endif // !defined(ssize_t)


class BytesMap
{
public:
	BytesMap();
	bool insert(size_t offset, shared_ptr<Bytes> bytes);
	bool overwrite(size_t offset, shared_ptr<Bytes> bytes);
	bool remove(size_t offset, size_t length);
	size_t size() const;
	void clear();

private:
	typedef std::map< size_t, shared_ptr<Bytes> > Map;

	/**
	 * Split the item at the given offset into two different items.
	 */
	bool splitAtOffset(size_t offset);

	/**
	 * Add diff to the offset of the item at iter and every item
	 * following.
	 */
	void reoffset(Map::iterator iter, off_t diff);

	void recalcSize();

	// Debug helper functions
	void printMap() const;
	bool checkMap() const;
	
	size_t currentSize;
	Map map;

public:
	/**
	 * Class to read from a BytesMap as if it were a stream/array.
	 */
	class Reader
	{
	public:
		/**
		 * Construct a plain reader with no options.
		 */
		Reader(const BytesMap& bytesMap);
		
		/**
		 * Seek to the given location in the BytesMap.
		 */
		void seek(size_t offset);
		size_t tell() const;
		virtual bool end() const;
		virtual bool beginning() const;
		virtual size_t read(void* buffer, size_t size, bool* showAsModified = nullptr);

	protected:
		const BytesMap& bytesMap;
		BytesMap::Map::const_iterator iter;
		size_t offsetInIter;
	};
	
	class ReversedReader : public Reader
	{
	public:
		/**
		 * Construct a plain reader with no options.
		 */
		ReversedReader(const BytesMap& bytesMap);
		virtual bool end() const;
		virtual bool beginning() const;
		virtual size_t read(void* buffer, size_t size, bool* showAsModified = nullptr);
	};
};


#define SR_BUFFER_SIZE size_t(128 * 1024)
#define SR_BUFFER_KEEP_SIZE size_t(256)

/**
 * Offer buffered, mostly sequential reading of a BytesMap::Reader. Can
 * only seek relative to the current position, and at least
 * bufferKeepSize bytes since the offset of the farthest forward seek.
 */
class SequentialReader
{
public:
	SequentialReader(BytesMap::Reader* reader = nullptr):
		reader(reader),
		bufferUsed(0),
		offset(0)
	{
		reset(reader);
	}
	
	inline void reset(BytesMap::Reader* reader)
	{
		this->reader = reader;
		bufferUsed = 0;
		offset = 0;
		
		forward(0);
	}
	
	/**
	 * Seek forward in the stream n bytes relative to the current
	 * offset. If n is negative, seek backward. Very important note: you
	 * can only seek backward at least bufferKeepSize bytes from the
	 * farthest forward seek.
	 */
	inline void forward(ssize_t n)
	{
		using std::min;
		using std::move;
		
		if (offset + n < 0)
		{
			std::cout << "OOPS: TRIED TO SEEK PAST BEGINNING OF STREAM" << std::endl;
			offset = offset + n;
			return;
		}
		
		while (offset + n >= ssize_t(bufferUsed))
		{
			// Have we encountered the end of the stream?
			if (reader->end())
			{
				offset = bufferUsed;
				return;
			}
				
			// Move the last bufferKeepSize bytes from the end of the
			// buffer to the beginning.
			size_t keepSize = min(bufferUsed, SR_BUFFER_KEEP_SIZE);
			
			if (keepSize > 0)
				move(buffer + (bufferUsed - keepSize), buffer + bufferUsed, buffer);
			
			offset -= ssize_t(bufferUsed - keepSize);
			bufferUsed = keepSize;
			
			// Read in more bytes to fill the buffer
			bufferUsed += reader->read(buffer + keepSize, SR_BUFFER_SIZE - keepSize);
		}
		
		offset += n;
		
		assert(offset >= 0);
	}
	
	inline bool end() const
	{
		if (!reader)
			return true;
			
		return reader->end() && (offset == ssize_t(bufferUsed));
	}
	
	inline uint8_t at() const
	{
		return buffer[offset];
	}
	
private:
	BytesMap::Reader* reader;
	
	uint8_t buffer[SR_BUFFER_SIZE];
	size_t bufferUsed;
	ssize_t offset;
};
