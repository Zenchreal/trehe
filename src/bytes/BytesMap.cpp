/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Store a map of bytes that can be read, inserted onto, deleted from,
 * etc. as efficiently as possible.
 */
#include <cassert>
#include <utility>
#include <string>
#include "BytesMap.hpp"
#include "Bytes.hpp"

#include <iostream>
using std::cout;
using std::endl;


BytesMap::BytesMap():
	currentSize(0)
{
}

bool BytesMap::insert(size_t offset, shared_ptr<Bytes> bytes)
{
	// We can't do anything with a negative offset or zero bytes
	if (offset < 0)
		return false;
	if (bytes->size() < 1)
		return false;

	splitAtOffset(offset);

	// Find location in the map that contains the requested offset
	Map::iterator iter = map.find(offset);
	if (iter == map.end())
	{
		// Make sure the offset is at the end of the map
		if (offset != currentSize)
			return false;
	}

	// Push all the following items back
	reoffset(iter, bytes->size());

	// Insert the new item
	map.insert(Map::value_type(offset, bytes));
	recalcSize();

	//printMap();
	assert(checkMap());
	return true;
}

bool BytesMap::overwrite(size_t offset, shared_ptr<Bytes> bytes)
{
	// We can't do anything with a negative offset or zero bytes
	if (offset < 0)
		return false;
	if (bytes->size() < 1)
		return false;

	const size_t endOffset = offset + bytes->size();
	assert(endOffset > offset);

	splitAtOffset(offset);
	splitAtOffset(endOffset);


	// Find iterators for our offsets
	Map::iterator startIter = map.find(offset);
	if (startIter == map.end())
	{
		// The offset must not have been in range
		return false;
	}

	Map::iterator endIter = map.find(endOffset);
	if (endIter == map.end())
	{
		assert(endIter != map.begin());
		
		// We need to make sure we don't run past the end of the map
		if (endOffset > currentSize)
			return false;
	}

	// And erase
	map.erase(startIter, endIter);
	
	// Insert the new item
	map.insert(Map::value_type(offset, bytes));

	// If we modify this function's behavior, we might have to call
	// recalcSize();

	//printMap();
	assert(checkMap());
	return true;
}

bool BytesMap::remove(size_t offset, size_t length)
{
	// We can't do anything with a negative offset or zero bytes
	if (offset < 0)
		return false;
	if (length < 1)
		return false;

	const size_t endOffset = offset + length;
	assert(endOffset > offset);

	splitAtOffset(offset);
	splitAtOffset(endOffset);

	// Find iterators for our offsets
	Map::iterator startIter = map.find(offset);
	if (startIter == map.end())
	{
		// The offset must not have been in range
		return false;
	}

	Map::iterator endIter = map.find(endOffset);

	// And erase
	map.erase(startIter, endIter);

	// Push all the following items forward
	reoffset(endIter, -(off_t) length);
	recalcSize();

	//printMap();
	assert(checkMap());
	return true;
}

size_t BytesMap::size() const
{
	return currentSize;
}

void BytesMap::clear()
{
	currentSize = 0;
	map.clear();
}

bool BytesMap::splitAtOffset(size_t offset)
{
	using std::pair;

	// We can't split at a negative offset or zero
	if (offset <= 0)
		return false;

	// Find location in the map that contains the requested offset
	Map::iterator iter = map.upper_bound(offset);
	if (iter != map.begin())
	{
		iter--;
	}

	// Is there an item at this location?
	if (iter == map.end())
	{
		return false;
	}
	else
	{
		const size_t itemStart = iter->first;
		const size_t itemEnd = itemStart + iter->second->size();
		
		if (offset > itemStart && offset < itemEnd)
		{
			// The offset is in the middle of an item, so we need to
			// split this item.
		}
		else if (offset < itemStart)
		{
			// Something is wrong with upper_bound if this happens.
			assert(offset < itemStart);
			return false;
		}
		else
		{
			return false;
		}
	}

	// Call the item's split function
	const size_t itemStart = iter->first;
	const size_t where = offset - itemStart;
	pair< shared_ptr<Bytes>, shared_ptr<Bytes> > result;
	result = iter->second->split(where);

	// Both need to be valid items
	assert(result.first);
	assert(result.second);

	// Remove the old, add the new
	Map::iterator insertIter = iter;
	if (iter != map.begin())
	{
		--insertIter;
		map.erase(iter);
	}
	else
	{
		map.erase(iter);
		insertIter = map.begin();
	}
	
	iter = map.insert(insertIter, Map::value_type(itemStart, result.first));
	iter = map.insert(iter, Map::value_type(itemStart + where, result.second));
	return true;
}

void BytesMap::reoffset(Map::iterator iter, off_t diff)
{
	Map::iterator startIter = iter;

	// Insert items onto a temporary map (attempt efficiency)
	Map temp;
	for (; iter != map.end(); ++iter)
	{
		temp.insert(temp.end(), Map::value_type(iter->first + diff, iter->second));
	}

	// Now erase them from the map and replace them
	map.erase(startIter, map.end());
	map.insert(temp.begin(), temp.end());
}

void BytesMap::recalcSize()
{
	Map::const_iterator last = map.end();
	if (last == map.begin())
	{
		currentSize = 0;
	}
	else
	{
		last--;
		currentSize = last->first + last->second->size();
	}
}

void BytesMap::printMap() const
{
	cout << "===== BytesMap =====" << endl;
	for (const Map::value_type& item : map)
	{
		std::string text((const char*) item.second->ptr(), item.second->size());
		cout << item.first << ": " << "" << " - " << item.second->size() << " byte(s)" << endl;
	}
	
	if (checkMap())
		cout << "valid" << endl;
	else
		cout << "INVALID!" << endl;
		
	cout << "--------------------" << endl;
}

bool BytesMap::checkMap() const
{
	size_t last = 0;
	for (const Map::value_type& item : map)
	{
		if (item.first != last)
			return false;

		last += item.second->size();
	}
	if (last != currentSize)
	{
		return false;
	}
	return true;
}


BytesMap::Reader::Reader(const BytesMap& bytesMap):
	bytesMap(bytesMap),
	iter(bytesMap.map.begin()),
	offsetInIter(0)
{
}

void BytesMap::Reader::seek(size_t offset)
{
	assert(offset >= 0);

	// If we're seeking beyond a valid offset, just go to the end
	if (offset >= bytesMap.size())
	{
		iter = bytesMap.map.end();
		offsetInIter = 0;
		return;
	}

	// Sanity check
	if (bytesMap.map.size() < 1)
		return;

	// Find location in the map that contains the requested offset
	iter = bytesMap.map.upper_bound(offset);
	if (iter != bytesMap.map.begin())
	{
		iter--;
	}

	// Just double check that we really are in the right one
	assert(iter->first <= offset);
	assert(iter->first + iter->second->size() > offset);

	offsetInIter = offset - iter->first;
}

size_t BytesMap::Reader::tell() const
{
	if (end())
		return bytesMap.size();
	else
		return iter->first + offsetInIter;
}

bool BytesMap::Reader::end() const
{
	return iter == bytesMap.map.end();
}

bool BytesMap::Reader::beginning() const
{
	return (iter == bytesMap.map.begin()) && (offsetInIter == 0);
}

size_t BytesMap::Reader::read(void* buffer, size_t size, bool* showAsModified)
{
	using std::min;
	using std::copy;
	using std::fill;

	assert(size >= 0);
	
	uint8_t* byteBuffer = (uint8_t*) buffer;
	size_t total = 0;

	// Loop while there's room left in the buffer and we have data
	while (size > 0 && !end())
	{
		const size_t itemSize = iter->second->size();
		const uint8_t* itemPtr = iter->second->ptr();
		
		assert(itemSize >= offsetInIter);

		// Copy data from the item to the buffer
		const uint8_t* ptr = itemPtr + offsetInIter;
		const size_t read = min(itemSize - offsetInIter, size);
		copy(ptr, ptr + read, byteBuffer);
		
		if (showAsModified != nullptr)
		{
			fill(showAsModified, showAsModified + read, iter->second->showAsModified());
		}
		
		byteBuffer += read;
		size -= read;
		total += read;
		offsetInIter += read;

		// Just to be absolutely clear -- this is obvious from above
		assert(offsetInIter <= itemSize);

		// Increment the iterator if we're at the end of the item
		if (offsetInIter == itemSize)
		{
			iter++;
			offsetInIter = 0;
		}
	}
	return total;
}

BytesMap::ReversedReader::ReversedReader(const BytesMap& bytesMap):
		Reader(bytesMap)
{
}

bool BytesMap::ReversedReader::end() const
{
	return Reader::beginning();
}

bool BytesMap::ReversedReader::beginning() const
{
	return Reader::end();
}

size_t BytesMap::ReversedReader::read(void* buffer, size_t size, bool* showAsModified)
{
	using std::min;
	using std::reverse_copy;
	using std::fill;

	assert(size >= 0);
	
	uint8_t* byteBuffer = (uint8_t*) buffer;
	size_t total = 0;

	// Loop while there's room left in the buffer and we have data
	while (size > 0 && !end())
	{
		// Decrement the iterator before reading
		if (offsetInIter <= 0)
		{
			// We can assume that this produces a valid item because !end() guarantees it
			iter--;
			offsetInIter = iter->second->size();
		}

		const size_t itemSize = iter->second->size(); // probably don't need
		const uint8_t* itemPtr = iter->second->ptr();
		
		assert(itemSize >= offsetInIter);

		// Copy data from the item to the buffer
		const size_t read = min(offsetInIter, size);
		const uint8_t* ptr = itemPtr + offsetInIter - read;
		reverse_copy(ptr, ptr + read, byteBuffer);
		
		if (showAsModified != nullptr)
		{
			fill(showAsModified, showAsModified + read, iter->second->showAsModified());
		}
		
		byteBuffer += read;
		size -= read;
		total += read;
		offsetInIter -= read;

		// Just to be absolutely clear -- this is obvious from above
		assert(offsetInIter <= itemSize);
	}
	return total;
}
