#include "MarksHierarchy.hpp"
#include <iostream>

using std::vector;

MarksHierarchy::Node::Node()
{
}

MarksHierarchy::Node::Node(MarkPtr mark):
	mark(mark)
{
}

MarkPtr MarksHierarchy::Node::ptr() const
{
	return mark;
}

MarksHierarchy::Node& MarksHierarchy::root()
{
	return n;
}

const MarksHierarchy::Node& MarksHierarchy::root() const
{
	return n;
}

void MarksHierarchy::Node::build(vector<MarkPtr>::const_iterator& iter,
								 vector<MarkPtr>::const_iterator& end)
{
	// If mark is null we're at the root node, so add anything that's left.
	while ((iter != end) && (!mark || mark->overlaps(*(*iter))))
	{
		children.push_back(Node(*iter));

		++iter;
		children.back().build(iter, end);
	}
}

void MarksHierarchy::Node::print(int indent) const
{
	using std::cout;
	using std::endl;

	for (int i = 0; i < indent; i++)
	{
		cout << " ";
	}
	if (mark && mark->hasName())
	{
		cout << mark->name() << endl;
	}
	else if (mark)
	{
		cout << "[" << mark->offset() << "]" << endl;
	}
	for (const Node& n : children)
	{
		n.print(indent + 1);
	}
}

void MarksHierarchy::build(const vector<MarkPtr>& marks)
{
	n = Node();
	vector<MarkPtr>::const_iterator begin = marks.begin();
	vector<MarkPtr>::const_iterator end = marks.end();
	
	n.build(begin, end);
}
