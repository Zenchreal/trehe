/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Store an array of bytes that can be inserted into and deleted from.
 * Currently implemented as an std::vector. If this proves too slow, use
 * malloc, realloc, and free.
 */
#include "MemoryBytes.hpp"


MemoryBytes::MemoryBytes()
{
}

MemoryBytes::MemoryBytes(size_t size):
	byteVec(size)
{
}

MemoryBytes::MemoryBytes(const uint8_t* bytes, size_t size):
	byteVec(bytes, bytes + size)
{
}

MemoryBytes::MemoryBytes(const MemoryBytes& obj):
	byteVec(obj.byteVec)
{
}

MemoryBytes& MemoryBytes::operator=(const MemoryBytes& obj)
{
	byteVec = obj.byteVec;
	return *this;
}

MemoryBytes::~MemoryBytes()
{
}

const uint8_t* MemoryBytes::ptr() const
{
	return &byteVec[0];
}

uint8_t* MemoryBytes::ptr()
{
	return &byteVec[0];
}

size_t MemoryBytes::size() const
{
	return byteVec.size();
}

Bytes::SplitResult MemoryBytes::split(off_t where) const
{
	// Make sure the offset is in range
	if (where < 0 || where >= (off_t) size())
	{
		return SplitResult();
	}

	// Create our result
	SplitResult result;
	result.first = make_shared<MemoryBytes>(ptr(), where);
	result.second = make_shared<MemoryBytes>(ptr() + where, size() - where);
	
	result.first->setShowAsModified(showAsModified());
	result.second->setShowAsModified(showAsModified());
	return result;
}
