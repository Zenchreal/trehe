/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * A memory mapped file
 */
#include "MMappedFileWrapper.hpp"
#include <iostream>

using namespace boost::iostreams;


MMappedFileWrapper::MMappedFileWrapper()
{
}

MMappedFileWrapper::~MMappedFileWrapper()
{
	if (!savedFileName.empty())
	{
		std::cout << "mmapped file closed: " << savedFileName << std::endl;
	}
	file.close();
}

void MMappedFileWrapper::open(const std::string& fileName)
{
	mapped_file_params params(fileName);
	params.flags = mapped_file::mapmode::readonly;

	// This can potentially throw an std::ios_base::failure
	file.open(params);

	// Debugging
	std::cout << "mmapped file opened: " << fileName << std::endl;
	savedFileName = fileName;
}

bool MMappedFileWrapper::isOpen() const
{
	return file.is_open();
}

size_t MMappedFileWrapper::size() const
{
	return file.size();
}

const void* MMappedFileWrapper::ptr() const
{
	if (file.is_open())
		return file.data();
	else
		return nullptr;
}
