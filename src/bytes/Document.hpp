/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * A document
 */
#pragma once
#include <cstddef>
#include <string>
#include <vector>
#include "BytesMap.hpp"
#include "MarksMap.hpp"
#include "MarksHierarchy.hpp"
#include "Interpretation.hpp"

using std::vector;


class Document
{
public:
	Document(const std::string& path, bool tempPath = true);

	~Document();

	/**
	 * Load the file from disk, and only if successful throw away any
	 * modifications to the bytes and replace with the loaded file.
	 * The marks should stay intact. Throws exceptions.
	 */
	void reload();

	/**
	 * Save the current document to disk to a file with the current
	 * path. We reload the file as part of this operation, and clear the
	 * modified flag.
	 */
	void save();

	/**
	 * Returns a reader object for bytes in this document.
	 */
	BytesMap::Reader reader() const;
	
	/**
	 * Change this document's path. This will change the temporary path
	 * flag to false.
	 */
	void setPath(const std::string& path);

	/**
	 * Return the path to the document's file, which may or may not
	 * exist yet if the user hasn't saved it.
	 */
	std::string path() const;

	/**
	 * Flag the path as being temporary or not. If a path is temporary,
	 * the program ought to prompt before saving it to that location.
	 * For example when the user clicks new, the path is set to
	 * 'untitled' which is a temporary path.
	 */
	void setTempPath(bool tempPath);

	/**
	 * Return the temporary path flag. For a description of what it
	 * means for a path to be temporary, see setTempPath.
	 */
	bool isTempPath() const;

	/**
	 * Has the document been modified since it was last saved?
	 */
	bool isModified() const;

	/**
	 * Set the read only flag on the document. If this is true, all
	 * operations that would modify the document return false without
	 * doing anything.
	 */
	void setReadOnly(bool readOnly);

	/**
	 * Is the read only flag set?
	 */
	bool isReadOnly() const;
	
	/**
	 * Set the default endian for the document, which may be stored in
	 * the marks file.
	 */
	void setDefaultEndian(Interpretation::Endian flag);
	
	/**
	 * Get the default endian.
	 */
	Interpretation::Endian defaultEndian() const;
	
	/**
	 * Returns the current number of bytes in the document.
	 */
	size_t size() const;
	
	/**
	 * Overwrite a single byte at the given offset. This will mark the
	 * document as modified.
	 */
	bool overwrite(size_t offset, uint8_t b);
	
	bool overwrite(size_t offset, const uint8_t* b, size_t length);
	
	/**
	 * Insert a single byte at the given offset. This will increase the
	 * size of the document and mark it as modified.
	 */
	bool insert(size_t offset, uint8_t b);
	
	bool insert(size_t offset, const uint8_t* b, size_t length);
	
	/**
	 * Remove a sequency of bytes at the given offset. This will
	 * decrease the size of the document and mark it as modified.
	 */
	bool remove(size_t offset, size_t length = 1);
	
	/**
	 * Find the next occurence of the given byte string beginning at
	 * offset start. Return size_t(-1) if no more occurrences of the
	 * string could be found, otherwise the offset of the byte string.
	 */
	size_t find(size_t start, const uint8_t* b, size_t length, bool forward = true);

	/**
	 * Write the subset of this document starting at offset and running
	 * length bytes into the file specified by path, overwritting that
	 * file. Throws exception if file save failed.
	 */
	void saveToFile(const std::string& path, size_t offset, size_t length);

	MarksMap& marks();

	const MarksMap& marks() const;

	MarksHierarchy& marksHierarchy();

	const MarksHierarchy& marksHierarchy() const;

	/**
	 * Rebuild the marks hierarchy.
	 */
	void marksChanged();

protected:
	std::string fullPath;
	BytesMap bytesMap;
	MarksMap marksMap;
	MarksHierarchy marksHier;
	bool modifiedFlag;
	bool tempPathFlag;
	bool readOnlyFlag;
	Interpretation::Endian defaultEndianValue;
	
	void loadMarks();
	void saveMarks();
};
