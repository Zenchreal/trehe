#include "MarksMap.hpp"
#include <map>
#include <set>
#include <iostream>
#include "Mark.hpp"

using std::cout;
using std::endl;


MarksMap::MarksMap()
{
}

void MarksMap::add(MarkPtr mark, size_t offset, size_t size)
{
	assert(mark);
	assert(offset >= 0);
	assert(size >= 0);

	// A zero-size mark makes no sense, silently ignore
	if (size == 0)
		return;

	// Make sure we haven't been added already
	assert(mark->startOffset == size_t(-1) && mark->endOffset == size_t(-1));

	mark->startOffset = offset;
	mark->endOffset = offset + size - 1;

	auto iter = newChunkAtOffset(mark->startOffset);
	auto end = chunkUpperBound(mark->endOffset);

	for (; iter != end; ++iter)
	{
		iter->second.insert(mark);
	}
}

void MarksMap::remove(MarkPtr mark)
{
	if (!mark)
		return;

	// Make sure we have been added already
	assert(mark->startOffset != size_t(-1) && mark->endOffset != size_t(-1));

	auto iter = chunkLowerBound(mark->startOffset);
	auto end = chunkUpperBound(mark->endOffset);

	while (iter != end)
	{
		iter->second.erase(mark);

		// Empty chunks are no longer needed
		if (iter->second.empty())
			iter = map.erase(iter);
		else
			++iter;
	}

	mark->startOffset = -1;
	mark->endOffset = -1;
}

void MarksMap::modify(MarkPtr mark, size_t newOffset, size_t newSize)
{
	assert(mark);
	assert(newOffset >= 0);
	assert(newSize >= 0);

	// It is not permissible to have a zero sized mark; just remove it
	if (newSize == 0)
	{
		remove(mark);
		return;
	}

	if (mark->startOffset == newOffset && mark->size() == newSize)
	{
		return;
	}

	// I'm not sure there's anything more efficient we can do
	remove(mark);
	add(mark, newOffset, newSize);
}

bool MarksMap::empty() const
{
	// This works because the remove function will always erase an empty
	// chunk.
	return map.empty();
}

void MarksMap::clear()
{
	map.clear();
}

void MarksMap::insertBytes(size_t offset, size_t size)
{
	assert(offset >= 0);
	assert(size >= 0);

	// Why would anyone ever pass us this?
	if (size == 0)
		return;

	// First, stretch any marks that are one the border
	if (offset > 0)
	{
		size_t lastOffset = offset - 1;
		for (MarkPtr mark : range(lastOffset, offset))
		{
			if (mark->endingOffset() != lastOffset &&
				mark->offset() != offset &&
				mark->stretchableFlag)
			{
				// TODO Stretch it!
			}
		}
	}

	// Next, move everything back
	// TODO
}

void MarksMap::removeBytes(size_t offset, size_t size)
{
	assert(offset >= 0);
	assert(size >= 0);

	// Why would anyone ever pass us this?
	if (size == 0)
		return;

	//
}

vector<MarkPtr> MarksMap::all() const
{
	vector<MarkPtr> result;

	for (const auto& value : map)
	{
		size_t offset = value.first;
		const Chunk& chunk = value.second;

		for (const auto& mark : chunk)
		{
			// Only add this mark to the result if it's the first time it is
			// listed, i.e. at the chunk that has the key the same as its offset.
			if (offset == mark->startOffset)
				result.push_back(mark);
		}
	}
	return result;
}

vector<MarkPtr> MarksMap::range(size_t startOffset, size_t endOffset) const
{
	vector<MarkPtr> result;
	if (startOffset > endOffset)
		return result;

	auto iter = chunkLowerBound(startOffset);
	auto end = chunkUpperBound(endOffset);

	// Add all marks from the first chunk
	if (iter != end)
	{
		for (const auto& mark : iter->second)
		{
			if (mark->endOffset >= startOffset)
				result.push_back(mark);
		}
		++iter;
	}

	for (; iter != end; ++iter)
	{
		size_t offset = iter->first;
		const Chunk& chunk = iter->second;

		for (const auto& mark : chunk)
		{
			// Only add this mark to the result if it's the first time it is
			// listed, i.e. at the chunk that has the key the same as its offset.
			if (offset == mark->startOffset && mark->endOffset >= startOffset)
				result.push_back(mark);
		}
	}
	return result;
}

vector<MarkPtr> MarksMap::contained(size_t startOffset, size_t endOffset) const
{
	vector<MarkPtr> result;
	if (startOffset > endOffset)
		return result;

	auto iter = chunkLowerBound(startOffset);
	auto end = chunkUpperBound(endOffset);

	for (; iter != end; ++iter)
	{
		size_t offset = iter->first;
		const Chunk& chunk = iter->second;

		for (const auto& mark : chunk)
		{
			// Only add this mark to the result if it's the first time it is
			// listed, i.e. at the chunk that has the key the same as its offset.
			if (offset == mark->startOffset &&
				mark->endOffset >= startOffset &&
				mark->endOffset <= endOffset)
			{
				result.push_back(mark);
			}
		}
	}
	return result;
}

void MarksMap::serialize(Json::Value& out) const
{
	for (const auto& mark : all())
	{
		Json::Value m;
		mark->serialize(m);
		out.append(m);
	}
}

void MarksMap::deserialize(const Json::Value& in)
{
	for (const Json::Value& m : in)
	{
		if (!m.isObject())
			continue;

		MarkPtr mark;
		mark = make_shared<Mark, const Json::Value&>(m);
		size_t startOffset = size_t(m["startOffset"].asInt64());
		size_t endOffset = size_t(m["endOffset"].asInt64());
		
		// So add takes an offset and a size, not a start offset and an
		// end offset. Doh.
		add(mark, startOffset, endOffset - startOffset + 1);
	}
}

MarksMap::ChunkMap::iterator MarksMap::newChunkAtOffset(size_t offset)
{
	auto iter = map.upper_bound(offset);

	if (iter == map.begin())
	{
		return map.insert(ChunkMap::value_type(offset, Chunk())).first;
	}
	--iter;

	if (iter->first == offset)
	{
		return iter;
	}
	else
	{
		auto newIter = map.insert(ChunkMap::value_type(offset, Chunk())).first;
		Chunk& first = iter->second;
		Chunk& second = newIter->second;

		// Add all of the marks in the last chunk to this new chunk (which
		// immediately follows the old) as long as they reach the new
		// offset.
		std::remove_copy_if(first.begin(), first.end(), std::inserter(second, second.end()), [=] (const MarkPtr& mark)
		{
			return bool(mark->endOffset < offset);
		});

		return newIter;
	}
}


MarksMap::ChunkMap::iterator MarksMap::chunkLowerBound(size_t offset)
{
	auto iter = map.upper_bound(offset);
	if (iter == map.begin())
	{
		return iter;
	}
	else
	{
		return --iter;
	}
}

MarksMap::ChunkMap::iterator MarksMap::chunkUpperBound(size_t offset)
{
	return map.upper_bound(offset);
}

MarksMap::ChunkMap::const_iterator MarksMap::chunkLowerBound(size_t offset) const
{
	auto iter = map.upper_bound(offset);
	if (iter == map.begin())
	{
		return iter;
	}
	else
	{
		return --iter;
	}
}

MarksMap::ChunkMap::const_iterator MarksMap::chunkUpperBound(size_t offset) const
{
	return map.upper_bound(offset);
}
