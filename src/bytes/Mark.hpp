#pragma once
#include <cstddef>
#include <cstdint>
#include <cassert>
#include <string>
#include <json/json.h>
#include "SmartPtr.hpp"

class Interpretation;


class Mark
{
public:
	Mark():
		startOffset(-1),
		endOffset(-1),
		markColor(0),
		actualMarkColor(0),
		stretchableFlag(false),
		collapsedFlag(false)
	{
	}

	Mark(uint32_t color, bool stretchable = false):
		startOffset(-1),
		endOffset(-1),
		markColor(color),
		actualMarkColor(color),
		stretchableFlag(stretchable),
		collapsedFlag(false)
	{
	}

	size_t offset() const
	{
		return startOffset;
	}

	size_t size() const
	{
		assert(endOffset >= startOffset);
		return endOffset - startOffset + 1;
	}

	size_t endingOffset() const
	{
		return endOffset;
	}

	void setStretchable(bool flag)
	{
		stretchableFlag = flag;
	}
	
	bool isStretchable() const
	{
		return stretchableFlag;
	}
	
	void setColor(uint32_t color)
	{
		markColor = color;
		actualMarkColor = color;
	}
	
	uint32_t color() const
	{
		return markColor;
	}
	
	void setActualColor(uint32_t color)
	{
		actualMarkColor = color;
	}
	
	uint32_t actualColor() const
	{
		return actualMarkColor;
	}

	bool isCollapsed() const
	{
		return collapsedFlag;
	}

	void setCollapsed(bool flag)
	{
		collapsedFlag = flag;
	}

	void setName(const std::string& name)
	{
		markName = name;
	}

	bool hasName() const
	{
		return !markName.empty();
	}

	const std::string& name() const
	{
		return markName;
	}
	
	/**
	 * Does the mark overlap with the specified mark?
	 */
	bool overlaps(const Mark& other) const;
	
	/**
	 * Does the mark completely contain the specified mark?
	 */
	bool contains(const Mark& other) const;
	
	/**
	 * Get the interpretation that is associated with this mark (sort of
	 * like the type of the mark).
	 */
	shared_ptr<Interpretation> interpretation() const;
	
	/**
	 * Change the interpretation associated with this mark.
	 */
	void setInterpretation(shared_ptr<Interpretation> newInterp);
	
	/**
	 * Get the interpretation if one is set, otherwise create a new one
	 * based on the properties of this mark. Does not set this in the
	 * mark.
	 */
	shared_ptr<Interpretation> defaultInterpretation() const;

	Mark(const Json::Value& in);

	virtual void serialize(Json::Value& out) const;

private:
	// Allow MarksMap to modify these in order to keep everything synced.
	friend class MarksMap;
	
	// Do not even think about modifying these. It will not do what you
	// assume it does.
	size_t startOffset;
	size_t endOffset;

protected:
	uint32_t markColor;
	uint32_t actualMarkColor;
	std::string markName;
	bool stretchableFlag;
	shared_ptr<Interpretation> interp;
	bool collapsedFlag;
};


/**
 * Save typing and make the APIs look prettier.
 */
typedef shared_ptr<Mark> MarkPtr;
typedef shared_ptr<Mark const> MarkConstPtr;
