/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * A read-only memory mapped file
 */
#pragma once
#include <cstddef>
#include <string>
#include <boost/iostreams/device/mapped_file.hpp>


class MMappedFileWrapper
{
public:
	MMappedFileWrapper();
	~MMappedFileWrapper();
	void open(const std::string& fileName);
	bool isOpen() const;
	size_t size() const;
	const void* ptr() const;

private:
	boost::iostreams::mapped_file_source file;
	std::string savedFileName;
};
