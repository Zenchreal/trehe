/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Base class for an array of bytes from any source. Once added to a
 * BytesMap, the size ABSOLUTELY MUST NOT change.
 */
#include "Bytes.hpp"


Bytes::Bytes():
	showAsModifiedFlag(false)
{
}

bool Bytes::showAsModified() const
{
	return showAsModifiedFlag;
}

void Bytes::setShowAsModified(bool modified)
{
	showAsModifiedFlag = modified;
}
