#pragma once
#include "Interpretation.hpp"


class CharInterp : public Interpretation
{
public:
	virtual std::string name() const;
	virtual Result bytesToStringBounded(const uint8_t* bytes, size_t length, std::string& output, unsigned truncateLength) const;
};
