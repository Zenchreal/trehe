/*
 * Put license here.
 *
 * Copyright (C) 2012 Ben Moench.
 *
 * Read only memory mapped bytes.
 */
#pragma once
#include <cstddef>
#include <cstdint>
#include "SmartPtr.hpp"
#include "Bytes.hpp"

class MMappedFileWrapper;


class MMappedBytes : public Bytes
{
public:
	MMappedBytes();
	MMappedBytes(shared_ptr<MMappedFileWrapper> file);
	MMappedBytes(shared_ptr<MMappedFileWrapper> file, size_t offset, size_t size);
	MMappedBytes(const MMappedBytes& obj);
	MMappedBytes& operator=(const MMappedBytes& obj);
	virtual ~MMappedBytes();
	virtual const uint8_t* ptr() const;
	virtual size_t size() const;
	virtual SplitResult split(off_t where) const;

private:
	size_t offsetRegion;
	size_t sizeRegion;
	shared_ptr<MMappedFileWrapper> file;
};
