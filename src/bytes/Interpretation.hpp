#pragma once
#include <cstdint>
#include <string>
#include <map>
#include <vector>
#include <json/json.h>
#include "SmartPtr.hpp"

/**
 * Base class for a family of classes used to convert an array of raw
 * bytes into a string and vice versa by interpreting it in a certain
 * way, e.g. as a signed, 2 byte integer.
 */
class Interpretation
{
public:
	enum Endian
	{
		DEFAULT = 0,
		LITTLE = 1,
		BIG = 2
	};

	enum Result
	{
		SUCCESS,
		NOT_ENOUGH_DATA,
		INVALID,
		METHOD_NOT_APPLICABLE
	};
	
public:
	Interpretation():
		endianFlag(LITTLE)
	{
	}

	virtual std::string name() const = 0;
	virtual std::string description() const;

	virtual Result bytesToStringBounded(const uint8_t* bytes, size_t length, std::string& output, unsigned truncateLength) const;
	
	//virtual Result bytesToStringUnbounded(Reader reader, std::string& output, uint truncateLength) const;
	
	virtual Result stringToBytes(const std::string& input, uint8_t* bytes, size_t& length) const;

	inline void setEndian(Endian endian)
	{
		endianFlag = endian;
	}
	
	inline Endian endian() const
	{
		return endianFlag;
	}
	
	/**
	 * Serialize the mark to a JSON object. When reimplementing this
	 * function in child classes, make sure to call the base class
	 * (Interpretation).
	 */
	virtual void serialize(Json::Value& out) const;
	
	/**
	 * Deserialize the mark from a JSON object, instantiating the
	 * appropriate child class.
	 */
	static shared_ptr<Interpretation> deserialize(const Json::Value& in);
	
	/**
	 * Call this function once before deserializing any interps or 
	 * instantiating any interps by name.
	 */
	static void initializeList();
	
	/**
	 * Instantiate an interpretation with the given name.
	 */
	static shared_ptr<Interpretation> instantiate(const std::string& name);

	/**
	 * Instantiate all interpretations and add them to the list.
	 */
	static void listAll(std::vector<shared_ptr<Interpretation>>& list);

	static const Endian systemEndian;

private:
	Endian endianFlag;
	
	typedef std::function<shared_ptr<Interpretation>()> InstFunc;
	
	static std::map<std::string, InstFunc> interpList;
	
protected:
	/**
	 * Once the correct class is instantiated, deserialize any
	 * attributes.
	 */
	virtual void doDeserialize(const Json::Value& in);
};
